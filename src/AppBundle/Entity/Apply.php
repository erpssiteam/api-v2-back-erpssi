<?php
namespace AppBundle\Entity;

use  \Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Created by PhpStorm.
 * User: rislou
 * Date: 22/03/16
 * Time: 19:43
 */

/**
 * Class Apply
 * @package AppBundle\Entity
 * @ORM\Table("apply")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ApplyRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Apply
{
    const APPLY_PENDING_STATE = 1;
    const APPLY_VALIDATED_STATE = 2;
    const APPLY_DENIED_STATE = 3;

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="cv", type="string", nullable=true)
     */
    protected $cv;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(name="first_name", type="string", length=100, nullable=false)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    protected $firstname;

    /**
     * @var string
     * @ORM\Column(name="last_name", type="string", length=100, nullable=false)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    protected $lastname;

    /**
     * @var \DateTime
     * @ORM\Column(name="birthday_date", type="datetime", nullable=false)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\DateTime()
     */
    protected $birthdayDate;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    protected $email;

    /**
     * @var string
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    protected $address;

    /**
     * @var string
     * @ORM\Column(name="zipCode", type="string", nullable=true)
     */
    protected $zipCode;

    /**
     * @var string
     * @ORM\Column(name="phone", type="string", length=10, nullable=true)
     * @Assert\Length(max="10")
     */
    protected $phone;

    /**
     * @var string
     * @ORM\Column(name="city", type="string", nullable=true)
     */
    protected $city;

    /**
     * @var integer
     * @ORM\Column(name="state", type="integer", options={"default": 1})
     */
    private $state = self::APPLY_PENDING_STATE;

    /**
     * @ORM\ManyToOne(targetEntity="Offer", inversedBy="applies")
     * @ORM\JoinColumn(name="id_offer", referencedColumnName="id", onDelete="CASCADE")
     * @Assert\Type(type="AppBundle\Entity\Offer")
     */
    private $offer;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="modified_at",type="datetime", nullable=true)
     */
    protected $modifiedAt;

    /**
     * Apply constructor.
     * @param $firstname
     * @param $lastname
     * @param $email
     * @param $birthdayDate
     * @param Offer $offer
     */
    public function __construct($firstname, $lastname, $email, $birthdayDate, Offer $offer) {
        $this->setOffer($offer);
        $this->setFirstname($firstname);
        $this->setLastname($lastname);
        $this->setEmail($email);
        $this->setBirthdayDate($birthdayDate);
        $this->setState(self::APPLY_PENDING_STATE);
    }

    /**
     * @return mixed
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * @param mixed $offer
     */
    public function setOffer($offer)
    {
        $this->offer = $offer;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /**
     * @return string
     */
    public function getCv()
    {
        return $this->cv;
    }

    /**
     * @param string $cv
     */
    public function setCv($cv)
    {
        $this->cv = $cv;
    }

    /**
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param int $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param string $zipCode
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return \DateTime
     */
    public function getBirthdayDate()
    {
        return $this->birthdayDate;
    }

    /**
     * @param \DateTime $birthdayDate
     */
    public function setBirthdayDate($birthdayDate)
    {
        $this->birthdayDate = $birthdayDate;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param int $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /** @ORM\PrePersist */
    public function timeStampOnPrePersist()
    {
        $this->setCreatedAt(new \DateTime());
    }

    /** @ORM\PreUpdate */
    public function timeStampOnPreUpdate()
    {
        $this->setModifiedAt(new \DateTime());
    }

}
