<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Class User
 * @package AppBundle\Entity
 * @ORM\Table("user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks
 * ---------------------------------------------------------------------------
 *  In order to maintain unity, this comment should not be removed.
 *
 *  This below, shows the tree role. It should be used for select, checkbox or radio button value, database communication, security, constant....
 *  This is how role are supposed to work.
 *  ///////
 *  ROLE_ADMIN (admin, rh, manager, collaborator, user) = 1;
 *  ROLE_RH (rh, collaborator, user)= 2;
 *  ROLE_MANAGER (manager, collaborator, user) = 3;
 *  ROLE_COLLABORATOR (collaborator, user) = 4;
 *  ROLE_USER (user) = 5;
 * ---------------------------------------------------------------------------
 */
class User implements UserInterface, UserProviderInterface
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="username", type="string", length=255, nullable=false)
     */
    private $username;

    /**
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\ManyToMany(targetEntity="Role")
     * @ORM\JoinTable(name="users_roles",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     *      )
     */
    private $roles;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="modified_at",type="datetime", nullable=true)
     */
    protected $modifiedAt;

    /**
     * User constructor.
     */
    public function __construct($username, $password, Role $role)
    {
        $this->username = $username;
        $this->password = $password;
        $this->roles = new ArrayCollection();
        $this->addRole($role);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }


    /**
     * @return array
     */
    public function getRoles()
    {
        $roles = [];
        foreach ($this->roles as $role) {
            /** @var Role $role */
            $roles [] = $role->getLabel();
        }
        return $roles;
    }

    /**
     * @param mixed $roles
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;
    }


    /**
     * @param Role $role
     */
    public function addRole(Role $role) {
        if(!$this->roles->contains($role)) {
            $this->roles->add($role);
        }
    }

    /**
     * @param Role $role
     */
    public function updateOrReplaceRole(Role $role){
        if(!empty($this->roles)){
            $this->roles = new ArrayCollection();
            $this->roles->add($role);
        }
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /**
     * Removes sensitive data from the user.
     */
    public function eraseCredentials() {}


    /**
     * Alternatively, we will use a salt
     */
    public function getSalt()
    {
        return;
    }

    public function loadUserByUsername($username)
    {
        // TODO: Implement loadUserByUsername() method.
    }

    public function refreshUser(UserInterface $user)
    {
        // TODO: Implement refreshUser() method.
    }

    public function supportsClass($class)
    {
        // TODO: Implement supportsClass() method.
    }


    /** @ORM\PreUpdate */
    public function timeStampOnPreUpdate()
    {
        $this->setModifiedAt(new \DateTime());
    }

    /** @ORM\PrePersist */
    public function timeStampOnPrePersist()
    {
        $this->setCreatedAt(new \DateTime());
    }

}
