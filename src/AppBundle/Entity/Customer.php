<?php
namespace AppBundle\Entity;

use  \Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Class Customer
 * @package AppBundle\Entity
 * @ORM\Table("customer")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CustomerRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Customer extends Person
{

    /**
     * @var string
     * @ORM\Column(name="siret", type="string", length=55, nullable=true)
     */
    private $siret;

    /**
     * @var string
     * @ORM\Column(name="siren", type="string", length=55, nullable=true)
     */
    private $siren;

    /**
     * @var string
     * @ORM\Column(name="website", type="string", length=100, nullable=true)
     */
    private $website;

    /**
     * @var string
     * @ORM\Column(name="contact_name", type="string", length=55, nullable=true)
     */
    private $contactName;

    /**
     * @var string
     * @ORM\Column(name="contact_phone", type="string", length=45, nullable=true)
     */
    private $contactPhone;

    /**
     * @var string
     * @ORM\Column(name="contact_email", type="string", length=55, nullable=true)
     */
    private $contactEmail;

    /**
     * @var string
     * @ORM\Column(name="general_email", type="string", length=55, nullable=true)
     */
    private $generalEmail;

    /**
     * Customer constructor.
     */
    public function __construct($firstname, $lastname, \DateTime $birthdayDate, $email, $address, $zipCode, $city, User $user)
    {
        parent::__construct($firstname, $lastname,$birthdayDate, $email, $address, $zipCode, $city, $user);
    }


    /**
     * @return string
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * @param string $siret
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;
    }

    /**
     * @return string
     */
    public function getSiren()
    {
        return $this->siren;
    }

    /**
     * @param string $siren
     */
    public function setSiren($siren)
    {
        $this->siren = $siren;
    }

    /**
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param string $website
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    }

    /**
     * @return string
     */
    public function getContactName()
    {
        return $this->contactName;
    }

    /**
     * @param string $contactName
     */
    public function setContactName($contactName)
    {
        $this->contactName = $contactName;
    }

    /**
     * @return string
     */
    public function getContactPhone()
    {
        return $this->contactPhone;
    }

    /**
     * @param string $contactPhone
     */
    public function setContactPhone($contactPhone)
    {
        $this->contactPhone = $contactPhone;
    }

    /**
     * @return string
     */
    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    /**
     * @param string $contactEmail
     */
    public function setContactEmail($contactEmail)
    {
        $this->contactEmail = $contactEmail;
    }

    /**
     * @return string
     */
    public function getGeneralEmail()
    {
        return $this->generalEmail;
    }

    /**
     * @param string $generalEmail
     */
    public function setGeneralEmail($generalEmail)
    {
        $this->generalEmail = $generalEmail;
    }




}