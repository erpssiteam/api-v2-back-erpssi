<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Person
 * @package AppBundle\Entity
 * @ORM\Table("person")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PersonRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"customer" = "Customer", "employee" = "Employee"})
 * @ORM\HasLifecycleCallbacks
 */
abstract class Person
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="first_name", type="string", length=100, nullable=false)
     */
    protected $firstname;

    /**
     * @var string
     * @ORM\Column(name="last_name", type="string", length=100, nullable=false)
     */
    protected $lastname;

    /**
     * @var \DateTime
     * @ORM\Column(name="birthday_date", type="datetime", nullable=false)
     */
    protected $birthdayDate;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $email;

    /**
     * @var string
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    protected $address;

    /**
     * @var integer
     * @ORM\Column(name="zipCode", type="integer", nullable=true)
     */
    protected $zipCode;

    /**
     * @var string
     * @ORM\Column(name="city", type="string", nullable=true)
     */
    protected $city;

    /**
     * One Person is one User.
     * @ORM\OneToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="modified_at",type="datetime", nullable=true)
     */
    protected $modifiedAt;

    /**
     * @var boolean
     * @ORM\Column(name="deprecated", type="boolean", options={ "default": FALSE })
     */
    protected $deprecated = 0;

    /**
     * Person constructor.
     * @param string $firstname
     * @param string $lastname
     * @param \DateTime $birthdayDate
     * @param $email
     * @param string $address
     * @param int $zipCode
     * @param string $city
     * @param $user
     */
    public function __construct($firstname, $lastname, \DateTime $birthdayDate, $email, $address, $zipCode, $city, User $user )
    {
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->birthdayDate = $birthdayDate;
        $this->email = $email;
        $this->address = $address;
        $this->zipCode = $zipCode;
        $this->city = $city;
        $this->user = $user;
    }


    public function getId()
    {
        return $this->id;
    }

    public function getEmail()
    {
        return $this->email;
    }
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return int
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param int $zipCode
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return \DateTime
     */
    public function getBirthdayDate()
    {
        return $this->birthdayDate;
    }

    /**
     * @param \DateTime $birthdayDate
     */
    public function setBirthdayDate($birthdayDate)
    {
        $this->birthdayDate = $birthdayDate;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return boolean
     */
    public function isDeprecated()
    {
        return $this->deprecated;
    }

    /**
     * @param boolean $deprecated
     */
    public function setDeprecated($deprecated)
    {
        $this->deprecated = $deprecated;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /** @ORM\PreUpdate */
    public function timeStampOnPreUpdate()
    {
        $this->setModifiedAt(new \DateTime());
    }

    /** @ORM\PrePersist */
    public function timeStampOnPrePersist()
    {
        $this->setCreatedAt(new \DateTime());
    }

}
