<?php
namespace AppBundle\Entity;

use  \Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Created by PhpStorm.
 * User: rislou
 * Date: 22/03/16
 * Time: 19:43
 */

/**
 * Class Offer
 * @package AppBundle\Entity
 * @ORM\Table("offer")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OfferRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Offer
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(name="start_at", type="datetime", nullable=false)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\DateTime()
     */
    private $startAt;
    
    /**
     * @var \DateTime
     * @ORM\Column(name="end_at", type="datetime", nullable=false)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\DateTime()
     */
    private $endAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="modified_at",type="datetime", nullable=true)
     */
    protected $modifiedAt;

    /**
     * @var boolean
     * @ORM\Column(name="is_valid", type="boolean", options={"default": FALSE})
     */
    private $valid = false;

    /**
     * @var string
     * @ORM\Column(name="reference", type="string", nullable=true)
     */
    private $reference;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", nullable=false)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $name;

    /**
     * @var int
     * @ORM\Column(name="consulted", type="integer", options={"default": 0}, nullable=false)
     */
    private $consulted = 0;

    /**
     * @var Apply[] | ArrayCollection
     * @ORM\OneToMany(targetEntity="Apply", mappedBy="offer")
     */
    private $applies;

    /**
     * @var JobProfile
     * @ORM\OneToOne(targetEntity="JobProfile", cascade={"remove"})
     */
    private $jobProfile;


    /**
     * Offer constructor.
     * @param JobProfile $jobProfile
     */
    public function __construct(JobProfile $jobProfile) {
        $this->setJobProfile($jobProfile);
        $this->applies = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /**
     * @return boolean
     */
    public function isValid()
    {
        return $this->valid;
    }

    /**
     * @param boolean $valid
     */
    public function setValid($valid)
    {
        $this->valid = $valid;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getConsulted()
    {
        return $this->consulted;
    }

    
    public function setConsulted()
    {
        $this->consulted = $this->consulted + 1;
    }

    /**
     * @return mixed
     */
    public function getApplies()
    {
        return $this->applies;
    }

    /**
     * @param mixed $applies
     */
    public function setApplies($applies)
    {
        $this->applies = $applies;
    }

    /**
     * @return JobProfile
     */
    public function getJobProfile()
    {
        return $this->jobProfile;
    }

    /**
     * @param JobProfile $jobProfile
     */
    public function setJobProfile($jobProfile)
    {
        $this->jobProfile = $jobProfile;
    }

    /**
     * @return \DateTime
     */
    public function getStartAt()
    {
        return $this->startAt;
    }

    /**
     * @param \DateTime $startAt
     */
    public function setStartAt($startAt)
    {
        $this->startAt = $startAt;
    }

    /**
     * @return \DateTime
     */
    public function getEndAt()
    {
        return $this->endAt;
    }

    /**
     * @param \DateTime $endAt
     */
    public function setEndAt($endAt)
    {
        $this->endAt = $endAt;
    }
    
    /** @ORM\PrePersist */
    public function timeStampOnPrePersist()
    {
        $this->setCreatedAt(new \DateTime());
    }

    /** @ORM\PreUpdate */
    public function timeStampOnPreUpdate()
    {
        $this->setModifiedAt(new \DateTime());
    }

}
