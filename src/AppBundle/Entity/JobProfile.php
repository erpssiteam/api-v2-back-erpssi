<?php
namespace AppBundle\Entity;

use  \Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Created by PhpStorm.
 * User: rislou
 * Date: 22/03/16
 * Time: 19:43
 */

/**
 * Class JobProfile
 * @package AppBundle\Entity
 * @ORM\Table("job_profile")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\JobProfileRepository")
 * @ORM\HasLifecycleCallbacks
 */
class JobProfile
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="label", type="string", length=100, nullable=false)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $label;

    /**
     * @var string
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var integer
     * @ORM\Column(name="salary", type="integer", nullable=true)
     */
    private $salary;

    /**
     * @var string
     * @ORM\Column(name="expertise", type="string", nullable=false)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $expertise;

    /**
     * @var string
     * @ORM\Column(name="information", type="string", nullable=true)
     */
    private $information;

    /**
     * @var boolean
     * @ORM\Column(name="published", type="boolean", options={ "default": FALSE })
     */
    private $published;

    /**
     * @var Apply[] | ArrayCollection
     * @ORM\OneToMany(targetEntity="Apply", mappedBy="jobProfile")
     */
    private $apply;

    /**
     * @ORM\ManyToOne(targetEntity="Employee")
     * @ORM\JoinColumn(name="id_employee", referencedColumnName="id")
     */
    private $employee;

    /**
     * @ORM\ManyToOne(targetEntity="Customer")
     * @ORM\JoinColumn(name="id_customer", referencedColumnName="id")
     */
    private $customer;

    /**
     * @var string
     * @ORM\Column(name="localisation", type="string", nullable=true)
     */
    private $localisation;

    /**
     * @var string
     * @ORM\Column(name="contract_type", type="string", nullable=false)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $contractType;

    /**
     * @var string
     * @ORM\Column(name="reference", type="string", nullable=true)
     */
    private $reference;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="modified_at",type="datetime", nullable=true)
     */
    protected $modifiedAt;

    /**
     * Construct JobProfile
     * @param Customer $customer
     * @param $label
     * @param $contractType
     */
    public function __construct(Customer $customer, $label, $contractType)
    {
        $this->setCustomer($customer);
        $this->setContractType($contractType);
        $this->setLabel($label);
        $this->apply = new ArrayCollection();
    }

    /**
     * @param Apply $apply
     */
    public function addApply(Apply $apply){

        if(!$this->apply->contains($apply)){
            $this->apply->add($apply);
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * @param int $salary
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;
    }

    /**
     * @return string
     */
    public function getExpertise()
    {
        return $this->expertise;
    }

    /**
     * @param string $expertise
     */
    public function setExpertise($expertise)
    {
        $this->expertise = $expertise;
    }

    /**
     * @return string
     */
    public function getInformation()
    {
        return $this->information;
    }

    /**
     * @param string $information
     */
    public function setInformation($information)
    {
        $this->information = $information;
    }

    /**
     * @return boolean
     */
    public function isPublished()
    {
        return $this->published;
    }

    /**
     * @param $published
     */
    public function setIsPublished($published)
    {
        $this->published = $published;
    }

    /**
     * @return Apply[]|ArrayCollection
     */
    public function getApply()
    {
        return $this->apply;
    }

    /**
     * @param Apply[]|ArrayCollection $apply
     */
    public function setApply($apply)
    {
        $this->apply = $apply;
    }

    /**
     * @return mixed
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param mixed $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @return string
     */
    public function getLocalisation()
    {
        return $this->localisation;
    }

    /**
     * @param string $localisation
     */
    public function setLocalisation($localisation)
    {
        $this->localisation = $localisation;
    }

    /**
     * @return string
     */
    public function getContractType()
    {
        return $this->contractType;
    }

    /**
     * @param string $contractType
     */
    public function setContractType($contractType)
    {
        $this->contractType = $contractType;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /**
     * @return mixed
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @param mixed $employee
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;
    }

    /** @ORM\PrePersist */
    public function timeStampOnPrePersist()
    {
        $this->setCreatedAt(new \DateTime());
    }

    /** @ORM\PreUpdate */
    public function timeStampOnPreUpdate()
    {
        $this->setModifiedAt(new \DateTime());
    }

}
