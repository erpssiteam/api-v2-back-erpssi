<?php
/**
 * Created by PhpStorm.
 * User: whiskey
 * Date: 24/03/17
 * Time: 14:24
 */
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class News
 * @package AppBundle\Entity
 * @ORM\Table("news")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NewsRepository")
 * @ORM\HasLifecycleCallbacks
 */
class News
{

    const NEWS_PUBLISHED_STATE = 1;
    const NEWS_UNPUBLISHED_STATE = 0;

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", length=100, nullable=false)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(name="content", type="text", nullable=false)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $content;

    /**
     * @var \DateTime
     * @ORM\Column(name="date_new", type="datetime", nullable=false)
     * @Assert\NotNull()
     */
    private $dateNew;

    /**
     * @var boolean
     * @ORM\Column(name="published", type="boolean", options={ "default": FALSE })
     */
    private $published;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="modified_at",type="datetime", nullable=true)
     */
    protected $modifiedAt;

    /**
     * News constructor.
     * @param string $title
     * @param string $content
     * @param \DateTime $dateNew
     */
    public function __construct($title, $content, \DateTime $dateNew)
    {
        $this->title = $title;
        $this->content = $content;
        $this->dateNew = $dateNew;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return \DateTime
     */
    public function getDateNew()
    {
        return $this->dateNew;
    }

    /**
     * @param \DateTime $dateNew
     */
    public function setDateNew($dateNew)
    {
        $this->dateNew = $dateNew;
    }

    /**
     * @return bool
     */
    public function isPublished()
    {
        return $this->published;
    }

    /**
     * @param bool $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /** @ORM\PrePersist */
    public function timeStampOnPrePersist()
    {
        $this->setCreatedAt(new \DateTime());
    }

    /** @ORM\PreUpdate */
    public function timeStampOnPreUpdate()
    {
        $this->setModifiedAt(new \DateTime());
    }

}
