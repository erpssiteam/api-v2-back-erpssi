<?php
/**
 * Created by PhpStorm.
 * User: reslene
 * Date: 24/03/2016
 * Time: 10:45
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\Skill;
use AppBundle\Entity\Employee;

/**
 * Class EmployeeSkill
 * @package AppBundle\Entity
 * @ORM\Table("employee_skill")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EmployeeSkillRepository")
 * @ORM\HasLifecycleCallbacks
 */
class EmployeeSkill
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Many Skills have One Employee.
     * @ManyToOne(targetEntity="Employee", inversedBy="employeeSkill")
     * @JoinColumn(name="employee_id", referencedColumnName="id")
     */
    private $employee;

    /**
     * @var string
     * @ORM\Column(name="level", type="string")
     */
    private $level;

    /**
     * @var boolean
     * @ORM\Column(name="validate", type="boolean")
     */
    private $validate;

    /**
     * @var string
     * @ORM\Column(name="description", type="string")
     */
    private $description;

    /**
     * @var \DateTime
     * @ORM\Column(name="date_skill", type="datetime")
     */
    private $dateSkill;

    /**
     * @var Skill
     * @ORM\ManyToOne(targetEntity="Skill")
     */
    private $skill;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="modified_at",type="datetime", nullable=true)
     */
    protected $modifiedAt;

    /**
     * EmployeeSkill constructor.
     */
    public function __construct()
    {

    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @param mixed $employee
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;
    }

    /**
     * @return string
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param string $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * @return bool
     */
    public function isValidate()
    {
        return $this->validate;
    }

    /**
     * @param bool $validate
     */
    public function setValidate($validate)
    {
        $this->validate = $validate;
    }


    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return \DateTime
     */
    public function getDateSkill()
    {
        return $this->dateSkill;
    }

    /**
     * @param \DateTime $dateSkill
     */
    public function setDateSkill($dateSkill)
    {
        $this->dateSkill = $dateSkill;
    }

    /**
     * @return \AppBundle\Entity\Skill
     */
    public function getSkill()
    {
        return $this->skill;
    }

    /**
     * @param \AppBundle\Entity\Skill $skill
     */
    public function setSkill($skill)
    {
        $this->skill = $skill;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /** @ORM\PrePersist */
    public function timeStampOnPrePersist()
    {
        $this->setCreatedAt(new \DateTime());
    }

    /** @ORM\PreUpdate */
    public function timeStampOnPreUpdate()
    {
        $this->setModifiedAt(new \DateTime());
    }


}
