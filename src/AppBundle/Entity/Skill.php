<?php
/**
 * Created by PhpStorm.
 * User: reslene
 * Date: 24/03/2016
 * Time: 10:45
 */

namespace AppBundle\Entity;

use  \Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Skill
 * @package AppBundle\Entity
 * @ORM\Table("skill")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SkillRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Skill
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="label", type="string", length=50, nullable=false)
     */
    private $label;

    /**
     * @var string
     * @ORM\Column(name="description", type="string")
     */
    private $description;

    /**
     * @var boolean
     * @ORM\Column(name="depreciated", type="string")
     */
    private $depreciated;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="modified_at",type="datetime", nullable=true)
     */
    protected $modifiedAt;

    /**
     * Skill constructor.
     * @param string $label
     */
    public function __construct($label)
    {
        $this->label = $label;
        $this->depreciated = FALSE;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDepreciated()
    {
        return $this->depreciated;
    }

    /**
     * @param mixed $depreciated
     */
    public function setDepreciated($depreciated)
    {
        $this->depreciated = $depreciated;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /** @ORM\PrePersist */
    public function timeStampOnPrePersist()
    {
        $this->setCreatedAt(new \DateTime());
    }

    /** @ORM\PreUpdate */
    public function timeStampOnPreUpdate()
    {
        $this->setModifiedAt(new \DateTime());
    }


}
