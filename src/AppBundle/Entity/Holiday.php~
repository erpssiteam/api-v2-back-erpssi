<?php
/**
 * Created by PhpStorm.
 * User: stagiaire
 * Date: 24/03/2016
 * Time: 10:45
 */

namespace AppBundle\Entity;

use  \Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Holiday
 * @package AppBundle\Entity
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\HolidayRepository")
 *
 */
class Holiday
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="holiday", cascade={})
     * @ORM\JoinColumn(name="idUser", referencedColumnName="id")
     * @Assert\Type(type="AppBundle\Entity\User")
     */
    private $user;

    /**
     * @var string
     * @ORM\Column(name="label", type="string", length=100, nullable=false)
     * @Assert\NotBlank()
     */
    private $label;

    /**
     * @var string
     * @ORM\Column(name="reason", type="string", length=100, nullable=false)
     * @Assert\NotBlank()
     */
    private $reason;

    /**
     * @var \DateTime
     * @ORM\Column(name="starting_date", type="datetime", nullable=false)
     * @Assert\NotBlank()
     */
    private $startingDate;

    /**
     * @var \DateTime
     * @ORM\Column(name="ending_date", type="datetime", nullable=false)
     * @Assert\NotBlank()
     */
    private $endingDate;

    /**
     * @var boolean
     * @ORM\Column(name="is_valid", type="boolean", nullable=false)
     */
    private $isValid;

    /**
     * Holiday constructor.
     */
    public function __construct()
    {

        $this->isValid = false;

    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
    }

    /**
     * @return \DateTime
     */
    public function getStartingDate()
    {
        return $this->startingDate;
    }

    /**
     * @param \DateTime $startingDate
     */
    public function setStartingDate($startingDate)
    {
        $this->startingDate = $startingDate;
    }

    /**
     * @return \DateTime
     */
    public function getEndingDate()
    {
        return $this->endingDate;
    }

    /**
     * @param \DateTime $endingDate
     */
    public function setEndingDate($endingDate)
    {
        $this->endingDate = $endingDate;
    }

    /**
     * @return boolean
     */
    public function isIsValid()
    {
        return $this->isValid;
    }

    /**
     * @param boolean $isValid
     */
    public function setIsValid($isValid)
    {
        $this->isValid = $isValid;
    }

    /**
     * @return bool
     */
    public function isLeft(){

        $now = new \DateTime();

        if($this->isValid === true && $now >= $this->startingDate && $now < $this->endingDate){
            return true;
        }else{
            return false;
        }
    }



}