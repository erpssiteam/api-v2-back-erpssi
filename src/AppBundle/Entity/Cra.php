<?php
/**
 * Created by PhpStorm.
 * User: reslene
 * Date: 24/03/2016
 * Time: 10:45
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Cra
 * @package AppBundle\Entity
 * @ORM\Table("cra")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CraRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Cra
{
    const CRA_TYPE_JOB_CDD = 1;
    const CRA_TYPE_JOB_INTERIM = 2;
    const CRA_TYPE_JOB_FREELANCE = 3;

    const STATE_PENDING = 1;
    const STATE_VALIDATED = 2;
    const STATE_DENIED = 3;

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", nullable=false)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(name="description", type="string", nullable=false)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $description;

    /**
     * @var integer
     * @ORM\Column(name="time_journey", type="integer", nullable=true)
     */
    private $timeJourney;

    /**
     * @var integer
     * @ORM\Column(name="distance", type="integer", nullable=true)
     */
    private $distance;

    /**
     * @var integer
     * @ORM\Column(name="type_job", type="integer", nullable=false, options={"default":1})
     * @Assert\NotNull()
     */
    private $typeJob = self::CRA_TYPE_JOB_CDD;

    /**
     * @var \DateTime
     * @ORM\Column(name="started_at", type="datetime", nullable=false)
     * @Assert\NotNull()
     */
    private $startedAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="ended_at", type="datetime", nullable=false)
     * @Assert\NotNull()
     */
    private $endedAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="validated_at", type="datetime", nullable=true)
     */
    private $validatedAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="Customer")
     * @ORM\JoinColumn(name="id_customer", referencedColumnName="id")
     */
    private $customer;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Employee")
     * @ORM\JoinColumn(name="id_employee", referencedColumnName="id")
     */
    private $employee;

    /**
     * @var \DateTime
     * @ORM\Column(name="modified_at",type="datetime", nullable=true)
     */
    protected $modifiedAt;

    /**
     * @var integer
     * @ORM\Column(name="state", type="integer", nullable=false,  options={"default": 1})
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $state = self::STATE_PENDING;

    function __construct(Employee $employee, Customer $customer, $title)
    {
        $this->setTitle($title);
        $this->setEmployee($employee);
        $this->setState(self::STATE_PENDING);
        $this->setCustomer($customer);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getTimeJourney()
    {
        return $this->timeJourney;
    }

    /**
     * @param string $timeJourney
     */
    public function setTimeJourney($timeJourney)
    {
        $this->timeJourney = $timeJourney;
    }

    /**
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param int $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return integer
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * @param integer $distance
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;
    }

    /**
     * @return int
     */
    public function getTypeJob()
    {
        return $this->typeJob;
    }

    /**
     * @param int $typeJob
     */
    public function setTypeJob($typeJob)
    {
        $this->typeJob = $typeJob;
    }

    /**
     * @return \DateTime
     */
    public function getStartedAt()
    {
        return $this->startedAt;
    }

    /**
     * @param \DateTime $startedAt
     */
    public function setStartedAt($startedAt)
    {
        $this->startedAt = $startedAt;
    }

    /**
     * @return \DateTime
     */
    public function getEndedAt()
    {
        return $this->endedAt;
    }

    /**
     * @param \DateTime $endedAt
     */
    public function setEndedAt($endedAt)
    {
        $this->endedAt = $endedAt;
    }

    /**
     * @return \DateTime
     */
    public function getValidatedAt()
    {
        return $this->validatedAt;
    }

    /**
     * @param \DateTime $validatedAt
     */
    public function setValidatedAt($validatedAt)
    {
        $this->validatedAt = $validatedAt;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param mixed $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @param mixed $employee
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /** @ORM\PrePersist */
    public function timeStampOnPrePersist()
    {
        $this->setCreatedAt(new \DateTime());
    }

    /** @ORM\PreUpdate */
    public function timeStampOnPreUpdate()
    {
        $this->setModifiedAt(new \DateTime());
    }


}
