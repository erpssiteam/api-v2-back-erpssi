<?php
/**
 * Created by PhpStorm.
 * User: stagiaire
 * Date: 24/03/2016
 * Time: 10:45
 */

namespace AppBundle\Entity;

use  \Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Holiday
 * @package AppBundle\Entity
 * @ORM\Table("holiday")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\HolidayRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Holiday
{
    const STATE_PENDING = 1;
    const STATE_VALIDATED = 2;
    const STATE_DENIED = 3;

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(name="asked_at", type="datetime", nullable=false)
     */
    private $askedAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="validated_at", type="datetime", nullable=true)
     */
    private $validatedAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="started_at", type="datetime", nullable=false)
     * @Assert\NotNull()
     */
    private $startedAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="ended_at", type="datetime", nullable=false)
     * @Assert\NotNull()
     */
    private $endedAt;

    /**
     * @var integer
     * @ORM\Column(name="reason", type="integer", nullable=false)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $reason;
    
    /**
     * @var integer
     * @ORM\Column(name="state", type="integer", nullable=false,  options={"default": 1})
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $state = self::STATE_PENDING;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Employee", inversedBy="holiday")
     * @ORM\JoinColumn(name="id_employee", referencedColumnName="id", onDelete="CASCADE")
     * @Assert\Type(type="AppBundle\Entity\Employee")
     */
    private $employee;


    /**
     * Holiday constructor.
     * @param Employee $employee
     */
    public function __construct(Employee $employee)
    {
        $this->setEmployee($employee);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getAskedAt()
    {
        return $this->askedAt;
    }

    /**
     * @param \mixed $askedAt
     */
    public function setAskedAt($askedAt)
    {
        $this->askedAt = $askedAt;
    }

    /**
     * @return mixed
     */
    public function getValidatedAt()
    {
        return $this->validatedAt;
    }

    /**
     * @param \mixed $validatedAt
     */
    public function setValidatedAt($validatedAt)
    {
        $this->validatedAt = $validatedAt;
    }

    /**
     * @return \mixed
     */
    public function getStartedAt()
    {
        return $this->startedAt;
    }

    /**
     * @param \mixed $startedAt
     */
    public function setStartedAt($startedAt)
    {
        $this->startedAt = $startedAt;
    }

    /**
     * @return \mixed
     */
    public function getEndedAt()
    {
        return $this->endedAt;
    }

    /**
     * @param \mixed $endedAt
     */
    public function setEndedAt($endedAt)
    {
        $this->endedAt = $endedAt;
    }

    /**
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param int $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @return int
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param int $reason
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
    }
    
    /**
     * @param mixed $employee
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;
    }

    /** @ORM\PrePersist */
    public function timeStampOnPrePersist()
    {
        $this->setAskedAt(new \DateTime());
    }


}
