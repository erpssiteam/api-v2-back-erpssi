<?php
/**
 * Created by PhpStorm.
 * User: reslene
 * Date: 24/03/2016
 * Time: 10:45
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ExpenseAccount
 * @package AppBundle\Entity
 * @ORM\Table("expense_account")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ExpenseAccountRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ExpenseAccount
{
    const EXPENSE_ACCOUNT_PENDING_STATE = 1;
    const EXPENSE_ACCOUNT_VALIDATED_STATE = 2;
    const EXPENSE_ACCOUNT_DENIED_STATE = 3;

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Many ExpenseAccount have One Employee.
     * @ManyToOne(targetEntity="Employee", inversedBy="expenseAccount")
     * @JoinColumn(name="employee_id", referencedColumnName="id")
     * @Assert\NotNull()
     */
    private $employee;

    /**
     * @var string
     * @ORM\Column(name="label", type="string", nullable=false)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $label;

    /**
     * @var string
     * @ORM\Column(name="description", type="string", nullable=false)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $description;

    /**
     * @var float
     * @ORM\Column(name="total", type="float", nullable=false)
     * @Assert\NotNull()
     */
    private $total;

    /**
     * @var float
     * @ORM\Column(name="tva", type="float", nullable=false)
     * @Assert\NotNull()
     */
    private $tva;

    /**
     * @var string
     * @ORM\Column(name="image", type="string", nullable=true)
     */
    private $image;

    /**
     * @var \DateTime
     * @ORM\Column(name="date_expense_account", type="datetime")
     */
    private $dateExpenseAccount;

    /**
     * @var integer
     * @ORM\Column(name="state", type="integer", nullable=false, options={"default":1})
     * @Assert\NotNull()
     */
    private $state = self::EXPENSE_ACCOUNT_PENDING_STATE;

    /**
     * @var \DateTime
     * @ORM\Column(name="validated_at", type="datetime", nullable=true)
     */
    private $validatedAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="modified_at",type="datetime", nullable=true)
     */
    protected $modifiedAt;

    /**
     * ExpenseAccount constructor.
     * @param $employee
     * @param string $label
     * @param float $total
     * @param float $tva
     */
    public function __construct($employee, $label, $total, $tva)
    {
        $this->setEmployee($employee);
        $this->setLabel($label);
        $this->setTotal($total);
        $this->setTva($tva);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @param mixed $employee
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return float
     */
    public function getTva()
    {
        return $this->tva;
    }

    /**
     * @param float $tva
     */
    public function setTva($tva)
    {
        $this->tva = $tva;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getDateExpenseAccount()
    {
        return $this->dateExpenseAccount;
    }

    /**
     * @param mixed $dateExpenseAccount
     */
    public function setDateExpenseAccount($dateExpenseAccount)
    {
        $this->dateExpenseAccount = $dateExpenseAccount;
    }

    /**
     * @return integer
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param integer $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }


    /**
     * @return \DateTime
     */
    public function getValidatedAt()
    {
        return $this->validatedAt;
    }

    /**
     * @param \DateTime $validatedAt
     */
    public function setValidatedAt($validatedAt)
    {
        $this->validatedAt = $validatedAt;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /** @ORM\PrePersist */
    public function timeStampOnPrePersist()
    {
        $this->setCreatedAt(new \DateTime());
    }

    /** @ORM\PreUpdate */
    public function timeStampOnPreUpdate()
    {
        $this->setModifiedAt(new \DateTime());
    }


}
