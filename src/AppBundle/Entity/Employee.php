<?php
/**
 * Created by PhpStorm.
 * User: whiskey
 * Date: 14/02/17
 * Time: 11:12
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;

/**
 * Class Employee
 * @package AppBundle\Entity
 * @ORM\Table("employee")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EmployeeRepository")
 */
class Employee extends Person
{
    /**
     * @var string
     * @ORM\Column(name="cin", type="string", length=55, nullable=true)
     */
    private $cin;

    /**
     * @var string
     * @ORM\Column(name="gmail", type="string", length=55, nullable=true)
     */
    private $gmail;

    /**
     * @var string
     * @ORM\Column(name="gmail_password", type="string", length=55, nullable=true)
     */
    private $gmailPassword;

    /**
     * @var string
     * @ORM\Column(name="securite_sociale", type="string", length=55, nullable=true)
     */
    private $securiteSociale;

    /**
     * @var \DateTime
     * @ORM\Column(name="date_embauche", type="datetime", nullable=true)
     */
    private $dateEmbauche;

   /**
    * @var string
    * @ORM\Column(name="situation_familiale", type="string", nullable=true)
    */

    private $situationFamiliale;

    /**
     * @var \DateTime
     * @ORM\Column(name="date_collaborater", type="datetime", nullable=true)
     */
    private $dateCollaborater;

    /**
     * @var \DateTime
     * @ORM\Column(name="date_manager", type="datetime", nullable=true)
     */
    private $dateManager;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Employee")
     * @ORM\JoinColumn(name="id_managed_by", referencedColumnName="id", onDelete="set null")
     */
    private $managedBy;

    /**
     * Employee constructor.
     */
    public function __construct($firstname, $lastname, \DateTime $birthdayDate, $email, $address, $zipCode, $city, User $user, Employee $managedBy = null)
    {
        parent::__construct($firstname, $lastname, $birthdayDate, $email, $address, $zipCode, $city, $user);
        $this->employeeSkill = new ArrayCollection();
        if($managedBy instanceof Employee && $managedBy != null) {
            $this->setManagedBy($managedBy);
        }
    }

    /**
     * @return Employee
     */
    public function getManagedBy()
    {
        return $this->managedBy;
    }

    /**
     * @param mixed $employee
     */
    public function setManagedBy($managedBy)
    {
        $this->managedBy = $managedBy;
    }


    /**
     * @return EmployeeSkill[]
     */
    public function getEmployeeSkill()
    {
        return $this->employeeSkill;
    }

    /**
     * @param EmployeeSkill[] $employeeSkill
     */
    public function setEmployeeSkill($employeeSkill)
    {
        $this->employeeSkill = $employeeSkill;
    }

    /**
     * @return string
     */
    public function getCin()
    {
        return $this->cin;
    }

    /**
     * @param string $cin
     */
    public function setCin($cin)
    {
        $this->cin = $cin;
    }

    /**
     * @return string
     */
    public function getGmail()
    {
        return $this->gmail;
    }

    /**
     * @param string $gmail
     */
    public function setGmail($gmail)
    {
        $this->gmail = $gmail;
    }

    /**
     * @return string
     */
    public function getGmailPassword()
    {
        return $this->gmailPassword;
    }

    /**
     * @param string $gmailPassword
     */
    public function setGmailPassword($gmailPassword)
    {
        $this->gmailPassword = $gmailPassword;
    }

    /**
     * @return string
     */
    public function getSecuriteSociale()
    {
        return $this->securiteSociale;
    }

    /**
     * @param string $securiteSociale
     */
    public function setSecuriteSociale($securiteSociale)
    {
        $this->securiteSociale = $securiteSociale;
    }

    /**
     * @return \DateTime
     */
    public function getDateEmbauche()
    {
        return $this->dateEmbauche;
    }

    /**
     * @param \DateTime $dateEmbauche
     */
    public function setDateEmbauche($dateEmbauche)
    {
        $this->dateEmbauche = $dateEmbauche;
    }

    /**
     * @return string
     */
    public function getSituationFamiliale()
    {
        return $this->situationFamiliale;
    }

    /**
     * @param string $situationFamiliale
     */
    public function setSituationFamiliale($situationFamiliale)
    {
        $this->situationFamiliale = $situationFamiliale;
    }


    /**
     * @return \DateTime
     */
    public function getDateCollaborater()
    {
        return $this->dateCollaborater;
    }

    /**
     * @param \DateTime $dateCollaborater
     */
    public function setDateCollaborater($dateCollaborater)
    {
        $this->dateCollaborater = $dateCollaborater;
    }

    /**
     * @return \DateTime
     */
    public function getDateManager()
    {
        return $this->dateManager;
    }

    /**
     * @param \DateTime $dateManager
     */
    public function setDateManager($dateManager)
    {
        $this->dateManager = $dateManager;
    }



}
