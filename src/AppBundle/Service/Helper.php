<?php
/**
 * Created by PhpStorm.
 * User: reslene
 * Date: 25/08/16
 * Time: 11:33
 */

namespace AppBundle\Service;
use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Exception\InvalidArgumentException;

class Helper
{

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * @param Serializer $serializer
     */
    public function setSerializer(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param RequestStack $requestStack
     */
    public function setRequest(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * @param $data
     * @param $status
     * @param $message
     * @return JsonResponse
     */
    public function createResponse($data, $status, $message)
    {
        return new JsonResponse([
            'data' => $this->objectToJson($data),
            'message' => $message
        ], $status);
    }

    /**
     * @param $trans
     * @return string
     */
    public function transResponse($trans)
    {
        return $this->translator->trans($trans, [], 'common');
    }

    /**
     * @return mixed
     */
    public function getFrontDataSerialized() {

        if($this->request->isXmlHttpRequest() && $this->request->get('data')) {
            return $this->request->get('data');
        } else {
            throw new BadRequestHttpException();
        }
        /**
         * --------------------------------------------------------------------------------------------------------
         * // TODO (pls, do not delete this comment until final decision)
         * Alternatively, when the front will communicate with the back, the data sent might be need to be decoded.
         * --------------------------------------------------------------------------------------------------------
            // $data = $this->request->get('data');
            // return json_decode($data);
         * -------------------------------------------------------------------------------------------------------
         */
    }

    /**
     * @param $obj
     * @return string|\Symfony\Component\Serializer\Encoder\scalar
     */
    private function objectToJson($obj) {
        if(!is_object($obj)) {
            return $obj;
        }
        return $this->serializer->serialize($obj, 'json');
    }

}
