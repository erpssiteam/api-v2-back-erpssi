<?php
/**
 * Created by PhpStorm.
 * User: rislou
 * Date: 20/03/17
 * Time: 11:51
 */

namespace AppBundle\Service;


use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    private $targetDir;

    /**
     * @param $targetDir
     */
    public function __construct($targetDir)
    {
        $this->targetDir = $targetDir;
    }

    /**
     * @param $file
     * @return string
     */
    public function upload($file)
    {
        if(!$file instanceof UploadedFile) {
            $file = new UploadedFile(isset($file['path']) ? $file['path'] : $file['tmp_name'], isset($file['originalName']) ? $file['originalName'] : $file['name'] , isset($file['mimeType']) ? $file['mimeType'] : $file['type']);
        }

        $fileName = md5(uniqid()).'.'.$file->guessExtension();

        $file->move($this->targetDir, $fileName);

        return $fileName;
    }

    /**
     * @return mixed
     */
    public function getTargetDir()
    {
        return $this->targetDir;
    }

}