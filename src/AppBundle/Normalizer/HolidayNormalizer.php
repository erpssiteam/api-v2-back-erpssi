<?php
/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 04/11/16
 * Time: 14:25
 */

namespace AppBundle\Normalizer;

use AppBundle\Entity\Employee;
use AppBundle\Entity\Holiday;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use \Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\scalar;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

class HolidayNormalizer extends SerializerAwareNormalizer  implements NormalizerInterface, DenormalizerInterface {

    const UPDATE = 'update';
    const CREATE = 'create';
    
    /**
     * @param Holiday $object
     * @param null $format
     * @param array $context
     * @return array|scalar
     */
    public function normalize($object, $format = null, array $context = array())
    {
        return [
            'id' => $object->getId(),
            'asked_at' => $object->getAskedAt()->format("d-m-Y"),
            'validated_at' => $object->getValidatedAt() != null ? $object->getValidatedAt()->format("d-m-Y") :  null,
            'started_at' => $object->getStartedAt()->format("d-m-Y"),
            'ended_at' => $object->getEndedAt()->format("d-m-Y"),
            'state' => $object->getState(),
            'reason' => $object->getReason(),
            'employee' => $this->serializer->normalize($object->getEmployee(), $format, $context)
        ];

    }

    /**
     * @param mixed $data
     * @param null $format
     * @return bool
     */
    public function supportsNormalization($data, $format = null){

        return ($data instanceof Holiday);
    }

    /**
     * @param mixed $data
     * @param string $class
     * @param null $format
     * @param array $context
     * @return Holiday|mixed|object
     */
    public function denormalize($data, $class, $format = null, array $context = array())
    {
        $startedAt = new \Datetime($data['started_at']);
        $endedAt = new \Datetime($data['ended_at']);
        $state = $data['state'];
        $reason = $data['reason'];
        
        /** @var Employee $employee */

        if($context['strategy'] == self::CREATE ) {
            $employee = $context['employee'];
            $holiday = new Holiday($employee);
        } else {
            $holiday = $context['holiday'];
        }

        $holiday->setState($state);
        $holiday->setStartedAt($startedAt);
        $holiday->setReason($reason);
        $holiday->setEndedAt($endedAt);

        return $holiday;

    }

    /**
     * @param mixed $data
     * @param string $type
     * @param null $format
     * @return bool
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type == Holiday::class;
    }


}