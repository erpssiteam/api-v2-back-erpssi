<?php
/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 04/11/16
 * Time: 15:12
 */


namespace AppBundle\Normalizer;

use AppBundle\Entity\ExpenseAccount;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use \Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\scalar;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

class ExpenseAccountNormalizer extends SerializerAwareNormalizer  implements NormalizerInterface, DenormalizerInterface {


    const UPDATE = 'update';
    const CREATE = 'create';

    private $pathToShare;

    /**
     * ExpenseAccountNormalizer constructor.
     */
    public function __construct($pathToShare)
    {
        $this->pathToShare = $pathToShare;
    }


    /**
     * @param ExpenseAccount $object
     * @param null $format
     * @param array $context
     * @return array|scalar
     */
    public function normalize($object, $format = null, array $context = array())
    {
        $path_image = null;
        if($object->getImage() != null)
            $path_image = $_SERVER['SERVER_NAME'].$this->pathToShare.'/'.$object->getImage();

        return [
            'id' => $object->getId(),
            'employee' => $this->serializer->normalize($object->getEmployee(), $format, $context),
            'label' => $object->getLabel(),
            'description' => $object->getDescription(),
            'total' => $object->getTotal(),
            'tva' => $object->getTva(),
            'image' => $path_image,
            'date_expense_account' => $object->getDateExpenseAccount()->format("d-m-Y"),
            'state' => $object->getState(),
            'is_validated' => $object->getValidatedAt(),
            'validated_at' => $object->getValidatedAt(),
            'created_at' => $object->getCreatedAt()
        ];

    }

    public function supportsNormalization($data, $format = null){

        return ($data instanceof ExpenseAccount);
    }

    public function denormalize($data, $class, $format = null, array $context = array())
    {

        $label= $data['label'];
        $description = $data['description'];
        $total = $data['total'];
        $tva = $data['tva'];
        $dateExpenseAccount = new \DateTime($data['date_expense_account']);

        if ($context['strategy'] == self::CREATE){
            $employee = $context['employee'];

            $expenseAccount = new ExpenseAccount($employee, $label, $total, $tva);
            $expenseAccount->setDescription($description);
            $expenseAccount->setDateExpenseAccount($dateExpenseAccount);
        } else {
            /* @var ExpenseAccount $expenseAccount */
            $expenseAccount = $context['expense_account'];
            $expenseAccount->setLabel($label);
            $expenseAccount->setDescription($description);
            $expenseAccount->setTotal($total);
            $expenseAccount->setTva($tva);
            $expenseAccount->setDateExpenseAccount($dateExpenseAccount);
        }

        return $expenseAccount;
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type == ExpenseAccount::class;
    }


}