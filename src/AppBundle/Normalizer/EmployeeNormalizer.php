<?php
/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 04/11/16
 * Time: 15:12
 */


namespace AppBundle\Normalizer;

use AppBundle\Entity\Employee;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use \Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\scalar;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

class EmployeeNormalizer extends SerializerAwareNormalizer  implements NormalizerInterface, DenormalizerInterface {

    const UPDATE = 'update';
    const CREATE = 'create';

    /**
     * @param Employee $object
     * @param null $format
     * @param array $context
     * @return array|scalar
     */
    public function normalize($object, $format = null, array $context = array())
    {
        return [
            'id' => $object->getId(),
            'user' => $this->serializer->normalize($object->getUser(), $format, $context),
            'first_name' => $object->getFirstname(),
            'last_name' => $object->getLastname(),
            'birthday_date' => $object->getBirthdayDate(),
            'email' => $object->getEmail(),
            'address' => $object->getAddress(),
            'zip_code' => $object->getZipCode(),
            'city' => $object->getCity(),
            'cin' => $object->getCin(),
            'gmail' => $object->getGmail(),
            'date_embauche' => $object->getDateEmbauche(),
            'situation_familiale' => $object->getSituationFamiliale(),
            'date_collaborater' => $object->getDateCollaborater(),
            'date_manager' => $object->getDateManager()
        ];


    }

    public function supportsNormalization($data, $format = null){

        return ($data instanceof Employee);
    }

    public function denormalize($data, $class, $format = null, array $context = array())
    {

        $first_name = $data['first_name'];
        $last_name = $data['last_name'];
        $birthday_date = new \DateTime($data['birthday_date']);
        $email = $data['email'];
        $address = $data['address'];
        $zip_code = $data['zip_code'];
        $city = $data['city'];
        $gmail = $data['gmail'];
        $gmail_password = $data['gmail_password'];
        $date_embauche = $data['date_embauche'] ? new \DateTime($data['date_embauche']) : null;
        $situation_familiale = $data['situation_familiale'];
        $date_collaborater = $data['date_collaborater'] ? new \DateTime($data['date_collaborater']) : null;
        $date_manager = $data['date_manager'] ? new \DateTime($data['date_manager']) : null;
        $user = $context['user'];

        if($context['strategy'] == self::CREATE ) {
            $employee = new Employee($first_name, $last_name, $birthday_date, $email, $address, $zip_code, $city, $user);
        } else {
            /* @var Employee $employee */
            $employee = $context['employee'];
            $employee->setFirstname($first_name);
            $employee->setLastname($last_name);
            $employee->setBirthdayDate($birthday_date);
            if($employee->getEmail() != $email) $employee->setEmail($email);
            $employee->setAddress($address);
            $employee->setZipCode($zip_code);
            $employee->setCity($city);
        }

        $employee->setGmail($gmail);
        $employee->setGmailPassword($gmail_password);
        $employee->setDateEmbauche($date_embauche);
        $employee->setSituationFamiliale($situation_familiale);
        $employee->setDateCollaborater($date_collaborater);
        $employee->setDateManager($date_manager);

        return $employee;


    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type == Employee::class;
    }


}