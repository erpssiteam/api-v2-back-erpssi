<?php
/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 04/11/16
 * Time: 15:12
 */


namespace AppBundle\Normalizer;

use AppBundle\Entity\Skill;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use \Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\scalar;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

class SkillNormalizer extends SerializerAwareNormalizer  implements NormalizerInterface, DenormalizerInterface {


    const UPDATE = 'update';
    const CREATE = 'create';

    /**
     * @param Skill $object
     * @param null $format
     * @param array $context
     * @return array|scalar
     */
    public function normalize($object, $format = null, array $context = array())
    {
        return [
            'id' => $object->getId(),
            'label' => $object->getLabel(),
            'description' => $object->getDescription(),
            'modified_at' => $object->getModifiedAt(),
            'created_at' => $object->getCreatedAt()
        ];

    }

    public function supportsNormalization($data, $format = null){

        return ($data instanceof Skill);
    }

    public function denormalize($data, $class, $format = null, array $context = array())
    {
        $label= $data['label'];
        $description = $data['description'];

        if ($context['strategy'] == self::CREATE){
            $skill = new Skill($label);
            $skill->setDescription($description);
        } else {
            /* @var Skill $skill */
            $skill = $context['skill'];
            $skill->setLabel($label);
            $skill->setDescription($description);
        }

        return $skill;
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type == Skill::class;
    }


}