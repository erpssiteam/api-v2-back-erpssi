<?php
/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 04/11/16
 * Time: 15:12
 */


namespace AppBundle\Normalizer;

use AppBundle\Entity\Customer;
use AppBundle\Entity\User;
use \Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\scalar;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;
use Symfony\Component\Validator\Constraints\DateTime;

class CustomerNormalizer extends SerializerAwareNormalizer  implements NormalizerInterface, DenormalizerInterface {

    const UPDATE = 'update';
    const CREATE = 'create';

    /**
     * @param Customer $object
     * @param null $format
     * @param array $context
     * @return array|scalar
     */
    public function normalize($object, $format = null, array $context = array())
    {
        return [
            'id' => $object->getId(),
            'user' => $this->serializer->normalize($object->getUser(), $format, $context),
            'first_name' => $object->getFirstname(),
            'last_name' => $object->getLastname(),
            'birthday_date' => $object->getBirthdayDate(),
            'email' => $object->getEmail(),
            'address' => $object->getAddress(),
            'zip_code' => $object->getZipCode(),
            'city' => $object->getCity(),
            'siret' => $object->getSiret(),
            'siren' => $object->getSiren(),
            'webiste' => $object->getWebsite(),
            'contact_name' => $object->getContactName(),
            'contact_phone' => $object->getContactPhone(),
            'contact_email' => $object->getContactEmail(),
            'general_email' => $object->getGeneralEmail(),
        ];


    }

    public function supportsNormalization($data, $format = null){

        return ($data instanceof Customer);
    }

    public function denormalize($data, $class, $format = null, array $context = array())
    {
        $first_name = $data['first_name'];
        $last_name = $data['last_name'];
        $birthday_date = new \DateTime($data['birthday_date']);
        $email = $data['email'];
        $address = $data['address'] ?  $data['address'] : null;
        $zip_code = $data['zip_code'] ?  $data['zip_code'] : null;
        $city = $data['city'] ?  $data['city'] : null;
        $siret = $data['siret'];
        $siren = $data['siren'];
        $website = $data['website'];
        $contact_name = $data['contact_name'];
        $contact_phone = $data['contact_phone'];
        $contact_email = $data['contact_email'];
        $general_email = $data['general_email'];
        $user = $context['user'];

        if($context['strategy'] == self::CREATE ) {
            $customer = new Customer($first_name, $last_name, $birthday_date, $email, $address, $zip_code, $city, $user);
        } else {
            /* @var Customer $customer*/
            $customer = $context['customer'];
            $customer->setFirstname($first_name);
            $customer->setLastname($last_name);
            $customer->setBirthdayDate($birthday_date);
            if($customer->getEmail() != $email) $customer->setEmail($email);
            $customer->setAddress($address);
            $customer->setZipCode($zip_code);
            $customer->setCity($city);
        }

        $customer->setSiret($siret);
        $customer->setSiren($siren);
        $customer->setWebsite($website);
        $customer->setContactName($contact_name);
        $customer->setContactPhone($contact_phone);
        $customer->setContactEmail($contact_email);
        $customer->setGeneralEmail($general_email);

        return $customer;
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type == Customer::class;
    }


}