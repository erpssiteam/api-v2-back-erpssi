<?php
/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 04/11/16
 * Time: 15:12
 */

namespace AppBundle\Normalizer;

use AppBundle\Entity\Customer;
use AppBundle\Entity\JobProfile;
use ClassesWithParents\D;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use \Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\scalar;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

class JobProfileNormalizer extends SerializerAwareNormalizer  implements NormalizerInterface, DenormalizerInterface {

    const UPDATE = 'update';
    const CREATE = 'create';

    /**
     * @param JobProfile $object
     * @param null $format
     * @param array $context
     * @return array|scalar
     */
    public function normalize($object, $format = null, array $context = array())
    {
        return [
            'id' => $object->getId(),
            'label' => $object->getLabel(),
            'description' => $object->getDescription(),
            'salary' => $object->getSalary(),
            'expertise' => $object->getExpertise(),
            'information' => $object->getInformation(),
            'is_published' => $object->isPublished(),
            'customer' => $this->serializer->normalize($object->getCustomer(), $format, $context),
            'created_at' => $object->getCreatedAt(),
            'modified_at' => $object->getModifiedAt()
        ];

    }

    /**
     * @param mixed $data
     * @param null $format
     * @return bool
     */
    public function supportsNormalization($data, $format = null){
        return ($data instanceof JobProfile);
    }

    /**
     * @param mixed $data
     * @param string $class
     * @param null $format
     * @param array $context
     * @return JobProfile
     */
    public function denormalize($data, $class, $format = null, array $context = array())
    {
        $label = $data['label'];
        $description = $data['description'];
        $salary = $data['salary'];
        $expertise = $data['expertise'];
        $information = $data['information'];
        $published = $data['published'];
        /** @var Customer $customer */
        $customer = $context['customer'];
        $localisation = $data['localisation'];
        $contractType = $data['contract_type'];

        if($context['strategy'] == self::CREATE ) {
            $jobProfile = new JobProfile($customer, $label, $contractType);
        } else {
            $jobProfile = $context['jobProfile'];
        }

        $jobProfile->setLabel($label);
        $jobProfile->setDescription($description);
        $jobProfile->setSalary($salary);
        $jobProfile->setExpertise($expertise);
        $jobProfile->setInformation($information);
        $jobProfile->setIsPublished($published);
        $jobProfile->setLocalisation($localisation);

        return $jobProfile;

    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type == JobProfile::class;
    }


}