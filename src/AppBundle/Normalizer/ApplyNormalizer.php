<?php
/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 04/11/16
 * Time: 14:25
 */

namespace AppBundle\Normalizer;

use AppBundle\Entity\Apply;
use AppBundle\Entity\Offer;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use \Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\scalar;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

class ApplyNormalizer extends SerializerAwareNormalizer  implements NormalizerInterface, DenormalizerInterface {

    const UPDATE = 'update';
    const CREATE = 'create';


    private $pathToShare;

    /**
     * ApplyNormalizer constructor.
     */
    public function __construct($pathToShare)
    {
        $this->pathToShare = $pathToShare;
    }

    /**
     * @param Apply $object
     * @param null $format
     * @param array $context
     * @return array|scalar
     */
    public function normalize($object, $format = null, array $context = array())
    {
        $path_cv = null;

        if($object->getCv() != null)
            $path_cv = $_SERVER['SERVER_NAME'].$this->pathToShare.'/'.$object->getCv();

        return [
            'id' => $object->getId(),
            'cv' => $path_cv,
            'firstname' => $object->getFirstname(),
            'lastname' => $object->getLastname(),
            'name' => $object->getName(),
            'birthday_date' => $object->getBirthdayDate()->format('d-m-Y'),
            'email' => $object->getEmail(),
            'address' => $object->getAddress(),
            'zip_code' => $object->getZipCode(),
            'city' => $object->getCity(),
            'state' => $object->getState(),
            'offer' => $this->serializer->normalize($object->getOffer(), $format, $context)
        ];

    }

    /**
     * @param mixed $data
     * @param null $format
     * @return bool
     */
    public function supportsNormalization($data, $format = null){

        return ($data instanceof Apply);
    }

    /**
     * @param mixed $data
     * @param string $class
     * @param null $format
     * @param array $context
     * @return Apply|mixed|object
     */
    public function denormalize($data, $class, $format = null, array $context = array())
    {
        $firstname = $data['firstname'];
        $lastname = $data['lastname'];
        $name = $data['name'];
        $birthdayDate = new \DateTime($data['birthday_date']);
        $email = $data['email'];
        $address = $data['address'];
        $zipCode = $data['zip_code'];
        $city = $data['city'];
        $state = isset($data['state']) ? $data['state'] : Apply::APPLY_PENDING_STATE;

        /** @var Offer $offer */
        $offer = isset($context['offer']) ? $context['offer'] : null;

        if($context['strategy'] == self::CREATE ) {
            $apply = new Apply($firstname, $lastname, $email, $birthdayDate, $offer);
        } else {
            $apply = $context['apply'];
        }

        $apply->setName($name);
        $apply->setFirstname($firstname);
        $apply->setLastname($lastname);
        $apply->setEmail($email);
        $apply->setBirthdayDate($birthdayDate);
        $apply->setAddress($address);
        $apply->setZipCode($zipCode);
        $apply->setCity($city);
        $apply->setState($state);

        return $apply;

    }

    /**
     * @param mixed $data
     * @param string $type
     * @param null $format
     * @return bool
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type == Apply::class;
    }


}