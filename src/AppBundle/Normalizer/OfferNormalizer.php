<?php
/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 04/11/16
 * Time: 14:25
 */

namespace AppBundle\Normalizer;

use AppBundle\Entity\Offer;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use \Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\scalar;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

class OfferNormalizer extends SerializerAwareNormalizer  implements NormalizerInterface, DenormalizerInterface {

    const UPDATE = 'update';
    const CREATE = 'create';
    
    /**
     * @param Offer $object
     * @param null $format
     * @param array $context
     * @return array|scalar
     */
    public function normalize($object, $format = null, array $context = array())
    {
       return [
           'id' => $object->getId(),
           'job_profile' => $this->serializer->normalize($object->getJobProfile(), $format, $context),
           'start_at' => $object->getStartAt()->format("d-m-Y"),
           'end_at' => $object->getEndAt()->format("d-m-Y"),
           'created_at' => $object->getCreatedAt(),
           'modified_at' => $object->getModifiedAt(),
           'is_valid' => $object->isValid(),
           'reference' => $object->getReference(),
           'name' => $object->getName(),
           'consulted' => $object->getConsulted()
       ];
    }

    /**
     * @param mixed $data
     * @param null $format
     * @return bool
     */
    public function supportsNormalization($data, $format = null){

        return ($data instanceof Offer);
    }

    /**
     * @param mixed $data
     * @param string $class
     * @param null $format
     * @param array $context
     * @return Apply|mixed|object
     */
    public function denormalize($data, $class, $format = null, array $context = array())
    {

        if ($context['strategy'] == self::CREATE){
            $jobProfile = $context['jobProfile'];
            $offer = new Offer($jobProfile);
            $offer = $this->setOffer($offer, $data);

        } else {
            $offer = $this->setOffer($context['offer'], $data);
        }

        return $offer;
    }

    /**
     * @param Offer $offer
     * @param $data
     * @return Offer
     */
    private function setOffer(Offer $offer, $data) {
        $offer->setStartAt(new \DateTime($data['start_at']));
        $offer->setEndAt(new \DateTime($data['end_at']));
        $offer->setCreatedAt(new \DateTime($data['created_at']));
        $offer->setModifiedAt(new \DateTime($data['modified_at']));
        $offer->setReference($data['reference']);
        $offer->setValid($data['is_valid']);
        $offer->setName($data['name']);

        return $offer;
    }

    /**
     * @param mixed $data
     * @param string $type
     * @param null $format
     * @return bool
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type == Offer::class;
    }


}