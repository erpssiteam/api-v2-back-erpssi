<?php
/**
 * Created by PhpStorm.
 * User: whiskey
 * Date: 24/03/17
 * Time: 14:39
 */

namespace AppBundle\Normalizer;

use AppBundle\Entity\News;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use \Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\scalar;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

class NewsNormalizer extends SerializerAwareNormalizer  implements NormalizerInterface, DenormalizerInterface
{

    const UPDATE = 'update';
    const CREATE = 'create';

    /**
     * @param News $object
     * @param null $format
     * @param array $context
     * @return array|scalar
     */
    public function normalize($object, $format = null, array $context = array())
    {
        return [
            'id' => $object->getId(),
            'title' => $object->getTitle(),
            'content' => $object->getContent(),
            'date_news' => $object->getDateNew()->format("d-m-Y"),
            'published' => $object->isPublished(),
            'created_at' => $object->getCreatedAt(),
            'modified_at' => $object->getModifiedAt()
        ];

    }

    /**
     * @param mixed $data
     * @param null $format
     * @return bool
     */
    public function supportsNormalization($data, $format = null)
    {
        return ($data instanceof News);
    }

    /**
     * @param mixed $data
     * @param string $class
     * @param null $format
     * @param array $context
     * @return News
     */
    public function denormalize($data, $class, $format = null, array $context = array())
    {
        $title = $data['title'];
        $content = $data['content'];
        $date_new = new \DateTime($data['date_news']);

        $published = $data['published'];

        if ($context['strategy'] == self::CREATE) {
            $news = new News($title, $content, $date_new);
        } else {
            /* @var News $news */
            $news = $context['news'];
            $news->setTitle($title);
            $news->setContent($content);
            $news->setDateNew($date_new);
        }

        $news->setPublished($published);
        return $news;
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type == News::class;
    }


}