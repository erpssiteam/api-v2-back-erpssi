<?php
/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 04/11/16
 * Time: 15:18
 */

namespace AppBundle\Normalizer;

use AppBundle\Entity\Cv;
use \Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\scalar;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

class CvNormalizer extends SerializerAwareNormalizer  implements NormalizerInterface {

    /**
     * @param Cv $object
     * @param null $format
     * @param array $context
     * @return array|scalar
     */
    public function normalize($object, $format = null, array $context = array())
    {
        return [
            'id' => $object->getId(),
            'name' => $object->getName(),
            'base_64' => $object->getBase64(),
            'created_at' => $object->getCreatedAt(),
            'modified_at' => $object->getModifiedAt()
        ];
    }

    public function supportsNormalization($data, $format = null){

        return ($data instanceof Cv);
    }


}