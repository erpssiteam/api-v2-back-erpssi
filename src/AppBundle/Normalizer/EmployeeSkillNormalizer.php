<?php
/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 04/11/16
 * Time: 15:12
 */


namespace AppBundle\Normalizer;

use AppBundle\Entity\EmployeeSkill;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use \Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\scalar;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

class EmployeeSkillNormalizer extends SerializerAwareNormalizer  implements NormalizerInterface, DenormalizerInterface {


    const UPDATE = 'update';
    const CREATE = 'create';

    /**
     * @param EmployeeSkill $object
     * @param null $format
     * @param array $context
     * @return array|scalar
     */
    public function normalize($object, $format = null, array $context = array())
    {
        return [
            'id' => $object->getId(),
            'level' => $object->getLevel(),
            'validate' => $object->isValidate(),
            'description' => $object->getDescription(),
            'date_skill' => $object->getDateSkill(),
            'skill' => $this->serializer->normalize($object->getSkill(), $format, $context)
        ];

    }

    public function supportsNormalization($data, $format = null){

        return ($data instanceof EmployeeSkill);
    }

    public function denormalize($data, $class, $format = null, array $context = array())
    {
        $level= $data['level'];
        $validate= $data['validate'];
        $description = $data['description'];
        $dateSkill = isset($data['date_skill'])? new \DateTime($data['date_skill']) : new \DateTime();
        $employee = $context['employee'];
        $skill = $context['skill'];

        if ($context['strategy'] == self::CREATE){
            $employeeSkill = new EmployeeSkill();
            $employeeSkill->setLevel($level);
            $employeeSkill->setValidate($validate);
            $employeeSkill->setDescription($description);
            $employeeSkill->setDateSkill($dateSkill);
            $employeeSkill->setEmployee($employee);
            $employeeSkill->setSkill($skill);
        } else {
            /* @var EmployeeSkill $employeeSkill */
            $employeeSkill = $context['employee_skill'];
            $employeeSkill->setLevel($level);
            $employeeSkill->setValidate($validate);
            $employeeSkill->setDescription($description);
            $employeeSkill->setDateSkill($dateSkill);
            $employeeSkill->setSkill($skill);
        }

        return $employeeSkill;
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type == EmployeeSkill::class;
    }


}