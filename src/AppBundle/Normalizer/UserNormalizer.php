<?php
/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 04/11/16
 * Time: 15:12
 */


namespace AppBundle\Normalizer;

use AppBundle\Entity\User;
use AppBundle\Entity\Role;
use \Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\scalar;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use AppBundle\Repository\RoleRepository;

class UserNormalizer extends SerializerAwareNormalizer  implements NormalizerInterface, DenormalizerInterface {

    const UPDATE = 'update';
    const CREATE = 'create';

    /**
     * @param User $object
     * @param null $format
     * @param array $context
     * @return array|scalar
     */
    public function normalize($object, $format = null, array $context = array())
    {
        return [
            'id' => $object->getId(),
            'username' => $object->getUsername(),
            'role' => $this->serializer->normalize($object->getRoles(), $format, $context),
        ];

    }

    public function supportsNormalization($data, $format = null){

        return ($data instanceof User);
    }

    public function denormalize($data, $class, $format = null, array $context = array())
    {
        $username = $data['username'];
        $role = $context['role'];

        if($context['strategy'] == self::CREATE ) {
            $password = $data['password'];
            $user = new User($username, $password, $role);
        } else {
            $user = $context['user'];
            $user->updateOrReplaceRole($role);
        }

        return $user;
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type == User::class;
    }


}