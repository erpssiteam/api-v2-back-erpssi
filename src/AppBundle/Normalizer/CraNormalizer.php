<?php
/**
 * Created by PhpStorm.
 * User: apprenant
 * Date: 04/11/16
 * Time: 14:25
 */

namespace AppBundle\Normalizer;

use AppBundle\Entity\Cra;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use \Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\scalar;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;

class CraNormalizer extends SerializerAwareNormalizer  implements NormalizerInterface, DenormalizerInterface {

    const UPDATE = 'update';
    const CREATE = 'create';

    /**
     * @param Cra $object
     * @param null $format
     * @param array $context
     * @return array|scalar
     */
    public function normalize($object, $format = null, array $context = array())
    {
        return [
            'id' => $object->getId(),
            'employee' => $this->serializer->normalize($object->getEmployee(), $format, $context),
            'customer' => $this->serializer->normalize($object->getCustomer(), $format, $context),
            'title' => $object->getTitle(),
            'description' => $object->getDescription(),
            'time_journey' => $object->getTimeJourney(),
            'distance' => $object->getDistance(),
            'type_job' => $object->getTypeJob(),
            'started_at' => $object->getStartedAt() != null ? $object->getStartedAt()->format('d-m-Y') : null,
            'ended_at' => $object->getEndedAt() != null ? $object->getEndedAt()->format('d-m-Y') : null,
            'validated_at' => $object->getValidatedAt() != null ? $object->getValidatedAt()->format('d-m-Y') : null,
            'created_at' => $object->getCreatedAt() != null ? $object->getCreatedAt()->format('d-m-Y') : null,
            'modified_at' => $object->getModifiedAt() != null ? $object->getModifiedAt()->format('d-m-Y') : null,
            'state' => $object->getState()
        ];

    }

    /**
     * @param mixed $data
     * @param null $format
     * @return bool
     */
    public function supportsNormalization($data, $format = null){

        return ($data instanceof Cra);
    }

    /**
     * @param mixed $data
     * @param string $class
     * @param null $format
     * @param array $context
     * @return Cra|mixed|object
     */
    public function denormalize($data, $class, $format = null, array $context = array())
    {
        $title = $data['title'];
        $description = $data['description'];
        $timeJourney = $data['time_journey'];
        $distance = $data['distance'];
        $typeJob = $data['type_job'];
        $startedAt = new \DateTime($data['started_at']);
        $endedAt = new \DateTime($data['ended_at']);
        $state = isset($data['state']) ? $data['state'] : Cra::STATE_PENDING;
        $employee = $context['employee'];
        $customer = $context['customer'];

        if($context['strategy'] == self::CREATE ) {
            $cra = new Cra($employee, $customer, $title);
        } else {
            /** @var Cra $cra */
            $cra = $context['cra'];
            $cra->setEmployee($employee);
            $cra->setCustomer($customer);
        }

        $cra->setStartedAt($startedAt);
        $cra->setEndedAt($endedAt);
        $cra->setEndedAt($endedAt);
        $cra->setDescription($description);
        $cra->setTimeJourney($timeJourney);
        $cra->setDistance($distance);
        $cra->setTypeJob($typeJob);
        $cra->setState($state);

        return $cra;
    }

    /**
     * @param mixed $data
     * @param string $type
     * @param null $format
     * @return bool
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type == Cra::class;
    }


}