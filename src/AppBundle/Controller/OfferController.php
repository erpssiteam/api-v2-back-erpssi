<?php

namespace AppBundle\Controller;
use AppBundle\Entity\JobProfile;
use AppBundle\Entity\Offer;
use AppBundle\Repository\OfferRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use \Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Validator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class OfferController
 * @package AppBundle\Controller
 * @Route("/offers", service= "erpssi.offer_controller")
 */

class OfferController extends ServiceController
{
    const UPDATE = 'update';
    const CREATE = 'create';

    /**
     * @var OfferRepository
     */
    private $offerRepository;

    /**
     * OfferController constructor.
     * @param OfferRepository $offerRepository
     */
    public function __construct(OfferRepository $offerRepository)
    {
        $this->offerRepository = $offerRepository;
    }


    /**
     * @Route("/{idOffer}", name="erpssi.offer.get", methods={"GET"}, requirements={"idOffer":"\d+"})
     * @ParamConverter("offer", class="AppBundle\Entity\Offer", options={"id" = "idOffer"})
     * @param Offer $offer
     * @return JsonResponse
     */
    public function getOfferAction(Offer $offer)
    {
        if($offer) {
            return $this->helperService->createResponse($this->serializer->normalize($offer), 200, $this->helperService->transResponse('erpssi.response.get.success'));
        }
        return $this->helperService->createResponse([], 206, $this->helperService->transResponse('erpssi.response.no_content'));
    }

    /**
     * @Route("/list", name="erpssi.offer.get_all", methods={"GET"})
     * @Method({"GET"})
     * @return JsonResponse
     */
    public function getAllOffers()
    {
        $offers = $this->offerRepository->findAll();

        if($offers) {
            return $this->helperService->createResponse($this->serializer->normalize($offers), 200, $this->helperService->transResponse("erpssi.response.get_all.success"));
        }
        return $this->helperService->createResponse([], 206, $this->helperService->transResponse("erpssi.response.no_content"));
    }

    /**
     * @Route("/{idJobProfile}", name="erpssi.offer.create", methods={"POST"}, requirements={"idJobProfile":"\d+"})
     * @ParamConverter("jobProfile", class="AppBundle\Entity\JobProfile", options={"id" = "idJobProfile"})
     * @param JobProfile $jobProfile
     * @Security("is_granted('create', null)")
     * @return JsonResponse
     */
    public function createOfferAction(JobProfile $jobProfile)
    {
        $context = ['strategy' => self::CREATE, 'jobProfile' => $jobProfile];
        $content = json_decode($this->request->getContent(), true)['data'];
        $offerEntity = $this->serializer->denormalize($content, Offer::class, 'json', $context);

        if($offerEntity instanceof Offer) {
            $errors = $this->validator->validate($offerEntity);
            if (count($errors) > 0) {
                return $this->helperService->createResponse($errors, 422, $this->helperService->transResponse("erpssi.response.create.error"));
            } else {
                $offer = $this->offerRepository->persist($offerEntity, true);
                return $this->helperService->createResponse($this->serializer->normalize($offer), 200, $this->helperService->transResponse("erpssi.response.create.success"));
            }
        }

        return $this->helperService->createResponse([], 422, $this->helperService->transResponse("erpssi.response.create.error"));
    }

    /**
     * @Route("/{idOffer}", name="erpssi.offer.update", methods={"PUT"}, requirements={"idOffer":"\d+"})
     * @ParamConverter("offer", class="AppBundle\Entity\Offer", options={"id" = "idOffer"})
     * @Security("is_granted('edit', offer)")
     * @param Offer $offer
     * @return JsonResponse
     */
    public function updateOfferAction(Offer $offer)
    {
        $content = json_decode($this->request->getContent(), true)['data'];

        $context = ['strategy' => self::UPDATE, 'offer' => $offer];

        $offerEntity = $this->serializer->denormalize($content, Offer::class, 'json', $context);

        $errors = $this->validator->validate($offerEntity);

        if (count($errors) > 0) {
            return $this->helperService->createResponse($this->serializer->normalize($errors), 422, $this->helperService->transResponse("erpssi.response.create.error"));
        } else {
            return $this->helperService->createResponse($this->serializer->normalize($this->offerRepository->flush($offerEntity)), 200, $this->helperService->transResponse("erpssi.response.update.success"));
        }

    }

    /**
     * @Route("/{idOffer}/applies", name="erpssi.offer.get_applies", methods={"PUT"}, requirements={"idOffer":"\d+"})
     * @ParamConverter("offer", class="AppBundle\Entity\Offer", options={"id" = "idOffer"})
     * @param Offer $offer
     * @Security("is_granted('get_all_applies', offer)")
     * @return JsonResponse
     */
    public function getAllApplies(Offer $offer)
    {
        if (count($offer->getApplies()) > 0) {
            return $this->helperService->createResponse($this->serializer->normalize($offer->getApplies()), 200, $this->helperService->transResponse("erpssi.response.create.success"));
        } else {
            return $this->helperService->createResponse([], 422, $this->helperService->transResponse("erpssi.response.update.error"));
        }
    }



    /**
     * @Route("/{idOffer}", name="erpssi.offer.delete", methods={"DELETE"}, requirements={"idOffer":"\d+"})
     * @ParamConverter("offer", class="AppBundle\Entity\Offer", options={"id" = "idOffer"})
     * @param Offer $offer
     * @Security("is_granted('delete', offer)")
     * @return JsonResponse
     */
    public function deleteOfferAction(Offer $offer)
    {
        $this->offerRepository->delete($offer);
        return $this->helperService->createResponse([], 200, $this->helperService->transResponse('erpssi.response.delete.success'));
    }


}