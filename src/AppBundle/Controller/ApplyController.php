<?php

namespace AppBundle\Controller;
use AppBundle\Entity\Apply;
use AppBundle\Entity\Offer;
use AppBundle\Repository\ApplyRepository;
use AppBundle\Service\FileUploader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use \Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Validator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ApplyController
 * @package AppBundle\Controller
 * @Route("/applies", service= "erpssi.apply_controller")
 */
class ApplyController extends ServiceController
{
    const UPDATE = 'update';
    const CREATE = 'create';

    /**
     * @var ApplyRepository $applyRepository
     */
    private $applyRepository;

    /**
     * @var FileUploader
     */
    private $fileUploader;


    /**
     * JobProfileController constructor.
     * @param ApplyRepository $applyRepository
     * @param FileUploader $fileUploader
     * @internal param ValidatorInterface $validator
     */
    public function __construct(ApplyRepository $applyRepository, FileUploader $fileUploader)
    {
        $this->applyRepository = $applyRepository;
        $this->fileUploader = $fileUploader;
    }

    /**
     * @Route("/{idApply}", name="erpssi.apply.get", methods={"GET"}, requirements={"idApply":"\d+"})
     * @ParamConverter("apply", class="AppBundle\Entity\Apply", options={"id" = "idApply"})
     * @param Apply $apply
     * @Security("is_granted('get', apply)")
     * @return JsonResponse
     */
    public function getApplyAction(Apply $apply)
    {
        if($apply) {
            return $this->helperService->createResponse($this->serializer->normalize($apply), 200, $this->helperService->transResponse('erpssi.response.get.success'));
        }
        return $this->helperService->createResponse([], 206, $this->helperService->transResponse("erpssi.response.no_content"));
    }

    /**
     * @Route("", name="erpssi.apply.get_all", methods={"GET"})
     * @Method({"GET"})
     * @Security("is_granted('get_all', null)")
     * @return JsonResponse
     */
    public function getAllApplies()
    {
        $applies = $this->applyRepository->findAll();

        if($applies) {
            return $this->helperService->createResponse($this->serializer->normalize($applies), 200, $this->helperService->transResponse("erpssi.response.get_all.success"));
        }
        return $this->helperService->createResponse([], 206, $this->helperService->transResponse("erpssi.response.no_content"));
    }

    /**
     * @Route("/create/{idOffer}", name="erpssi.apply.create", methods={"POST"}, requirements={"idOffer":"\d+"})
     * @ParamConverter("offer", class="AppBundle\Entity\Offer", options={"id" = "idOffer"})
     * @param Offer $offer
     * @return JsonResponse
     */
    public function createApplyAction(Offer $offer)
    {
        $context = ['strategy' => self::CREATE, 'offer' => $offer];
        $content = json_decode($this->request->getContent(), true)['data'];
        $applyEntity = $this->serializer->denormalize($content, Apply::class, 'json', $context);

        if($applyEntity instanceof Apply) {
            $errors = $this->validator->validate($applyEntity);
            if (count($errors) > 0) {
                return $this->helperService->createResponse($errors, 422, $this->helperService->transResponse("erpssi.response.create.error"));
            } else {
                $jobProfile = $this->applyRepository->persist($applyEntity, true);
                return $this->helperService->createResponse($this->serializer->normalize($jobProfile), 200, $this->helperService->transResponse("erpssi.response.create.success"));
            }
        }

        return $this->helperService->createResponse([], 422, $this->helperService->transResponse("erpssi.response.create.error"));
    }

    /**
     * @Route("/{idApply}", name="erpssi.apply.update", methods={"PUT"}, requirements={"idApply":"\d+"})
     * @ParamConverter("apply", class="AppBundle\Entity\Apply", options={"id" = "idApply"})
     * @param Apply $apply
     * @Security("is_granted('edit', apply)")
     * @return JsonResponse
     */
    public function updateApplyAction(Apply $apply)
    {
        $content = json_decode($this->request->getContent(), true)['data'];

        $context = ['strategy' => self::UPDATE, 'apply' => $apply];

        $applyEntity = $this->serializer->denormalize($content, Apply::class, 'json', $context);

        $errors = $this->validator->validate($applyEntity);

        if (count($errors) > 0) {
            return $this->helperService->createResponse($this->serializer->normalize($errors), 422, $this->helperService->transResponse("erpssi.response.create.error"));
        } else {
            return $this->helperService->createResponse($this->serializer->normalize($this->applyRepository->flush($applyEntity)), 200, $this->helperService->transResponse("erpssi.response.update.success"));
        }

    }

    /**
     * Due to upload file, this method can not be a PUT one // cf symfony doc
     * @Route("/upload-cv/{idApply}", name="erpssi.apply.update_cv", methods={"POST"}, requirements={"idApply":"\d+"})
     * @ParamConverter("apply", class="AppBundle\Entity\Apply", options={"id" = "idApply"})
     * @param Apply $apply
     * @return JsonResponse
     */
    public function updateApplicationCvAction(Apply $apply)
    {
        if($this->request->files->get('cv')) {
            $file = $this->request->files->get('cv');
        }

        if($this->request->files->get('file')) {
            $file = $this->request->files->get('file');
        }

        if($this->request->get('body')) {
            $file = $this->request->get('body');
        }

        if($file) {
            $fileName = $this->fileUploader->upload($file);
            $apply = $this->applyRepository->updateCv($apply, $fileName);
            return $this->helperService->createResponse($this->serializer->normalize($apply), 200, $this->helperService->transResponse("erpssi.response.update.success"));
        }

        return $this->helperService->createResponse([], 422, $this->helperService->transResponse("erpssi.response.create.error"));

    }

    /**
     * @Route("/{idApply}", name="erpssi.apply.delete", methods={"DELETE"}, requirements={"idApply":"\d+"})
     * @ParamConverter("apply", class="AppBundle\Entity\Apply", options={"id" = "idApply"})
     * @param Apply $apply
     * @Security("is_granted('delete', apply)")
     * @return JsonResponse
     */
    public function deleteApplyAction(Apply $apply)
    {
        $this->applyRepository->delete($apply);
        return $this->helperService->createResponse([], 200, $this->helperService->transResponse('erpssi.response.delete.success'));
    }

    /**
     * @Route("/{idApply}/handle/{state}", name="erpssi.apply.handle", methods={"PUT"}, requirements={"idApply":"\d+", "state":"\d+"})
     * @ParamConverter("apply", class="AppBundle\Entity\Apply", options={"id" = "idApply"})
     * @param Apply $apply
     * @Security("is_granted('handle', apply)")
     * @return JsonResponse
     */
    public function handleApply(Apply $apply)
    {
        switch (intval($this->request->attributes->get('state'))) {
            case Apply::APPLY_VALIDATED_STATE : $this->applyRepository->validate($apply); break;
            case Apply::APPLY_DENIED_STATE : $this->applyRepository->denied($apply); break;
        }
        return $this->helperService->createResponse($this->serializer->normalize($apply), 200, $this->helperService->transResponse('erpssi.response.handle.success'));
    }
}