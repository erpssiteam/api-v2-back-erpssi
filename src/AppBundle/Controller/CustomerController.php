<?php
/**
 * Created by PhpStorm.
 * User: reslene
 * Date: 02/06/16
 * Time: 10:13
 */

namespace AppBundle\Controller;
use AppBundle\Entity\Customer;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use AppBundle\Repository\CustomerRepository;
use AppBundle\Repository\RoleRepository;
use AppBundle\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class CustomerController
 * @package AppBundle\Controller
 * @Route("/customers", service="erpssi.customer_controller")
 */
class CustomerController extends ServiceController {


    const UPDATE = 'update';
    const CREATE = 'create';


    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * @var UserRepository $userRepository
     */
    private $userRepository;

    /**
     * @var RoleRepository $roleRepository
     */
    private $roleRepository;

    /**
     * CustomerController constructor.
     * @param CustomerRepository $customerRepository
     * @param UserRepository $userRepository
     * @param RoleRepository $roleRepository
     */
    public function __construct(CustomerRepository $customerRepository, UserRepository $userRepository, RoleRepository $roleRepository)
    {
        $this->customerRepository = $customerRepository;
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
    }

    /**
     * @Route("", name="erpssi.customer.get_all")
     * @Method({"GET"})
     */
    public function getAllCustomersAction() {

        $customers = $this->customerRepository->findAll();

        if($customers) {
            return $this->helperService->createResponse($this->serializer->normalize($customers), 200, $this->helperService->transResponse("erpssi.response.get_all.success"));
        }
        return $this->helperService->createResponse([], 206, $this->helperService->transResponse("erpssi.response.no_content"));
    }

    /**
     * @Route("/{idCustomer}", name="erpssi.customer.get", methods={"GET"}, requirements={"idCustomer":"\d+"})
     * @ParamConverter("customer", class="AppBundle\Entity\Customer", options={"id" = "idCustomer"})
     * @param Customer $customer
     * @return JsonResponse
     */
    public function getCustomerAction(Customer $customer)
    {
        if($customer) {
            return $this->helperService->createResponse($this->serializer->normalize($customer), 200, $this->helperService->transResponse('erpssi.response.get.success'));
        }
        return $this->helperService->createResponse([], 206, $this->helperService->transResponse('erpssi.response.no_content'));
    }


    /**
     * @Route("", name="erpssi.customer.create", methods={"POST"})
     * @return JsonResponse
     */
    public function createCustomerAction(){
        $content = json_decode($this->request->getContent(), true)['data'];
        $context = ['strategy' => self::CREATE];

        $roleEntity = $this->roleRepository->findOneBy(['label' => $content['role']]);

        if($roleEntity instanceof Role)
            $context['role'] = $roleEntity;
        else
            $context['role'] = $this->roleRepository->findOneBy(['label' => 'ROLE_USER']);

        $userEntity = $this->serializer->denormalize($content, User::class, 'json', $context);

        if($userEntity instanceof  User){
            $userEntity->setPassword($this->encoder->encodePassword($userEntity,$userEntity->getPassword()));
            $user = $this->userRepository->persist($userEntity, true);
            $context['user'] = $user;
        }

        $customerEntity = $this->serializer->denormalize($content, Customer::class, 'json', $context);

        if($customerEntity instanceof Customer) {
            $customer = $this->customerRepository->persist($customerEntity, true);
            return $this->helperService->createResponse($this->serializer->normalize($customer), 200, $this->helperService->transResponse("erpssi.response.create.success"));
        }

        return $this->helperService->createResponse([], 422, $this->helperService->transResponse("erpssi.response.create.error"));
    }

    /**
     * @Route("/{idCustomer}", name="erpssi.customer.update", methods={"PUT"}, requirements={"idCustomer":"\d+"})
     * @ParamConverter("customer", class="AppBundle\Entity\Customer", options={"id" = "idCustomer"})
     * @param Customer $customer
     * @return JsonResponse
     */
    public function updateCustomerAction(Customer $customer)
    {
        $content = json_decode($this->request->getContent(), true)['data'];
        $context = ['strategy' => self::UPDATE, 'customer' => $customer];

        if($content['role']){
            $roleEntity = $this->roleRepository->findOneBy(['label' => $content['role']]);

            if($roleEntity instanceof Role){
                $context['role'] = $roleEntity;
                $context['user'] = $customer->getUser();
                $userEntity = $this->serializer->denormalize($content, User::class, 'json', $context);
                if($userEntity instanceof  User) {
                    $this->userRepository->flush($userEntity);
                }
            }
        }

        $customerEntity = $this->serializer->denormalize($content, Customer::class, 'json', $context);

        $customer = $this->customerRepository->flush($customerEntity);

        return $this->helperService->createResponse($this->serializer->normalize($customer), 200, $this->helperService->transResponse("erpssi.response.update.success"));
    }

    /**
     * @Route("/{idCustomer}", name="erpssi.customer.delete", methods={"DELETE"}, requirements={"idCustomer":"\d+"})
     * @ParamConverter("customer", class="AppBundle\Entity\Customer", options={"id" = "idCustomer"})
     * @param Customer $customer
     * @return JsonResponse
     */
    public function deleteCustomerAction(Customer $customer)
    {
        $this->customerRepository->delete($customer);
        return $this->helperService->createResponse([], 200, $this->helperService->transResponse('erpssi.response.delete.success'));
    }

}