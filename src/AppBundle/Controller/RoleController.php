<?php
/**
 * Created by PhpStorm.
 * User: reslene
 * Date: 02/06/16
 * Time: 10:13
 */

namespace AppBundle\Controller;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use AppBundle\Repository\RoleRepository;
use AppBundle\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class UserController
 * @package AppBundle\Controller
 * @Security("has_role('ROLE_ADMIN')")
 * @Route("/roles", service="erpssi.role_controller")
 */
class RoleController extends ServiceController {

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * RoleController constructor.
     * @param UserRepository $userRepository
     * @param RoleRepository $roleRepository
     */
    public function __construct(UserRepository $userRepository, RoleRepository $roleRepository)
    {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
    }

    /**
     * @Route("", name="erpssi.role.get_all")
     * @Method({"GET"})
     */
    public function getAllRolesAction() {
        $roles = $this->roleRepository->findAll();

        if($roles) {
            return $this->helperService->createResponse($this->serializer->normalize($roles), 200, $this->helperService->transResponse("erpssi.response.get_all.success"));
        }
        return $this->helperService->createResponse([], 206, $this->helperService->transResponse("erpssi.response.no_content"));
    }

}