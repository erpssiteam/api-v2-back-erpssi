<?php
/**
 * Created by PhpStorm.
 * User: reslene
 * Date: 02/06/16
 * Time: 10:13
 */

namespace AppBundle\Controller;
use AppBundle\Entity\Employee;
use AppBundle\Entity\Skill;
use AppBundle\Repository\EmployeeRepository;
use AppBundle\Repository\SkillRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class SkillController
 * @package AppBundle\Controller
 * @Route("/skills", service="erpssi.skill_controller")
 * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_RH') or has_role('ROLE_MANAGER')")
 */
class SkillController extends ServiceController {

    const UPDATE = 'update';
    const CREATE = 'create';

    /**
     * @var SkillRepository
     */
    private $skillRepository;

    /**
     * @var EmployeeSkillController
     */
    private $employeeSkillController;

    /**
     * @var EmployeeRepository
     */
    private $employeeRepository;

    /**
     * SkillController constructor.
     * @param SkillRepository $skillRepository
     * @param EmployeeSkillController $employeeSkillController
     * @param EmployeeRepository $employeeRepository
     */
    public function __construct(SkillRepository $skillRepository, EmployeeSkillController $employeeSkillController, EmployeeRepository $employeeRepository)
    {
        $this->skillRepository = $skillRepository;
        $this->employeeSkillController = $employeeSkillController;
        $this->employeeRepository = $employeeRepository;
    }

    /**
     * @Route("", name="erpssi.skill.get_all")
     * @Method({"GET"})
     */
    public function getAllSkillsAction() {
        $skills = $this->skillRepository->findBy(['depreciated' => FALSE]);

        if($skills) {
            return $this->helperService->createResponse($this->serializer->normalize($skills), 200, $this->helperService->transResponse("erpssi.response.get_all.success"));
        }
        return $this->helperService->createResponse([], 422, $this->helperService->transResponse("erpssi.response.no_content"));
    }

    /**
     * @Route("/{idSkill}", name="erpssi.skill.get", requirements={"idSkill" : "\d+"})
     * @Method({"GET"})
     * @ParamConverter("skill", class="AppBundle\Entity\Skill", options={"id" = "idSkill"}))
     * @param Skill $skill
     * @return JsonResponse
     */
    public function getSkillAction(Skill $skill) {
        if($skill && $skill->getDepreciated() == FALSE) {
            return $this->helperService->createResponse($this->serializer->normalize($skill), 200, $this->helperService->transResponse('erpssi.response.get.success'));
        }
        return $this->helperService->createResponse([], 206, $this->helperService->transResponse('erpssi.response.no_content'));
    }

    /**
     * @Route("", name="erpssi.skill.create")
     * @Method({"POST"})
     */
    public function createSkillAction() {
        $context = ['strategy' => self::CREATE];
        $content = json_decode($this->request->getContent(), true)['data'];
        $skillEntity = $this->serializer->denormalize($content, Skill::class, 'json', $context);

        if($skillEntity instanceof Skill) {
            $skill = $this->skillRepository->persist($skillEntity, true);
            /* create new entry in employee skills for each employee */

            $employees = $this->employeeRepository->findAll();

            foreach ($employees as $employee){
                if($employee instanceof Employee) {
                    $this->employeeSkillController->addEntryEmployeeSkills($employee,$skill);
                }
            }

            return $this->helperService->createResponse($this->serializer->normalize($skill), 200, $this->helperService->transResponse("erpssi.response.create.success"));
        }

        return $this->helperService->createResponse([], 206, $this->helperService->transResponse("erpssi.response.create.error"));
    }

    /**
     * @Route("/{idSkill}", name="erpssi.skill.update", methods={"PUT"}, requirements={"idSkill":"\d+"})
     * @ParamConverter("skill", class="AppBundle\Entity\Skill", options={"id" = "idSkill"})
     * @param Skill $skill
     * @return JsonResponse
     */
    public function updateSkillAction(Skill $skill) {
        $context = ['strategy' => self::UPDATE, 'skill' => $skill];
        $content = json_decode($this->request->getContent(), true)['data'];
        $skillEntity = $this->serializer->denormalize($content, Skill::class, 'json', $context);

        if($skillEntity instanceof Skill) {
            $skill = $this->skillRepository->flush($skillEntity);
            return $this->helperService->createResponse($this->serializer->normalize($skill), 200, $this->helperService->transResponse("erpssi.response.update.success"));
        }

        return $this->helperService->createResponse([], 422, $this->helperService->transResponse("erpssi.response.update.error"));
    }

    /**
     * @param Skill $skill
     * @Route("/{idSkill}", name="erpssi.skill.delete", requirements={"idSkill" :"\d+"})
     * @Method({"DELETE"})
     * @ParamConverter("skill", class="AppBundle\Entity\Skill", options={"id" = "idSkill"})
     * @return JsonResponse
     */
    public function deleteSkillAction(Skill $skill){

        if($skill instanceof Skill) {
            /* delete entries of depreciated skill in employee skills for each employee */

            $employees = $this->employeeRepository->findAll();

            foreach ($employees as $employee){
                if($employee instanceof Employee) {
                    $this->employeeSkillController->deleteEmployeeSkillsBySkillAction($skill);
                }
            }
            $this->skillRepository->delete($skill);
        }
        return $this->helperService->createResponse([], 200, $this->helperService->transResponse('erpssi.response.delete.success'));
    }

}