<?php

namespace AppBundle\Controller;
use AppBundle\Entity\Customer;
use AppBundle\Entity\JobProfile;
use AppBundle\Repository\JobProfileRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use \Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Validator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class JobProfileController
 * @package AppBundle\Controller
 * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_RH') or has_role('ROLE_MANAGER')")
 * @Route("/job-profiles", service= "erpssi.job_profile_controller")
 */

class JobProfileController extends ServiceController
{
    const UPDATE = 'update';
    const CREATE = 'create';

    /**
     * @var JobProfileRepository $jobProfileRepository
     */
    private $jobProfileRepository;


    /**
     * JobProfileController constructor.
     * @param JobProfileRepository $jobProfileRepository
     * @param ValidatorInterface $validator
     */
    public function __construct(JobProfileRepository $jobProfileRepository)
    {
        $this->jobProfileRepository = $jobProfileRepository;
    }

    /**
     * @Route("/{idJobProfile}", name="erpssi.job_profile.get", methods={"GET"}, requirements={"idJobProfile":"\d+"})
     * @ParamConverter("jobProfile", class="AppBundle\Entity\JobProfile", options={"id" = "idJobProfile"})
     * @param JobProfile $jobProfile
     * @return JsonResponse
     */
    public function getJobProfileAction(JobProfile $jobProfile)
    {
        if($jobProfile) {
            return $this->helperService->createResponse($this->serializer->normalize($jobProfile), 200, $this->helperService->transResponse('erpssi.response.get.success'));
        }
        return $this->helperService->createResponse([], 206, $this->helperService->transResponse('erpssi.response.no_content'));
    }

    /**
     * @Route("", name="erpssi.job_profile.get_all", methods={"GET"})
     * @Method({"GET"})
     * @return JsonResponse
     */
    public function getAllJobProfileAction()
    {
        $jobProfiles = $this->jobProfileRepository->findAll();

        if($jobProfiles) {
            return $this->helperService->createResponse($this->serializer->normalize($jobProfiles), 200, $this->helperService->transResponse("erpssi.response.get_all.success"));
        }
        return $this->helperService->createResponse([], 206, $this->helperService->transResponse("erpssi.response.no_content"));
    }

    /**
     * @Route("/{idCustomer}", name="erpssi.job_profile.create", methods={"POST"}, requirements={"idCustomer":"\d+"})
     * @ParamConverter("customer", class="AppBundle\Entity\Customer", options={"id" = "idCustomer"})
     * @param Customer $customer
     * @return JsonResponse
     */
    public function createJobProfileAction(Customer $customer)
    {
        $context = ['strategy' => self::CREATE, 'customer' => $customer];
        $content = json_decode($this->request->getContent(), true)['data'];
        $jobProfileEntity = $this->serializer->denormalize($content, JobProfile::class, 'json', $context);

        if($jobProfileEntity instanceof JobProfile) {

            if (count($this->validator->validate($jobProfileEntity)) > 0) {
                return $this->helperService->createResponse($this->serializer->normalize($jobProfileEntity), 422, $this->helperService->transResponse("erpssi.response.create.error"));
            } else {
                $jobProfile = $this->jobProfileRepository->persist($jobProfileEntity, true);
                return $this->helperService->createResponse($this->serializer->normalize($jobProfile), 200, $this->helperService->transResponse("erpssi.response.create.success"));
            }
        }

        return $this->helperService->createResponse([], 422, $this->helperService->transResponse("erpssi.response.create.error"));
    }

    /**
     * @Route("/{idJobProfile}/{idCustomer}", name="erpssi.job_profile.update", methods={"PUT"}, requirements={"idJobProfile":"\d+", "idCustomer":"\d+"})
     * @ParamConverter("customer", class="AppBundle\Entity\Customer", options={"id" = "idCustomer"})
     * @ParamConverter("jobProfile", class="AppBundle\Entity\JobProfile", options={"id" = "idJobProfile"})
     * @param JobProfile $jobProfile
     * @param Customer $customer
     * @return JsonResponse
     */
    public function updateJobProfileAction(JobProfile $jobProfile, Customer $customer)
    {
        $content = json_decode($this->request->getContent(), true)['data'];
        $context = ['strategy' => self::UPDATE, 'customer' => $customer, 'jobProfile' => $jobProfile];

        $jobProfileEntity = $this->serializer->denormalize($content, JobProfile::class, 'json', $context);

        if (count($this->validator->validate($jobProfileEntity)) > 0) {
            return $this->helperService->createResponse($this->serializer->normalize($jobProfileEntity), 422, $this->helperService->transResponse("erpssi.response.update.error"));
        } else {
            return $this->helperService->createResponse($this->serializer->normalize($this->jobProfileRepository->flush($jobProfileEntity)), 200, $this->helperService->transResponse("erpssi.response.update.success"));
        }

    }

    /**
     * @Route("/{idJobProfile}", name="erpssi.job_profile.delete", methods={"DELETE"}, requirements={"idJobProfile":"\d+"})
     * @ParamConverter("jobProfile", class="AppBundle\Entity\JobProfile", options={"id" = "idJobProfile"})
     * @param JobProfile $jobProfile
     * @return JsonResponse
     */
    public function deleteJobProfileAction(JobProfile $jobProfile)
    {
        $this->jobProfileRepository->delete($jobProfile);
        return $this->helperService->createResponse([], 200, $this->helperService->transResponse('erpssi.response.delete.success'));
    }
}