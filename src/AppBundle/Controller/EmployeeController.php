<?php
/**
 * Created by PhpStorm.
 * User: reslene
 * Date: 02/06/16
 * Time: 10:13
 */

namespace AppBundle\Controller;
use AppBundle\Entity\Employee;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use AppBundle\Provider\EmployeeProvider;
use AppBundle\Repository\EmployeeRepository;
use AppBundle\Repository\JobProfileRepository;
use AppBundle\Repository\UserRepository;
use AppBundle\Repository\RoleRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class EmployeeController
 * @package AppBundle\Controller
 * @Route("/employees", service="erpssi.employee_controller")
 */
class EmployeeController extends ServiceController {


    const UPDATE = 'update';
    const CREATE = 'create';


    /**
     * @var EmployeeRepository $employeeRepository
     */
    private $employeeRepository;

    /**
     * @var UserRepository $userRepository
     */
    private $userRepository;

    /**
     * @var RoleRepository $roleRepository
     */
    private $roleRepository;

    /**
     * @var JobProfileRepository
     */
    private $jobProfileRepository;

    /**
     * @var EmployeeProvider
     */
    private $employeeProvider;

    /**
     * EmployeeController constructor.
     * @param EmployeeRepository $employeeRepository
     * @param UserRepository $userRepository
     * @param RoleRepository $roleRepository
     * @param JobProfileRepository $jobProfileRepository
     * @param EmployeeProvider $employeeProvider
     */
    public function __construct(EmployeeRepository $employeeRepository, UserRepository $userRepository, RoleRepository $roleRepository, JobProfileRepository $jobProfileRepository, EmployeeProvider $employeeProvider)
    {
        $this->employeeRepository = $employeeRepository;
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
        $this->jobProfileRepository = $jobProfileRepository;
        $this->employeeProvider = $employeeProvider;
    }


    /**
     * @Route("", name="erpssi.employee.get_all")
     * @Method({"GET"})
     * @Security("is_granted('get_all', null)")
     */
    public function getAllEmployeesAction()
    {
        $employees = $this->employeeProvider->getAllDecision($this->tokenStorage->getToken()->getUser(), $this->employeeRepository);
        if($employees) {
            return $this->helperService->createResponse($this->serializer->normalize($employees), 200, $this->helperService->transResponse("erpssi.response.get_all.success"));
        }
        return $this->helperService->createResponse([], 206, $this->helperService->transResponse("erpssi.response.no_content"));
    }

    /**
     * @Route("/{idEmployee}", name="erpssi.employee.get", methods={"GET"}, requirements={"idEmployee":"\d+"})
     * @ParamConverter("employee", class="AppBundle\Entity\Employee", options={"id" = "idEmployee"})
     * @param Employee $employee
     * @return JsonResponse
     * @Security("is_granted('get', employee)")
     */
    public function getEmployeeAction(Employee $employee)
    {
        if($employee) {
            return $this->helperService->createResponse($this->serializer->normalize($employee), 200, $this->helperService->transResponse('erpssi.response.get.success'));
        }
        return $this->helperService->createResponse([], 206, $this->helperService->transResponse('erpssi.response.no_content'));
    }

    /**
     * @Route("/{idEmployee}/customers", name="erpssi.employee.get_customer", methods={"GET"}, requirements={"idEmployee":"\d+"})
     * @ParamConverter("employee", class="AppBundle\Entity\Employee", options={"id" = "idEmployee"})
     * @param Employee $employee
     * @return JsonResponse
     * @Security("is_granted('get', employee)")
     */
    public function getCustomerByEmployeeAction(Employee $employee)
    {
        if($employee) {
            $jobProfiles = $this->jobProfileRepository->findCustomersByJobProfile($employee);

            $customers = [];

            foreach ($jobProfiles as $jobProfile){
                /** @var JobProfile $jobProfile */
                $customers[] = $this->serializer->normalize($jobProfile->getCustomer());
            }

            return $this->helperService->createResponse($customers, 200, $this->helperService->transResponse('erpssi.response.get.success'));
        }
        return $this->helperService->createResponse([], 422, $this->helperService->transResponse('erpssi.response.no_content'));
    }

    /**
     * @Route("", name="erpssi.employee.create", methods={"POST"})
     * @Security("is_granted('create', null)")
     * @return JsonResponse
     */
    public function createEmployeeAction(){

        $content = json_decode($this->request->getContent(), true)['data'];
        $context = ['strategy' => self::CREATE];

        $roleEntity = $this->roleRepository->findOneBy(['label' => $content['role']]);

        if($roleEntity instanceof Role)
            $context['role'] = $roleEntity;
        else
            $context['role'] = $this->roleRepository->findOneBy(['label' => 'ROLE_USER']);

        $userEntity = $this->serializer->denormalize($content, User::class, 'json', $context);

        if($userEntity instanceof  User){
            $userEntity->setPassword($this->encoder->encodePassword($userEntity,$userEntity->getPassword()));
            $user = $this->userRepository->persist($userEntity, true);
            $context['user'] = $user;
        }

        $employeeEntity = $this->serializer->denormalize($content, Employee::class, 'json', $context);

        if($employeeEntity instanceof Employee) {
            $employee = $this->employeeRepository->persist($employeeEntity, true);
            return $this->helperService->createResponse($this->serializer->normalize($employee), 200, $this->helperService->transResponse("erpssi.response.create.success"));
        }

        return $this->helperService->createResponse([], 422, $this->helperService->transResponse("erpssi.response.create.error"));
    }

    /**
     * @Route("/{idEmployee}", name="erpssi.employee.update", methods={"PUT"}, requirements={"idEmployee":"\d+"})
     * @ParamConverter("employee", class="AppBundle\Entity\Employee", options={"id" = "idEmployee"})
     * @Security("is_granted('edit', employee)")
     * @param Employee $employee
     * @return JsonResponse
     */
    public function updateEmployeeAction(Employee $employee)
    {
        $content = json_decode($this->request->getContent(), true)['data'];
        $context = ['strategy' => self::UPDATE, 'employee' => $employee];

        if($content['role']){
            $roleEntity = $this->roleRepository->findOneBy(['label' => $content['role']]);

            if($roleEntity instanceof Role){
                $context['role'] = $roleEntity;
                $context['user'] = $employee->getUser();
                $userEntity = $this->serializer->denormalize($content, User::class, 'json', $context);
                if($userEntity instanceof  User) {
                    $this->userRepository->flush($userEntity);
                }
            }
        }

        $employeeEntity = $this->serializer->denormalize($content, Employee::class, 'json', $context);

        $employee = $this->employeeRepository->flush($employeeEntity);

        return $this->helperService->createResponse($this->serializer->normalize($employee), 200, $this->helperService->transResponse("erpssi.response.update.success"));
    }

    /**
     * @Route("/role", name="erpssi.employee.update_role", methods={"POST"})
     * @Security("is_granted('create', null)")
     * @return JsonResponse
     */
    public function updateEmployeeRoleAction()
    {
        $content = json_decode($this->request->getContent(), true)['data'];
        $context = ['strategy' => self::UPDATE];

        foreach($content as $item){
            if($item['name_role']){
                $roleEntity = $this->roleRepository->findOneBy(['label' => $item['name_role']]);

                if($roleEntity instanceof Role){
                    $context['role'] = $roleEntity;
                    $employee = $this->employeeRepository->findOneBy(['id' => $item['id_employee']]);
                    if($employee instanceof Employee){
                        $context['user'] = $employee->getUser();
                        /* fix not nice */
                        $content['username'] = '';
                        $userEntity = $this->serializer->denormalize($content, User::class, 'json', $context);
                        if($userEntity instanceof  User) {
                            $this->userRepository->flush($userEntity);
                        }
                    }

                }
            }
        }
        return $this->helperService->createResponse('OK', 200, $this->helperService->transResponse("erpssi.response.update.success"));

    }

    /**
     * @Route("/{idEmployee}/password", name="erpssi.employee.update_password", methods={"PUT"}, requirements={"idEmployee":"\d+"})
     * @ParamConverter("employee", class="AppBundle\Entity\Employee", options={"id" = "idEmployee"})
     * @param Employee $employee
     * @Security("is_granted('edit', employee)")
     * @return JsonResponse
     */
    public function updatePasswordEmployeeAction(Employee $employee){
        $content = json_decode($this->request->getContent(), true)['data'];
        $context = ['employee' => $employee];

        if($employee){
            /* @var User $user */
            $user = $employee->getUser();
//            $old_password = $user->getPassword();
//
//            if($old_password === $this->encoder->encodePassword($user,$content['old_password']))
//                return $this->helperService->createResponse($this->serializer->normalize($employee), 200, $this->helperService->transResponse("erpssi.response.update.password_error"));
//
//            else if ($old_password === $this->encoder->encodePassword($user,$content['new_password']))
//                return $this->helperService->createResponse($this->serializer->normalize($employee), 200, $this->helperService->transResponse("erpssi.response.update.same_password"));

            $user->setPassword($this->encoder->encodePassword($user,$content['new_password']));

            if($user instanceof  User) {
                $this->userRepository->flush($user);
            }
        }

        return $this->helperService->createResponse($this->serializer->normalize($employee), 200, $this->helperService->transResponse("erpssi.response.update.success"));
    }

    /**
     * @Route("/{idEmployee}", name="erpssi.employee.delete", methods={"DELETE"}, requirements={"idEmployee":"\d+"})
     * @ParamConverter("employee", class="AppBundle\Entity\Employee", options={"id" = "idEmployee"})
     * @param Employee $employee
     * @Security("is_granted('delete', employee)")
     * @return JsonResponse
     */
    public function deleteEmployeeAction(Employee $employee)
    {
        $this->employeeRepository->delete($employee);
        return $this->helperService->createResponse([], 200, $this->helperService->transResponse('erpssi.response.delete.success'));
    }

}