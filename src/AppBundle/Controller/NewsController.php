<?php
/**
 * Created by PhpStorm.
 * User: whiskey
 * Date: 24/03/17
 * Time: 15:07
 */


namespace AppBundle\Controller;
use AppBundle\Entity\News;
use AppBundle\Repository\NewsRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use \Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Validator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class NewsController
 * @package AppBundle\Controller
 * @Route("/news", service= "erpssi.news_controller")
 */

class NewsController extends ServiceController
{
    const UPDATE = 'update';
    const CREATE = 'create';

    /**
     * @var NewsRepository $newsRepository
     */
    private $newsRepository;
    
    /**
     * NewsController constructor.
     * @param NewsRepository $newsRepository
     */
    public function __construct(NewsRepository $newsRepository)
    {
        $this->newsRepository = $newsRepository;
    }

    /**
     * @Route("/list", name="erpssi.news.get_all", methods={"GET"})
     * @Method({"GET"})
     * @return JsonResponse
     */
    public function getAllNews()
    {
        $news = $this->newsRepository->findAll();

        if($news) {
            return $this->helperService->createResponse($this->serializer->normalize($news), 200, $this->helperService->transResponse("erpssi.response.get_all.success"));
        }
        return $this->helperService->createResponse([], 206, $this->helperService->transResponse("erpssi.response.no_content"));

    }

    /**
     * @Route("/{idNews}", name="erpssi.news.get", methods={"GET"}, requirements={"idNews":"\d+"})
     * @ParamConverter("news", class="AppBundle\Entity\News", options={"id" = "idNews"})
     * @param News $news
     * @return JsonResponse
     */
    public function getNews(News $news)
    {
        if($news) {
            return $this->helperService->createResponse($this->serializer->normalize($news), 200, $this->helperService->transResponse('erpssi.response.get.success'));
        }
        return $this->helperService->createResponse([], 206, $this->helperService->transResponse('erpssi.response.no_content'));
    }


    /**
     * @Route("", name="erpssi.news.create", methods={"POST"})
     * @Security("is_granted('create', null)")
     * @return JsonResponse
     */
    public function createNewsAction()
    {
        $content = json_decode($this->request->getContent(), true)['data'];

        $context = ['strategy' => self::CREATE];

        $newsEntity = $this->serializer->denormalize($content, News::class, 'json', $context);

        $errors = $this->validator->validate($newsEntity);

        if (count($errors) > 0) {
            return $this->helperService->createResponse($this->serializer->normalize($errors), 422, $this->helperService->transResponse("erpssi.response.create.error"));
        } else {
            if($newsEntity instanceof News) {
                $news = $this->newsRepository->persist($newsEntity, true);
                return $this->helperService->createResponse($this->serializer->normalize($news), 200, $this->helperService->transResponse("erpssi.response.create.success"));
            }
        }
    }

    /**
     * @Route("/{idNews}", name="erpssi.news.update", methods={"PUT"}, requirements={"idNews":"\d+"})
     * @ParamConverter("news", class="AppBundle\Entity\News", options={"id" = "idNews"})
     * @param News $news
     * @Security("is_granted('edit', news)")
     * @return JsonResponse
     */
    public function updateNewsAction(News $news)
    {
        $content = json_decode($this->request->getContent(), true)['data'];

        $context = ['strategy' => self::UPDATE, 'news' => $news];

        $newsEntity = $this->serializer->denormalize($content, News::class, 'json', $context);

        $errors = $this->validator->validate($newsEntity);

        if (count($errors) > 0) {
            return $this->helperService->createResponse($this->serializer->normalize($errors), 422, $this->helperService->transResponse("erpssi.response.create.error"));
        } else {
            return $this->helperService->createResponse($this->serializer->normalize($this->newsRepository->flush($newsEntity)), 200, $this->helperService->transResponse("erpssi.response.update.success"));
        }
    }

    /**
     * @Route("/{idNews}", name="erpssi.news.delete", methods={"DELETE"}, requirements={"idNews":"\d+"})
     * @ParamConverter("news", class="AppBundle\Entity\News", options={"id" = "idNews"})
     * @param News $news
     * @Security("is_granted('delete', news)")
     * @return JsonResponse
     */
    public function deleteNewsAction(News $news)
    {

        $this->newsRepository->delete($news);
        return $this->helperService->createResponse([], 200, $this->helperService->transResponse('erpssi.response.delete.success'));
    }

    /**
     * @Route("/{idNews}/publish/{state}", name="erpssi.news.handle", methods={"PUT"}, requirements={"idNews":"\d+", "state":"\d+"})
     * @ParamConverter("news", class="AppBundle\Entity\News", options={"id" = "idNews"})
     * @param News $news
     * @Security("is_granted('handle', news)")
     * @return JsonResponse
     */
    public function publishNews(News $news)
    {
        switch (intval($this->request->attributes->get('state'))) {
            case News::NEWS_PUBLISHED_STATE : $this->newsRepository->publish($news); break;
            case News::NEWS_UNPUBLISHED_STATE : $this->newsRepository->unpublish($news); break;
        }
        return $this->helperService->createResponse($this->serializer->normalize($news), 200, $this->helperService->transResponse('erpssi.response.update.success'));
    }
}