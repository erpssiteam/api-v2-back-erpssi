<?php

namespace AppBundle\Controller;
use AppBundle\Entity\Employee;
use AppBundle\Entity\Holiday;
use AppBundle\Repository\HolidayRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use \Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Validator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class HolidayController
 * @package AppBundle\Controller
 * @Route("/holidays", service= "erpssi.holiday_controller")
 */

class HolidayController extends ServiceController
{
    const UPDATE = 'update';
    const CREATE = 'create';

    /**
     * @var HolidayRepository
     */
    private $holidayRepository;


    /**
     * HolidayController constructor.
     * @param HolidayRepository $holidayRepository
     * @param ValidatorInterface $validator
     */
    public function __construct(HolidayRepository $holidayRepository)
    {
        $this->holidayRepository = $holidayRepository;
    }

    /**
     * @Route("/{idHoliday}", name="erpssi.holiday.get", methods={"GET"}, requirements={"idHoliday":"\d+"})
     * @ParamConverter("holiday", class="AppBundle\Entity\Holiday", options={"id" = "idHoliday"})
     * @param Holiday $holiday
     * @Security("is_granted('get', holiday)")
     * @return JsonResponse
     */
    public function getHolidayAction(Holiday $holiday)
    {
        if($holiday) {
            return $this->helperService->createResponse($this->serializer->normalize($holiday), 200, $this->helperService->transResponse('erpssi.response.get.success'));
        }
        return $this->helperService->createResponse([], 206, $this->helperService->transResponse('erpssi.response.no_content'));
    }

    /**
     * @Route("", name="erpssi.holiday.get_all", methods={"GET"})
     * @Method({"GET"})
     * @Security("is_granted('get_all', null)")
     * @return JsonResponse
     */
    public function getAllHolidays()
    {
        $holidays = $this->entityProvider->getAllDecision($this->tokenStorage->getToken()->getUser(), $this->holidayRepository);

        if($holidays) {
            return $this->helperService->createResponse($this->serializer->normalize($holidays), 200, $this->helperService->transResponse("erpssi.response.get_all.success"));
        }
        return $this->helperService->createResponse([], 206, $this->helperService->transResponse("erpssi.response.no_content"));
    }

    /**
     * @Route("/{idEmployee}", name="erpssi.holiday.create", methods={"POST"}, requirements={"idEmployee":"\d+"})
     * @ParamConverter("employee", class="AppBundle\Entity\Employee", options={"id" = "idEmployee"})
     * @param Employee $employee
     * @Security("is_granted('create', null)")
     * @return JsonResponse
     */
    public function createHolidayAction(Employee $employee)
    {
        $context = ['strategy' => self::CREATE, 'employee' => $employee];
        $content = json_decode($this->request->getContent(), true)['data'];

        $holidayEntity = $this->serializer->denormalize($content, Holiday::class, 'json', $context);
        if($holidayEntity instanceof Holiday) {
            $errors = $this->validator->validate($holidayEntity);
            if (count($errors) > 0) {
                return $this->helperService->createResponse($errors, 422, $this->helperService->transResponse("erpssi.response.create.error"));
            } else {
                $holiday = $this->holidayRepository->persist($holidayEntity, true);
                return $this->helperService->createResponse($this->serializer->normalize($holiday), 200, $this->helperService->transResponse("erpssi.response.create.success"));
            }
        }

        return $this->helperService->createResponse([], 422, $this->helperService->transResponse("erpssi.response.create.error"));
    }

    /**
     * @Route("/{idHoliday}", name="erpssi.holiday.update", methods={"PUT"}, requirements={"idHoliday":"\d+"})
     * @ParamConverter("holiday", class="AppBundle\Entity\Holiday", options={"id" = "idHoliday"})
     * @param Holiday $holiday
     * @Security("is_granted('edit', holiday)")
     * @return JsonResponse
     */
    public function updateHolidayAction(Holiday $holiday)
    {
        $content = json_decode($this->request->getContent(), true)['data'];
        $context = ['strategy' => self::UPDATE, 'holiday' => $holiday];

        $holidayEntity = $this->serializer->denormalize($content, Holiday::class, 'json', $context);
        $errors = $this->validator->validate($holidayEntity);

        if (count($errors) > 0) {
            return $this->helperService->createResponse($errors, 422, $this->helperService->transResponse("erpssi.response.update.error"));
        }

        return $this->helperService->createResponse($this->serializer->normalize($this->holidayRepository->flush($holidayEntity)), 200, $this->helperService->transResponse("erpssi.response.update.success"));

    }

    /**
     * @Route("/{idHoliday}", name="erpssi.holiday.delete", methods={"DELETE"}, requirements={"idHoliday":"\d+"})
     * @ParamConverter("holiday", class="AppBundle\Entity\Holiday", options={"id" = "idHoliday"})
     * @param Holiday $holiday
     * @Security("is_granted('delete', holiday)")
     * @return JsonResponse
     */
    public function deleteHolidayAction(Holiday $holiday)
    {
        $this->holidayRepository->delete($holiday);
        return $this->helperService->createResponse([], 200, $this->helperService->transResponse('erpssi.response.delete.success'));
    }

    /**
     * @Route("/{idHoliday}/handle/{state}", name="erpssi.holiday.handle", methods={"PUT"}, requirements={"idHoliday":"\d+", "state":"\d+"})
     * @ParamConverter("holiday", class="AppBundle\Entity\Holiday", options={"id" = "idHoliday"})
     * @param Holiday $holiday
     * @Security("is_granted('handle', holiday)")
     * @return JsonResponse
     */
    public function handleHoliday(Holiday $holiday)
    {
        switch (intval($this->request->attributes->get('state'))) {
            case Holiday::STATE_VALIDATED : $this->holidayRepository->validate($holiday); break;
            case Holiday::STATE_DENIED : $this->holidayRepository->denied($holiday); break;
        }
        return $this->helperService->createResponse($this->serializer->normalize($holiday), 200, $this->helperService->transResponse('erpssi.response.handle.success'));
    }

}