<?php

namespace AppBundle\Controller;
use AppBundle\Entity\Employee;
use AppBundle\Entity\ExpenseAccount;
use AppBundle\Repository\ExpenseAccountRepository;
use AppBundle\Service\FileUploader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use \Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Validator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ExpenseAccountController
 * @package AppBundle\Controller
 * @Route("/expense-accounts", service= "erpssi.expense_account_controller")
 */

class ExpenseAccountController extends ServiceController
{
    const UPDATE = 'update';
    const CREATE = 'create';

    /**
     * @var ExpenseAccountRepository $expenseAccountRepository
     */
    private $expenseAccountRepository;

    /**
     * @var FileUploader
     */
    private $fileUploader;


    /**
     * ExpenseAccountController constructor.
     * @param ExpenseAccountRepository $expenseAccountRepository
     */
    public function __construct(ExpenseAccountRepository $expenseAccountRepository, FileUploader $fileUploader)
    {
        $this->expenseAccountRepository = $expenseAccountRepository;
        $this->fileUploader = $fileUploader;
    }

    /**
     * @Route("/{idExpenseAccount}", name="erpssi.expense_account.get", methods={"GET"}, requirements={"idExpenseAccount":"\d+"})
     * @ParamConverter("expenseAccount", class="AppBundle\Entity\ExpenseAccount", options={"id" = "idExpenseAccount"})
     * @param ExpenseAccount $expenseAccount
     * @Security("is_granted('get', expenseAccount)")
     * @return JsonResponse
     */
    public function getExpenseAccount(ExpenseAccount $expenseAccount)
    {
        if($expenseAccount) {
            return $this->helperService->createResponse($this->serializer->normalize($expenseAccount), 200, $this->helperService->transResponse('erpssi.response.get.success'));
        }
        return $this->helperService->createResponse([], 422, $this->helperService->transResponse('erpssi.response.no_content'));
    }

    /**
     * @Route("", name="erpssi.expense_account.get_all", methods={"GET"})
     * @Method({"GET"})
     * @Security("is_granted('get_all', null)")
     * @return JsonResponse
     */
    public function getAllExpenseAccount()
    {
        $expenseAccounts = $this->entityProvider->getAllDecision($this->tokenStorage->getToken()->getUser(), $this->expenseAccountRepository);

        if($expenseAccounts) {
            return $this->helperService->createResponse($this->serializer->normalize($expenseAccounts), 200, $this->helperService->transResponse("erpssi.response.get_all.success"));
        }
        return $this->helperService->createResponse([], 206, $this->helperService->transResponse("erpssi.response.no_content"));

    }


    /**
     * @Route("/employee/{idEmployee}", name="erpssi.expense_account.get_all_by_user", methods={"GET"}, requirements={"idEmployee":"\d+"})
     * @ParamConverter("employee", class="AppBundle\Entity\Employee", options={"id" = "idEmployee"})
     * @Method({"GET"})
     * @Security("is_granted('get_all_by', employee)")
     * @param Employee $employee
     * @return JsonResponse
     */
    public function getAllExpenseAccountByEmployee(Employee $employee)
    {
        $expenseAccounts = $this->expenseAccountRepository->findBy(['employee' => $employee]);
        if($expenseAccounts) {
            return $this->helperService->createResponse($this->serializer->normalize($expenseAccounts), 200, $this->helperService->transResponse("erpssi.response.get_all.success"));
        }
        return $this->helperService->createResponse([], 206, $this->helperService->transResponse("erpssi.response.no_content"));
    }

    /**
     * Create method does not concern update image, to update expense's account image, you need to use updateExpenseAccountImageAction
     * @Route("/{idEmployee}", name="erpssi.expense_account.create", methods={"POST"}, requirements={"idEmployee":"\d+"})
     * @ParamConverter("employee", class="AppBundle\Entity\Employee", options={"id" = "idEmployee"})
     * @param Employee $employee
     * @Security("is_granted('create', null)")
     * @return JsonResponse
     */
    public function createExpenseAccountAction(Employee $employee)
    {
        $content = json_decode($this->request->getContent(), true)['data'];

        $context = ['strategy' => self::CREATE, 'employee' => $employee];

        $expenseAccountEntity = $this->serializer->denormalize($content, ExpenseAccount::class, 'json', $context);

        if($expenseAccountEntity instanceof ExpenseAccount) {

            $errors = $this->validator->validate($expenseAccountEntity);

            if (count($errors) > 0) {
                return $this->helperService->createResponse($this->serializer->normalize($errors), 422, $this->helperService->transResponse("erpssi.response.create.error"));
            } else {
                $expenseAccount = $this->expenseAccountRepository->persist($expenseAccountEntity, true);
                return $this->helperService->createResponse($this->serializer->normalize($expenseAccount), 200, $this->helperService->transResponse("erpssi.response.create.success"));
            }
        }

        return $this->helperService->createResponse([], 422, $this->helperService->transResponse("erpssi.response.create.error"));

    }

    /**
     * Update method does not concern update image, to update expense's account image, you need to use updateExpenseAccountImageAction
     * @Route("/{idExpenseAccount}", name="erpssi.expense_account.update", methods={"PUT"}, requirements={"idExpenseAccount":"\d+"})
     * @ParamConverter("expenseAccount", class="AppBundle\Entity\ExpenseAccount", options={"id" = "idExpenseAccount"})
     * @param ExpenseAccount $expenseAccount
     * @Security("is_granted('edit', expenseAccount)")
     * @return JsonResponse
     */
    public function updateExpenseAccountAction(ExpenseAccount $expenseAccount)
    {
        $content = json_decode($this->request->getContent(), true)['data'];

        $context = ['strategy' => self::UPDATE, 'expense_account' => $expenseAccount];

        $expenseAccountEntity = $this->serializer->denormalize($content, ExpenseAccount::class, 'json', $context);

        $errors = $this->validator->validate($expenseAccountEntity);

        if (count($errors) > 0) {
            return $this->helperService->createResponse($this->serializer->normalize($errors), 422, $this->helperService->transResponse("erpssi.response.create.error"));
        } else {
            return $this->helperService->createResponse($this->serializer->normalize($this->expenseAccountRepository->flush($expenseAccountEntity)), 200, $this->helperService->transResponse("erpssi.response.update.success"));
        }

    }

    /**
     * Due to upload file, this method can not be a PUT one // cf symfony doc
     * @Route("/{idExpenseAccount}/image", name="erpssi.expense_account.update_image", methods={"POST"}, requirements={"idExpenseAccount":"\d+"})
     * @ParamConverter("expenseAccount", class="AppBundle\Entity\ExpenseAccount", options={"id" = "idExpenseAccount"})
     * @param ExpenseAccount $expenseAccount
     * @Security("is_granted('edit', expenseAccount)")
     * @return JsonResponse
     */
    public function updateExpenseAccountImageAction(ExpenseAccount $expenseAccount)
    {
        if($this->request->files->get('image')) {
            $file = $this->request->files->get('image');
        } else {
            $file = $this->request->files->get('file');
        }

        if($file) {
            $fileName = $this->fileUploader->upload($file);
            $this->expenseAccountRepository->updateImage($expenseAccount, $fileName);
            return $this->helperService->createResponse($this->serializer->normalize($expenseAccount), 200, $this->helperService->transResponse("erpssi.response.update.success"));
        }

        return $this->helperService->createResponse([], 206, $this->helperService->transResponse("erpssi.response.create.error"));

    }

    /**
     * @Route("/{idExpenseAccount}", name="erpssi.expense_account.delete", methods={"DELETE"}, requirements={"idExpenseAccount":"\d+"})
     * @ParamConverter("expenseAccount", class="AppBundle\Entity\ExpenseAccount", options={"id" = "idExpenseAccount"})
     * @param ExpenseAccount $expenseAccount
     * @Security("is_granted('delete', expenseAccount)")
     * @return JsonResponse
     */
    public function deleteExpenseAccountAction(ExpenseAccount $expenseAccount)
    {
        $this->expenseAccountRepository->delete($expenseAccount);
        return $this->helperService->createResponse([], 200, $this->helperService->transResponse('erpssi.response.delete.success'));
    }

    /**
     * @Route("/{idExpenseAccount}/handle/{state}", name="erpssi.expense_account.handle", methods={"PUT"}, requirements={"idExpenseAccount":"\d+", "state":"\d+"})
     * @ParamConverter("expenseAccount", class="AppBundle\Entity\ExpenseAccount", options={"id" = "idExpenseAccount"})
     * @param ExpenseAccount $expenseAccount
     * @Security("is_granted('handle', expenseAccount)")
     * @return JsonResponse
     */
    public function handleExpenseAccount(ExpenseAccount $expenseAccount)
    {
        switch (intval($this->request->attributes->get('state'))) {
            case ExpenseAccount::EXPENSE_ACCOUNT_VALIDATED_STATE : $this->expenseAccountRepository->validate($expenseAccount); break;
            case ExpenseAccount::EXPENSE_ACCOUNT_DENIED_STATE : $this->expenseAccountRepository->denied($expenseAccount); break;
        }
        return $this->helperService->createResponse($this->serializer->normalize($expenseAccount), 200, $this->helperService->transResponse('erpssi.response.validate.success'));
    }
}