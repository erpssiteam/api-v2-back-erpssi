<?php

namespace AppBundle\Controller;
use AppBundle\Entity\Cra;
use AppBundle\Entity\Customer;
use AppBundle\Entity\Employee;
use AppBundle\Repository\CraRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use \Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Validator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class CraController
 * @package AppBundle\Controller
 * @Security("has_role('ROLE_ADMIN')")
 * @Route("/cras", service= "erpssi.cra_controller")
 */

class CraController extends ServiceController
{
    const UPDATE = 'update';
    const CREATE = 'create';

    /**
     * @var CraRepository $craRepository
     */
    private $craRepository;

    /**
     * CraController constructor.
     * @param CraRepository $craRepository
     */
    public function __construct(CraRepository $craRepository)
    {
        $this->craRepository = $craRepository;
    }

    /**
     * @Route("/{idCra}", name="erpssi.cra.get", methods={"GET"}, requirements={"idCra":"\d+"})
     * @ParamConverter("cra", class="AppBundle\Entity\Cra", options={"id" = "idCra"})
     * @Security("is_granted('get', cra)")
     * @param Cra $cra
     * @return JsonResponse
     */
    public function getCraAction(Cra $cra)
    {
        if($cra) {
            return $this->helperService->createResponse($this->serializer->normalize($cra), 200, $this->helperService->transResponse('erpssi.response.get.success'));
        }
        return $this->helperService->createResponse([], 206, $this->helperService->transResponse('erpssi.response.no_content'));
    }

    /**
     * @Route("", name="erpssi.cra.get_all", methods={"GET"})
     * @Method({"GET"})
     * @Security("is_granted('get_all', null)")
     * @return JsonResponse
     */
    public function getAllCraAction()
    {

        $cras = $this->entityProvider->getAllDecision($this->tokenStorage->getToken()->getUser(), $this->craRepository);

        if($cras) {
            return $this->helperService->createResponse($this->serializer->normalize($cras), 200, $this->helperService->transResponse("erpssi.response.get_all.success"));
        }
        return $this->helperService->createResponse([], 206, $this->helperService->transResponse("erpssi.response.no_content"));
    }

    /**
     * @Route("/employee/{idEmployee}", name="erpssi.cra.get_all_by_user", methods={"GET"}, requirements={"idEmployee":"\d+"})
     * @ParamConverter("employee", class="AppBundle\Entity\Employee", options={"id" = "idEmployee"})
     * @Method({"GET"})
     * @Security("is_granted('get_all_by', employee)")
     * @param Employee $employee
     * @return JsonResponse
     */
    public function getAllCraByEmployee(Employee $employee)
    {
        $cras = $this->craRepository->findBy(['employee' => $employee], ['createdAt' => 'desc']);
        if($cras) {
            return $this->helperService->createResponse($this->serializer->normalize($cras), 200, $this->helperService->transResponse("erpssi.response.get_all.success"));
        }
        return $this->helperService->createResponse([], 206, $this->helperService->transResponse("erpssi.response.no_content"));
    }

    /**
     * @Route("/employee/{idEmployee}/customer/{idCustomer}", name="erpssi.cra.create", methods={"POST"}, requirements={"idEmployee":"\d+", "idCustomer":"\d+"})
     * @ParamConverter("customer", class="AppBundle\Entity\Customer", options={"id" = "idCustomer"})
     * @ParamConverter("employee", class="AppBundle\Entity\Employee", options={"id" = "idEmployee"})
     * @Security("is_granted('create', null)")
     * @param Customer $customer
     * @param Employee $employee
     * @return JsonResponse
     */
    public function createCraAction(Customer $customer, Employee $employee)
    {
        $context = ['strategy' => self::CREATE, 'customer' => $customer, 'employee' => $employee];
        $content = json_decode($this->request->getContent(), true)['data'];
        $craEntity = $this->serializer->denormalize($content, Cra::class, 'json', $context);

        if($craEntity instanceof Cra) {

            if (count($this->validator->validate($craEntity)) > 0) {
                return $this->helperService->createResponse($this->serializer->normalize($craEntity), 422, $this->helperService->transResponse("erpssi.response.create.error"));
            } else {
                $cra = $this->craRepository->persist($craEntity, true);
                return $this->helperService->createResponse($this->serializer->normalize($cra), 200, $this->helperService->transResponse("erpssi.response.create.success"));
            }
        }

        return $this->helperService->createResponse([], 422, $this->helperService->transResponse("erpssi.response.create.error"));
    }

    /**
     * @Route("/{idCra}/employee/{idEmployee}/customer/{idCustomer}", name="erpssi.cra.update", methods={"PUT"}, requirements={"idEmployee":"\d+", "idCustomer":"\d+", "idCra":"\d+"})
     * @ParamConverter("cra", class="AppBundle\Entity\Cra", options={"id" = "idCra"})
     * @ParamConverter("customer", class="AppBundle\Entity\Customer", options={"id" = "idCustomer"})
     * @ParamConverter("employee", class="AppBundle\Entity\Employee", options={"id" = "idEmployee"})
     * @Security("is_granted('edit', cra)")
     * @param Cra $cra
     * @param Employee $employee
     * @param Customer $customer
     * @return JsonResponse
     * @internal param JobProfile $jobProfile
     */
    public function updateCraAction(Cra $cra, Employee $employee, Customer $customer)
    {
        $content = json_decode($this->request->getContent(), true)['data'];
        $context = ['strategy' => self::UPDATE, 'customer' => $customer, 'employee' => $employee, 'cra' => $cra];

        $craNormalizedEntity = $this->serializer->denormalize($content, Cra::class, 'json', $context);

        if (count($this->validator->validate($craNormalizedEntity)) > 0) {
            return $this->helperService->createResponse($this->serializer->normalize($craNormalizedEntity), 422, $this->helperService->transResponse("erpssi.response.update.error"));
        } else {
            return $this->helperService->createResponse($this->serializer->normalize($this->craRepository->flush($craNormalizedEntity)), 200, $this->helperService->transResponse("erpssi.response.update.success"));
        }

    }

    /**
     * @Route("/{idCra}", name="erpssi.cra.delete", methods={"DELETE"}, requirements={"idCra":"\d+"})
     * @ParamConverter("cra", class="AppBundle\Entity\Cra", options={"id" = "idCra"})
     * @Security("is_granted('delete', cra)")
     * @param Cra $cra
     * @return JsonResponse
     */
    public function deleteJobProfileAction(Cra $cra)
    {
        $this->craRepository->delete($cra);
        return $this->helperService->createResponse([], 200, $this->helperService->transResponse('erpssi.response.delete.success'));
    }

    /**
     * @Route("/{idCra}/handle/{state}", name="erpssi.cra.handle", methods={"PUT"}, requirements={"idCra":"\d+", "state":"\d+"})
     * @ParamConverter("cra", class="AppBundle\Entity\Cra", options={"id" = "idCra"})
     * @Security("is_granted('handle', cra)")
     * @param Cra $cra
     * @return JsonResponse
     */
    public function handleCra(Cra $cra)
    {
        switch (intval($this->request->attributes->get('state'))) {
            case Cra::STATE_VALIDATED : $this->craRepository->validate($cra); break;
            case Cra::STATE_DENIED : $this->craRepository->denied($cra); break;
        }
        return $this->helperService->createResponse($this->serializer->normalize($cra), 200, $this->helperService->transResponse('erpssi.response.handle.success'));
    }
}