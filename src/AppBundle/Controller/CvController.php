<?php

namespace AppBundle\Controller;
use AppBundle\Entity\Apply;
use AppBundle\Repository\ApplyRepository;
use AppBundle\Service\FileUploader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use \Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Validator;

/**
 * Class CvController
 * @package AppBundle\Controller
 * @Route("/cvs", service= "erpssi.cv_controller")
 * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_RH') or has_role('ROLE_MANAGER')")
 */
class CvController extends ServiceController
{

    /**
     * @var ApplyRepository $applyRepository
     */
    private $applyRepository;

    /**
     * @var FileUploader
     */
    private $fileUploader;


    /**
     * JobProfileController constructor.
     * @param ApplyRepository $applyRepository
     * @param FileUploader $fileUploader
     * @internal param ValidatorInterface $validator
     */
    public function __construct(ApplyRepository $applyRepository, FileUploader $fileUploader)
    {
        $this->applyRepository = $applyRepository;
        $this->fileUploader = $fileUploader;
    }

    /**
     * @Route("", name="erpssi.cv.get_all", methods={"GET"})
     * @Method({"GET"})
     * @return JsonResponse
     */
    public function getAllCvAction()
    {
        $applies = $this->applyRepository->findAll();

        if($applies) {
            $normalizedApplies = $this->serializer->normalize($applies);
            $cvs = [];
            foreach($normalizedApplies as $normalizedApply) {
                /** @var Apply $normalizedApply */
                $cvs[] = ['src' => $normalizedApply['cv']];
            }
            return $this->helperService->createResponse($cvs, 200, $this->helperService->transResponse("erpssi.response.get_all.success"));
        }
        return $this->helperService->createResponse([], 206, $this->helperService->transResponse("erpssi.response.no_content"));
    }

}