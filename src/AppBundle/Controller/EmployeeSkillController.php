<?php
/**
 * Created by PhpStorm.
 * User: reslene
 * Date: 02/06/16
 * Time: 10:13
 */

namespace AppBundle\Controller;
use AppBundle\Entity\Employee;
use AppBundle\Entity\EmployeeSkill;
use AppBundle\Entity\Skill;
use AppBundle\Repository\EmployeeSkillRepository;
use AppBundle\Repository\SkillRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class EmployeeSkillController
 * @package AppBundle\Controller
 * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_RH') or has_role('ROLE_MANAGER') or has_role('ROLE_COLLABORATOR') or has_role('ROLE_NEWS')")
 * @Route("/employee-skills", service="erpssi.employee_skill_controller")
 */
class EmployeeSkillController extends ServiceController {


    const UPDATE = 'update';
    const CREATE = 'create';


    /**
     * @var EmployeeSkillRepository $employeeSkillRepository
     */
    private $employeeSkillRepository;

    /**
     * @var SkillRepository $skillRepository
     */
    private $skillRepository;

    /**
     * EmployeeSkillController constructor.
     * @param EmployeeSkillRepository $employeeSkillRepository
     * @param SkillRepository $skillRepository
     */
    public function __construct(EmployeeSkillRepository $employeeSkillRepository, SkillRepository $skillRepository)
    {
        $this->employeeSkillRepository = $employeeSkillRepository;
        $this->skillRepository = $skillRepository;
    }

    /**
     * @Route("/{idEmployee}", name="erpssi.employee_skill.get", methods={"GET"}, requirements={"idEmployee":"\d+"})
     * @ParamConverter("employee", class="AppBundle\Entity\Employee", options={"id" = "idEmployee"})
     * @param Employee $employee
     * @return JsonResponse
     */
    public function getEmployeeSkillsAction(Employee $employee)
    {
        if($employee instanceof Employee){
            $employeeSkills = $this->employeeSkillRepository->findBy(['employee' => $employee->getId()]);
            if(!$employeeSkills) {
                $employeeSkills = $this->createEntryEmployeeSkills($employee);

            }
            return $this->helperService->createResponse($this->serializer->normalize($employeeSkills), 200, $this->helperService->transResponse('erpssi.response.get.success'));
        }
        return $this->helperService->createResponse([], 206, $this->helperService->transResponse('erpssi.response.no_content'));
    }

    private function createEntryEmployeeSkills(Employee $employee){
        $skills = $this->skillRepository->findBy(['depreciated' => FALSE]);

        foreach ($skills as $skill){
            if($skill instanceof Skill){
                $content = ['level' => '', 'validate' => FALSE, 'description' => ''];
                $employeeSkill = $this->serializer->denormalize($content,EmployeeSkill::class,'json',['strategy' => self::CREATE, 'employee' => $employee, 'skill' => $skill]);
                $this->employeeSkillRepository->persist($employeeSkill, TRUE);
            }
        }

        return $this->employeeSkillRepository->findBy(['employee' => $employee->getId()]);
    }

    public function addEntryEmployeeSkills(Employee $employee, Skill $skill){
            if($skill instanceof Skill){
                $content = ['level' => '', 'validate' => FALSE, 'description' => ''];
                $employeeSkill = $this->serializer->denormalize($content,EmployeeSkill::class,'json',['strategy' => self::CREATE, 'employee' => $employee, 'skill' => $skill]);
                $this->employeeSkillRepository->persist($employeeSkill, TRUE);
            }


        return $this->employeeSkillRepository->findBy(['employee' => $employee->getId()]);
    }



    /**
     * @Route("/{idEmployee}", name="erpssi.employee_skill.create", methods={"POST"}, requirements={"idEmployee":"\d+"})
     * @ParamConverter("employee", class="AppBundle\Entity\Employee", options={"id" = "idEmployee"})
     * @param Employee $employee
     * @return JsonResponse
     */
    public function assignEmployeeSkillsAction(Employee $employee){

        $content = json_decode($this->request->getContent(), true)['data'];
        $context = ['strategy' => self::CREATE, 'employee' => $employee];

        if($employee instanceof Employee){
            foreach($content['data'] as $skill){
                $skillEntity = $this->skillRepository->findOneBy(['id' => $skill['id'], 'depreciated' => FALSE]);
                if($skillEntity instanceof Skill){
                    $context['skill'] = $skillEntity;

                    /* Check if employeeSkill already exists*/
                    $employeeSkill = $this->checkIfEmployeeSkillExists($employee,$skillEntity);
                    if($employeeSkill instanceof EmployeeSkill){
                        $context['strategy'] = self::UPDATE;
                        $context['employee_skill'] = $employeeSkill;
                    }

                    $employeeSkillEntity = $this->serializer->denormalize($skill, EmployeeSkill::class, 'json', $context);

                    if($employeeSkillEntity instanceof EmployeeSkill) {
                        $this->employeeSkillRepository->flush($employeeSkillEntity);
                    }
                }
            }
            return $this->helperService->createResponse('OKAY', 200, $this->helperService->transResponse("erpssi.response.create.success"));
        }

        return $this->helperService->createResponse([], 206, $this->helperService->transResponse("erpssi.response.create.error"));
    }

    private function checkIfEmployeeSkillExists(Employee $employee, Skill $skill){

        $employee_skill = $this->employeeSkillRepository->findOneBy(['employee' => $employee, 'skill' => $skill]);
        if($employee_skill instanceof EmployeeSkill){
            return $employee_skill;
        }

        return FALSE;
    }

    /**
     * @Route("/{idEmployeeSkill}", name="erpssi.employee_skill.delete", methods={"DELETE"}, requirements={"idEmployeeSkill":"\d+"})
     * @ParamConverter("employeeSkill", class="AppBundle\Entity\EmployeeSkill", options={"id" = "idEmployeeSkill"})
     * @param EmployeeSkill $employeeSkill
     * @return JsonResponse
     */
    public function deleteEmployeeSkillAction(EmployeeSkill $employeeSkill)
    {
        $this->employeeSkillRepository->delete($employeeSkill);
        return $this->helperService->createResponse([], 200, $this->helperService->transResponse('erpssi.response.delete.success'));
    }

    public function deleteEmployeeSkillsBySkillAction(Skill $skill){
        $employee_skills = $this->employeeSkillRepository->findBy(['skill' => $skill]);

        foreach ($employee_skills as $employee_skill) {
            $this->employeeSkillRepository->delete($employee_skill);
        }
    }

}