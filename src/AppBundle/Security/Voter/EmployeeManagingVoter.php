<?php
namespace AppBundle\Security\Voter;
use AppBundle\Entity\Employee;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use AppBundle\Repository\EmployeeRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Created by PhpStorm.
 * User: rislou
 * Date: 11/06/17
 * Time: 16:40
 */
class EmployeeManagingVoter extends Voter
{
    const GET = 'get';
    const EDIT = 'edit';
    const GET_ALL = 'get_all';
    const DELETE = 'delete';
    const CREATE = 'create';

    /**
     * @var EmployeeRepository
     */
    private $employeeRepository;

    /**
     * EmployeeManagingVoter constructor.
     * @param EmployeeRepository $employeeRepository
     */
    public function __construct(EmployeeRepository $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array(self::GET, self::EDIT, self::GET_ALL, self::DELETE, self::CREATE))) {
            return false;
        }

        if($subject instanceof Employee || $subject == null) {
            return true;
        }

        return false;
    }


    /**
     * @param string $attribute
     * @param null $subject
     * @param TokenInterface $token
     * @return bool|void
     */
    protected function voteOnAttribute($attribute, $subject = null, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::GET_ALL:
                return $this->canGetAll($user);
            case self::GET:
                return $this->canGet($subject, $user);
            case self::EDIT:
                return $this->canEdit($subject, $user);
            case self::DELETE:
                return $this->canDelete($subject, $user);
            case self::CREATE:
                return $this->canCreate($user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param User $user
     * @return bool
     */
    private function canGetAll(User $user) {
        $role = $user->getRoles()[0];
        if($role === Role::ROLE_ADMIN || $role === Role::ROLE_RH || $role === Role::ROLE_MANAGER) {
            return true;
        }
        return false;
    }

    /**
     * @param User $user
     * @return bool
     */
    private function canCreate(User $user) {
      return $this->canGetAll($user);
    }

    /**
     * @param $subject
     * @param User $user
     * @return bool
     */
    private function canGet($subject, User $user) {
        /** @var Employee $subject */
        $role = $user->getRoles()[0];
        switch ($role) {
            case Role::ROLE_MANAGER:
                $manager = $this->employeeRepository->findOneBy(['user' => $user]);
                if($manager && $subject->getManagedBy()) {
                    /** @var Employee $manager */
                    if($manager->getId() === $subject->getManagedBy()->getId()) {
                        return true;
                    }
                    return false;
                } break;
            case Role::ROLE_RH:
                return true; break;
            case Role::ROLE_ADMIN:
                return true; break;
            default: return false; break;
        }
        return false;
    }

    /**
     * @param $subject
     * @param User $user
     * @return bool
     */
    private function canEdit($subject, User $user) {
        return $this->canGet($subject, $user);
    }

    /**
     * @param $subject
     * @param User $user
     * @return bool
     */
    private function canDelete($subject, User $user) {
        return $this->canGet($subject, $user);
    }

}