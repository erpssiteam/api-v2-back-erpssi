<?php
namespace AppBundle\Security\Voter;
use AppBundle\Entity\Employee;
use AppBundle\Entity\ExpenseAccount;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use AppBundle\Repository\EmployeeRepository;
use AppBundle\Repository\ExpenseAccountRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Created by PhpStorm.
 * User: rislou
 * Date: 11/06/17
 * Time: 16:40
 */
class ExpenseAccountManagingVoter extends Voter
{
    const GET = 'get';
    const EDIT = 'edit';
    const GET_ALL = 'get_all';
    const GET_ALL_BY = 'get_all_by';
    const DELETE = 'delete';
    const CREATE = 'create';
    const HANDLE = 'handle';

    /**
     * @var ExpenseAccountRepository
     */
    private $expenseAccountRepository;

    /**
     * @var EmployeeRepository
     */
    private $employeeRepository;

    /**
     * CraManagingVoter constructor.
     * @param ExpenseAccountRepository $expenseAccountRepository
     * @param EmployeeRepository $employeeRepository
     */
    public function __construct(ExpenseAccountRepository $expenseAccountRepository, EmployeeRepository $employeeRepository)
    {
        $this->expenseAccountRepository = $expenseAccountRepository;
        $this->employeeRepository = $employeeRepository;
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array(self::GET, self::GET_ALL_BY,self::EDIT, self::GET_ALL, self::DELETE, self::CREATE, self::HANDLE))) {
            return false;
        }

        if($attribute === self::GET_ALL_BY) {
            if($subject instanceof Employee) {
                return true;
            }
        }

        if($subject instanceof ExpenseAccount || $subject ==  null) {
            return true;
        }

        return false;
    }

    /**
     * @param string $attribute
     * @param null $subject
     * @param TokenInterface $token
     * @return bool|void
     */
    protected function voteOnAttribute($attribute, $subject = null, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::GET_ALL:
                return $this->canGetAll($user);
            case self::GET:
                return $this->canGet($subject, $user);
            case self::GET_ALL_BY:
                return $this->canGetAllBy($user, $subject);
            case self::EDIT:
                return $this->canEdit($subject, $user);
            case self::DELETE:
                return $this->canDelete($subject, $user);
            case self::CREATE:
                return $this->canCreate($user);
            case self::HANDLE:
                return $this->canHandle($subject, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param User $user
     * @return bool
     */
    private function canGetAll(User $user) {
        $role = $user->getRoles()[0];
        if($role === Role::ROLE_ADMIN || $role === Role::ROLE_RH || $role === Role::ROLE_COLLABORATOR || $role === Role::ROLE_MANAGER) {
            return true;
        }
        return false;
    }
    /**
     * @param User $user
     * @return bool
     */
    private function canGetAllBy(User $user, $object) {
        /** @var Employee $object */
        if($object->getUser()->getId() === $user->getId()) {
            $role = $user->getRoles()[0];
            if($role === Role::ROLE_MANAGER) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param User $user
     * @return bool
     */
    private function canCreate(User $user) {
      return true;
    }

    /**
     * @param $subject
     * @param User $user
     * @return bool
     */
    private function canGet($subject, User $user) {
        /** @var ExpenseAccount $subject */
        $role = $user->getRoles()[0];
        $employee = $this->employeeRepository->findOneBy(['user' => $user]);
        /** @var Employee $employee */
        switch ($role) {
            case Role::ROLE_COLLABORATOR:
                if($employee->getId() === $subject->getEmployee()->getId()) return true;
                return false; break;
            case Role::ROLE_MANAGER:
                if($subject->getEmployee()->getManagedBy()) {
                    if($subject->getEmployee()->getManagedBy()->getId() === $employee->getId()) return true;
                }
            return false; break;
            case Role::ROLE_RH:
                return true; break;
            case Role::ROLE_ADMIN:
                return true; break;
            default: return false; break;
        }
    }

    /**
     * @param $subject
     * @param User $user
     * @return bool
     */
    private function canEdit($subject, User $user) {
        return $this->canGet($subject, $user);
    }

    /**
     * @param $subject
     * @param User $user
     * @return bool
     */
    private function canDelete($subject, User $user) {
        return $this->canGet($subject, $user);
    }

    /**
     * @param $subject
     * @param User $user
     * @return bool
     */
    private function canHandle($subject, User $user) {
        return $this->canGet($subject, $user);
    }

}