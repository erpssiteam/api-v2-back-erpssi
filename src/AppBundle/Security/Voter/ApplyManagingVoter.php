<?php
namespace AppBundle\Security\Voter;
use AppBundle\Entity\Apply;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Created by PhpStorm.
 * User: rislou
 * Date: 11/06/17
 * Time: 16:40
 */
class ApplyManagingVoter extends Voter
{
    const GET = 'get';
    const EDIT = 'edit';
    const GET_ALL = 'get_all';
    const DELETE = 'delete';
    const HANDLE = 'handle';

    /**
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array(self::GET,self::EDIT, self::GET_ALL, self::DELETE, self::HANDLE))) {
            return false;
        }

        if($subject instanceof Apply || $subject == null) {
            return true;
        }

        return false;
    }

    /**
     * @param string $attribute
     * @param null $subject
     * @param TokenInterface $token
     * @return bool|void
     */
    protected function voteOnAttribute($attribute, $subject = null, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::GET_ALL:
                return $this->canGetAll($subject, $user);
            case self::GET:
                return $this->canGet($subject, $user);
            case self::EDIT:
                return $this->canEdit($subject, $user);
            case self::DELETE:
                return $this->canDelete($subject, $user);
            case self::HANDLE:
                return $this->canHandle($subject, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param User $user
     * @return bool
     */
    private function canGetAll($subject, User $user) {
        return $this->canEdit($subject, $user);
    }


    /**
     * @param $subject
     * @param User $user
     * @return bool
     */
    private function canGet($subject, User $user) {
        return $this->canEdit($subject, $user);
    }

    /**
     * @param $subject
     * @param User $user
     * @return bool
     */
    private function canEdit($subject, User $user) {
        $role = $user->getRoles()[0];
        if($role === Role::ROLE_ADMIN || $role === Role::ROLE_RH) {
            return true;
        }
        return false;
    }

    /**
     * @param $subject
     * @param User $user
     * @return bool
     */
    private function canDelete($subject, User $user) {
        return $this->canEdit($subject, $user);
    }

    /**
     * @param $subject
     * @param User $user
     * @return bool
     */
    private function canHandle($subject, User $user) {
        return $this->canEdit($subject, $user);
    }

}