<?php
namespace AppBundle\Security\Voter;
use AppBundle\Entity\Offer;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Created by PhpStorm.
 * User: rislou
 * Date: 11/06/17
 * Time: 16:40
 */
class OfferManagingVoter extends Voter
{
    const EDIT = 'edit';
    const GET_ALL_APPLIES = 'get_all_applies';
    const DELETE = 'delete';
    const HANDLE = 'handle';

    /**
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array(self::EDIT, self::DELETE, self::HANDLE, self::GET_ALL_APPLIES))) {
            return false;
        }

        if($subject instanceof Offer || $subject == null) {
            return true;
        }

        return false;
    }

    /**
     * @param string $attribute
     * @param null $subject
     * @param TokenInterface $token
     * @return bool|void
     */
    protected function voteOnAttribute($attribute, $subject = null, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::EDIT:
                return $this->canEdit($subject, $user);
            case self::DELETE:
                return $this->canDelete($subject, $user);
            case self::HANDLE:
                return $this->canHandle($subject, $user);
            case self::GET_ALL_APPLIES:
                return $this->canGetAllApplies($subject, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param $subject
     * @param User $user
     * @return bool
     */
    private function canEdit($subject, User $user) {
        $role = $user->getRoles()[0];
        if($role === Role::ROLE_ADMIN || $role === Role::ROLE_RH) {
            return true;
        }
        return false;
    }

    /**
     * @param $subject
     * @param User $user
     * @return bool
     */
    private function canDelete($subject, User $user) {
        return $this->canEdit($subject, $user);
    }

    /**
     * @param $subject
     * @param User $user
     * @return bool
     */
    private function canHandle($subject, User $user) {
        return $this->canEdit($subject, $user);
    }

    /**
     * @param $subject
     * @param User $user
     * @return bool
     */
    private function canGetAllApplies($subject, User $user) {
        return $this->canEdit($subject, $user);
    }

}