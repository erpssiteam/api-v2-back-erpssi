<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\EmployeeSkill;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadEmployeeSkillData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    public function load(ObjectManager $manager)
    {

//        $skill = new EmployeeSkill();
//        $skill->setLevel('10');
//        $skill->setValidate(TRUE);
//        $skill->setDescription('Bon niveau général');
//        $skill->setDateSkill(new \DateTime());
//        $skill->setSkill($this->getReference('php'));
//        $skill->setEmployee($this->getReference('employee'));
//        $manager->persist($skill);
//        $manager->flush();
//
//        $skill1 = new EmployeeSkill();
//        $skill1->setLevel('10');
//        $skill1->setValidate(TRUE);
//        $skill1->setDescription('Bon niveau général');
//        $skill1->setDateSkill(new \DateTime());
//        $skill1->setSkill($this->getReference('javascript'));
//        $skill1->setEmployee($this->getReference('employee'));
//        $manager->persist($skill1);
//        $manager->flush();
//
//        $skill2 = new EmployeeSkill();
//        $skill2->setLevel('10');
//        $skill2->setValidate(FALSE);
//        $skill2->setDescription('Niveau de merde');
//        $skill2->setDateSkill(new \DateTime());
//        $skill2->setSkill($this->getReference('seo'));
//        $skill2->setEmployee($this->getReference('employee1'));
//        $manager->persist($skill2);
//        $manager->flush();
//
//        $skill3 = new EmployeeSkill();
//        $skill3->setLevel('10');
//        $skill3->setValidate(FALSE);
//        $skill3->setDescription('very well');
//        $skill3->setDateSkill(new \DateTime());
//        $skill3->setSkill($this->getReference('ga'));
//        $skill3->setEmployee($this->getReference('employee1'));
//        $manager->persist($skill3);
//        $manager->flush();
//
//        $skill4 = new EmployeeSkill();
//        $skill4->setLevel('10');
//        $skill4->setValidate(TRUE);
//        $skill4->setDescription('very well');
//        $skill4->setDateSkill(new \DateTime());
//        $skill4->setSkill($this->getReference('word'));
//        $skill4->setEmployee($this->getReference('employee2'));
//        $manager->persist($skill4);
//        $manager->flush();
//
//        $skill5 = new EmployeeSkill();
//        $skill5->setLevel('10');
//        $skill5->setValidate(TRUE);
//        $skill5->setDescription('very well');
//        $skill5->setDateSkill(new \DateTime());
//        $skill5->setSkill($this->getReference('word'));
//        $skill5->setEmployee($this->getReference('employee1'));
//        $manager->persist($skill5);
//        $manager->flush();
//
//        $skill6 = new EmployeeSkill();
//        $skill6->setLevel('10');
//        $skill6->setValidate(TRUE);
//        $skill6->setDescription('very well');
//        $skill6->setDateSkill(new \DateTime());
//        $skill6->setSkill($this->getReference('excel'));
//        $skill6->setEmployee($this->getReference('employee1'));
//        $manager->persist($skill6);
//        $manager->flush();

    }


    public function setContainer(ContainerInterface $container = null)
    {
        // TODO: Implement setContainer() method.
    }

    public function getOrder()
    {
        return 17;
    }
}
