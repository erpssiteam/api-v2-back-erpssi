<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Employee;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadEmployeeData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
    * @var ContainerInterface
    */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {

        $employee = new Employee('Jean','Delarue', new \DateTime(), 'admin@erpssi.fr', '30 rue du fou', '69003', 'Lyon', $this->getReference('user'));
        $this->addReference('employee', $employee);
        $employee->setCin('45H984615DF545');
        $employee->setGmail('admin@gmail.com');
        $employee->setSituationFamiliale('couple');
        $manager->persist($employee);
        $manager->flush();
        $this->setReference('employee', $employee);


        $employee1 = new Employee('Hervé','Millet', new \DateTime(), 'manager@erpssi.fr', '1 rue du loup', '69003', 'Lyon', $this->getReference('user2'));
        $this->addReference('employee1', $employee1);
        $employee1->setCin('45H984615DF545');
        $employee1->setGmail('admin@gmail.com');
        $employee1->setSituationFamiliale('couple');
        $manager->persist($employee1);
        $manager->flush();
        $this->setReference('employee1', $employee1);


        $employee2 = new Employee('Nicolas','Rotin', new \DateTime(), 'rh@erpssi.fr', '71 rue du progrès', '69003', 'Lyon', $this->getReference('user3'));
        $this->addReference('employee2', $employee2);
        $employee2->setCin('45H984615DF545');
        $employee2->setGmail('admin@gmail.com');
        $employee2->setSituationFamiliale('couple');
        $manager->persist($employee2);
        $manager->flush();
        $this->setReference('employee2', $employee2);

        $employee3 = new Employee('Valérie','Torin', new \DateTime(), 'collabo@erpssi.fr', '30 rue du bar', '69100', 'Lyon', $this->getReference('user4'), $this->getReference('employee1'));
        $this->addReference('employee3', $employee3);
        $employee3->setCin('98984984HHHH787');
        $employee3->setGmail('collabo@gmail.com');
        $employee3->setSituationFamiliale('célibataire');
        $manager->persist($employee3);
        $manager->flush();

        $employee4 = new Employee('Alex','Diani', new \DateTime(), 'collabo2@erpssi.fr', '22 rue du bar a jeu', '69100', 'Lyon', $this->getReference('user5'), $this->getReference('employee1'));
        $this->addReference('employee4', $employee4);
        $employee4->setCin('000000000000');
        $employee4->setGmail('collabo2@gmail.com');
        $employee4->setSituationFamiliale('couple');
        $manager->persist($employee4);
        $manager->flush();
        
        $employee5 = new Employee('Kali','Nouado', new \DateTime(), 'news@erpssi.fr', '5 rue de varenne', '21000', 'Dijon', $this->getReference('user7'), $this->getReference('employee1'));
        $this->addReference('employee5', $employee5);
        $employee5->setCin('000000000000');
        $employee5->setGmail('news@gmail.com');
        $employee5->setSituationFamiliale('couple');
        $manager->persist($employee5);
        $manager->flush();

     }

    public function getOrder()
    {
        return 10;
    }
}
