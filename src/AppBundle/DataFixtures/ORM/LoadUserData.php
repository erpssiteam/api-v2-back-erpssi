<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
    * @var ContainerInterface
    */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $encoder = $this->container->get('security.password_encoder');

        $user = new User('userAdmin','erpssi', $this->getReference('roleAdmin'));
        $this->addReference('user', $user);
        $user->setCreatedAt(new \DateTime());
        $user->setModifiedAt(new \DateTime());
        $user->setPassword($encoder->encodePassword($user, 'erpssi'));
        $manager->persist($user);
        $manager->flush();

        $user1 = new User('userCollabo1','erpssi', $this->getReference('roleCollaborator'));
        $this->addReference('user1', $user1);
        $user1->setCreatedAt(new \DateTime());
        $user1->setModifiedAt(new \DateTime());
        $user1->setPassword($encoder->encodePassword($user1, 'erpssi'));
        $manager->persist($user1);
        $manager->flush();

        $user2 = new User('userManager','erpssi', $this->getReference('roleManager'));
        $this->addReference('user2', $user2);
        $user2->setCreatedAt(new \DateTime());
        $user2->setModifiedAt(new \DateTime());
        $user2->setPassword($encoder->encodePassword($user2, 'erpssi'));
        $manager->persist($user2);
        $manager->flush();

        $user3 = new User('userRh','erpssi', $this->getReference('roleRh'));
        $this->addReference('user3', $user3);
        $user3->setCreatedAt(new \DateTime());
        $user3->setModifiedAt(new \DateTime());
        $user3->setPassword($encoder->encodePassword($user3, 'erpssi'));
        $manager->persist($user3);
        $manager->flush();

        $user4 = new User('userCollabo','erpssi', $this->getReference('roleCollaborator'));
        $this->addReference('user4', $user4);
        $user4->setCreatedAt(new \DateTime());
        $user4->setModifiedAt(new \DateTime());
        $user4->setPassword($encoder->encodePassword($user4, 'erpssi'));
        $manager->persist($user4);
        $manager->flush();
        
        $user5 = new User('userCollabo2','erpssi', $this->getReference('roleCollaborator'));
        $this->addReference('user5', $user5);
        $user5->setCreatedAt(new \DateTime());
        $user5->setModifiedAt(new \DateTime());
        $user5->setPassword($encoder->encodePassword($user5, 'erpssi'));
        $manager->persist($user5);
        $manager->flush();

        $user6 = new User('userCollabo3','erpssi', $this->getReference('roleCollaborator'));
        $this->addReference('user6', $user6);
        $user6->setCreatedAt(new \DateTime());
        $user6->setModifiedAt(new \DateTime());
        $user6->setPassword($encoder->encodePassword($user6, 'erpssi'));
        $manager->persist($user6);
        $manager->flush();

        $user7 = new User('userNews','erpssi', $this->getReference('roleNews'));
        $this->addReference('user7', $user7);
        $user7->setCreatedAt(new \DateTime());
        $user7->setModifiedAt(new \DateTime());
        $user7->setPassword($encoder->encodePassword($user7, 'erpssi'));
        $manager->persist($user7);
        $manager->flush();

     }

    public function getOrder()
    {
        return 2;
    }
}
