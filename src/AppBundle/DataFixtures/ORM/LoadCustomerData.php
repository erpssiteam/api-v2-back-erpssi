<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Customer;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadCustomerData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {

        $customer = new Customer('customer1','customer1', new \DateTime(), 'collaborateur1@erpssi.fr', '30 rue Georges Pompidou', '69003', 'Lyon', $this->getReference('user1'));
        $this->addReference('customer', $customer);
        $customer->setSiren('45H984615DF545');
        $customer->setSiret('45H984615DF545');
        $customer->setWebsite('www.google.com');
        $customer->setContactName('Yeppppp');
        $customer->setContactPhone('0946766554');
        $customer->setContactEmail('collaborateur@erpssi.fr');
        $customer->setGeneralEmail('general@erpssi.fr');
        $manager->persist($customer);
        $manager->flush();

        $customer1 = new Customer('customer2','customer2', new \DateTime(), 'collaborateur2@erpssi.fr', '30 rue Georges Pompidou', '69003', 'Lyon', $this->getReference('user6'));
        $this->addReference('customer1', $customer1);
        $customer1->setSiren('45H984615DF545');
        $customer1->setSiret('45H984615DF545');
        $customer1->setWebsite('www.google.com');
        $customer1->setContactName('Yeppppp');
        $customer1->setContactPhone('0946766554');
        $customer1->setContactEmail('collaborateur@erpssi.fr');
        $customer1->setGeneralEmail('general@erpssi.fr');
        $manager->persist($customer1);
        $manager->flush();

    }

    public function getOrder()
    {
        return 11;
    }
}
