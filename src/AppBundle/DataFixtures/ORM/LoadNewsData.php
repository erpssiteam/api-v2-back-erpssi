<?php
/**
 * Created by PhpStorm.
 * User: whiskey
 * Date: 24/03/17
 * Time: 14:49
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\News;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadNewsData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    public function load(ObjectManager $manager)
    {
        $news = new News(
            "Ressources humaines, ce CV peut cacher le ransomware GoldenEye",
           '<div class="storyBody">
				
		<p><b>Mise à jour 15/05/2017 :</b> La DSI est souvent appelée au secours par les direction métiers dans des situations de détresse absolues. La propagation récente du ransomware <a href="http://www.zdnet.fr/actualites/wannacrypt-la-seconde-vague-arrive-alors-soyez-prets-39852440.htm">WannaCrypt</a> risque donc de faire sonner le téléphone de la DSI, avec une voix stressée au bout du fil. Surtout que par nature les rançongiciels ciblent les directions métiers. Exemple avec GoldenEye, qui en veut tout particulièrement aux ressources humaines.


</p><p><img src="http://www.zdnet.fr/i/edit/ne/2017/01/goldeneye-ransomware-note.jpg" align="bottom" width="620" vspace="5" hspace="10" height="416"> <br></p><p>Des cybercriminels se présentent comme des demandeurs d\'emploi dans le cadre d\'une nouvelle campagne <a href="http://blog.checkpoint.com/2017/01/03/looking-new-employee-beware-new-ransomware-campaign/" target="_blank">visant à infecter les départements ressources humaines</a> des entreprises avec le ransomware GoldenEye. Cette <a href="http://www.zdnet.fr/actualites/petya-un-nouveau-ransomware-en-circulation-39834844.htm">variante du ransomware Petya</a> ne vise pas les RH par hasard. Ceux-ci sont en effet souvent amenés à ouvrir les pièces jointes de contacts inconnus dans le cadre de leur mission de gestion des recrutements.<br></p><h2>Un premier fichier PDF inoffensif pour endormir la méfiance <br></h2><p>Le fournisseur de sécurité Check Point a ainsi détecté une campagne ciblée en Allemagne. Les pirates envoient des emails se présentant <a href="http://www.zdnet.com/article/this-ransomware-targets-hr-departments-with-fake-job-applications/" target="_blank">comme des candidatures à des postes</a> et embarquant des pièces jointes.<br></p><p>Mais les deux documents ne constituent pas un risque. Le premier vise au contraire à endormir la méfiance du destinataire. Il s\'agit en effet d\'une lettre de candidature au format PDF. Et ici aucun programme malveillant.<br></p><p>Il en va tout autrement du second fichier, un document Excel censé être un formulaire de candidature, mais en vérité le ransomware GoldenEye. L\'exécution n\'est pas automatique toutefois et la cible peut encore réagir et refuser d\'autoriser les Macros.<br></p><p>Dans le cas contraire, GoldenEye exécutera un programme et commencera à chiffrer les fichiers des utilisateurs. Pour en récupérer l\'accès, ceux-ci devront s\'acquitter d\'une rançon en bitcoin : 1,3 bitcoin. <br></p><p>C\'est peu ? Un bitcoin avoisine aujourd\'hui les 1000 dollars. Pour se prémunir contre une telle menace, il est recommandé de bloquer les Macros dans les documents Microsoft Office et de se méfier des emails génériques. <br></p><p>Selon une étude d\'IBM Security parue fin 2016, les entreprises victimes de ransomware auraient majoritairement tendance, à près de <a href="http://www.zdnet.fr/actualites/ransomware-payer-ou-ne-pas-payer-une-large-majorite-d-entreprises-a-choisi-39846026.htm">70%, à payer la rançon pour récupérer leurs données</a>. Le préjudice financier dépasserait les 10.000 dollars pour 50% de ces sociétés.<span></span>
</p>



</div>',
            new \DateTime()
        );
        $news->setPublished(true);
        $news->setCreatedAt(new \DateTime());
        $news->setModifiedAt(new \DateTime());
        $manager->persist($news);
        $manager->flush();

        $news2 = new News(
            'Google : après Youtube, la pub passe au HTML5',
            '<div class="storyBody">
				
		

<p>Google poursuit l\'adoption du standard HTML5. Il y a quelques semaines, le géant  <a href="http://youtube-eng.blogspot.jp/2015/01/youtube-now-defaults-to-html5_27.html" target="_blank">opérait une bascule</a>
 historique en annonçant que cette techno devenait le choix par défaut 
pour la lecture des contenus dans Chrome, Internet Explorer 11, Safari 8
 et les betas de Firefox. YouTube estime désormais qu\'HTML5 est suffisamment performant et stable pour devenir la norme.</p><p><img src="http://www.zdnet.fr/i/edit/ne/2014/10/HTML5-logo-140x105.jpg" alt="" align="left" hspace="5" vspace="5">Et cette norme s\'applique également à la publicité. Google a ainsi fourni aux annonceurs de nouveaux outils via Google Display Network, AdMob Network et DoubleClick. Un de ces outils : <a title="" target="_blank" href="https://developers.google.com/swiffy">Swiffy</a><a title="" target="_blank" href="https://plus.google.com/+GoogleAds/posts/BWVyFHGUi9n"></a> implémenté dans AdWords permet de convertir automatiquement des publicités Flash en HTML5. </p><p>"Les campagnes Flash éligibles, présentes ou à venir, seront maintenant converties automatiquement vers HTML5 lorsqu\'elles sont envoyées au travers d\'AdWords, AdWords Editor et de nombreux outils tiers", explique Google.</p><p>Il s\'agit ici d\'aider les annonceurs à mieux pénétrer les navigateurs pour smartphones qui pour certains sont allergiques à la technologie Flash.<br><br><br><br><br>&nbsp;<br></p>


</div>',
            new \DateTime()
        );
        $news2->setPublished(false);
        $news2->setCreatedAt(new \DateTime());
        $news2->setModifiedAt(new \DateTime());
        $manager->persist($news2);
        $manager->flush();
    }

    public function setContainer(ContainerInterface $container = null)
    {
        // TODO: Implement setContainer() method.
    }

    public function getOrder()
    {
        return 19;
    }
}