<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Apply;
use AppBundle\Entity\Cv;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadApplyData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    public function load(ObjectManager $manager)
    {
        
        $apply1 = new Apply("Luke", "Cage", "luke@gmail.com", new \DateTime(), $this->getReference('offer1'));
        $this->addReference("apply1", $apply1);
        $apply1->setCv('cv1.pdf');
        $apply1->setAddress("71 rue du progrès");
        $apply1->setZipCode("71100");
        $apply1->setPhone("0612715684");
        $apply1->setName("Candidature au poste de chargée de projet");
        $apply1->setCity("Lyon");
        $apply1->setState(1);

        $manager->persist($apply1);      
        
        $apply2 = new Apply("Delphine", "Cormier", "delphine@gmail.com", new \DateTime(), $this->getReference('offer2'));
        $this->addReference("apply2", $apply2);
        $apply2->setCv('cv2.pdf');
        $apply2->setZipCode("69200");
        $apply2->setAddress("4 rue du sirop d'érable");
        $apply2->setPhone("0611660248");
        $apply2->setName("Candidature au poste de développeur full stack");
        $apply2->setCity("Montreal");
        $apply2->setEmail("delphine@gmail.com");
        $apply2->setState(1);

        $this->setImageForApplies([
            $apply1,
            $apply2,
        ]);

        $manager->persist($apply2);
        $manager->flush();

    }


    private function setImageForApplies($as) {
        foreach($as as $a) {
            $this->setImageForApply($a);
        }
    }
    private function setImageForApply(Apply $a) {
        $image = $this->getImageForApply($a);
        if($image) {
            $this->writeImage($image, $a);
        }
    }
    private function getImageForApply(Apply $a) {
        $path = getcwd()."/src/AppBundle/DataFixtures/Resources/";
        $filename = 'cv test';
        $imageFound = file_exists($path.$filename);
        if ($imageFound && $filename != null){
            return $path.$filename;
        }
        return null;
    }
    private function writeImage($image, Apply $a) {
        $path = $this->getPathForApply($a);
        if (!is_dir($path)){
            if (!mkdir($path, 0777, true)) {
                die('Echec lors de la création des répertoires...');
            }else{
                if(file_exists($path.'/'.$a->getCv()))
                    unlink($path.'/'.$a->getCv());
                file_put_contents($path."/".$a->getCv(), file_get_contents($image));
            }
        }
    }
    private function getPathForApply(Apply $a) {
        $path = getcwd().'/web/uploads/expense_accounts';
        return $path;
    }



    public function setContainer(ContainerInterface $container = null)
    {
        // TODO: Implement setContainer() method.
    }

    public function getOrder()
    {
        return 14;
    }
}
