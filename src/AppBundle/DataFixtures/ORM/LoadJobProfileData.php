<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\JobProfile;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadJobProfileData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    public function load(ObjectManager $manager)
    {
        $jobProfile1 = new JobProfile($this->getReference('customer'), 'Developpeur web', 'CDI');
        $jobProfile1->setIsPublished(true);
        $jobProfile1->setInformation("Voici une information !");
        $jobProfile1->setDescription("Putin de description ma gueule");
        $jobProfile1->setExpertise("Maitrise de jedi");
        $jobProfile1->setLabel("Ben c'est un label");
        $jobProfile1->setSalary(5000);
        $jobProfile1->setCreatedAt(new \DateTime());
        $jobProfile1->setModifiedAt(new \DateTime());
        $manager->persist($jobProfile1);
        $manager->flush();
        $this->addReference("jobProfile1", $jobProfile1);     
        
        $jobProfile2 = new JobProfile($this->getReference('customer1'), 'Developpeur full stack', 'CDD');
        $jobProfile2->setIsPublished(true);
        $jobProfile2->setInformation("Recherche CDD Web Dev !");
        $jobProfile2->setDescription("Nous recherchons un dev full stack capable de se gérer....");
        $jobProfile2->setExpertise("Notions de NodeJs, maitrise absolue de git");
        $jobProfile2->setLabel("fullStack");
        $jobProfile2->setSalary(4000);
        $jobProfile2->setCreatedAt(new \DateTime());
        $jobProfile2->setModifiedAt(new \DateTime());
        $manager->persist($jobProfile2);
        $manager->flush();
        $this->addReference("jobProfile2", $jobProfile2);

        $jobProfile3 = new JobProfile($this->getReference('customer1'), 'Developpeur Node.js', 'CDI');
        $jobProfile3->setIsPublished(true);
        $jobProfile3->setInformation("Recherche Saisonnier Recolteur de kiwi !");
        $jobProfile3->setDescription("Nous recherchons une personne motivée pour la récolte annuelle de kiwi");
        $jobProfile3->setExpertise("Maîtriser la dégustation de kiwi");
        $jobProfile3->setLabel("motivation");
        $jobProfile3->setSalary(2000);
        $jobProfile3->setCreatedAt(new \DateTime());
        $jobProfile3->setModifiedAt(new \DateTime());
        $manager->persist($jobProfile3);
        $manager->flush();
        $this->addReference("jobProfile3", $jobProfile3);

    }


    public function setContainer(ContainerInterface $container = null)
    {
        // TODO: Implement setContainer() method.
    }

    public function getOrder()
    {
        return 12;
    }
}
