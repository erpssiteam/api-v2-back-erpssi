<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Cv;
use AppBundle\Entity\Employee;
use AppBundle\Entity\Holiday;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\Validator\Constraints\DateTime;

class LoadHolidayData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    public function load(ObjectManager $manager)
    {
        $this->createHoliday($this->getReference('employee1'), 2, 'holiday1', 3, $manager);
        $this->createHoliday($this->getReference('employee2'), 3, 'holiday2', 2, $manager);
        $this->createHoliday($this->getReference('employee3'), 1, 'holiday3', 1, $manager);
        $this->createHoliday($this->getReference('employee3'), 2, 'holiday4', 1, $manager);
    }

    public function setContainer(ContainerInterface $container = null)
    {
        // TODO: Implement setContainer() method.
    }

    /**
     * @param Employee $employee
     * @param $state
     * @param $ref
     * @param $reason
     * @param ObjectManager $manager
     */
    private function createHoliday(Employee $employee, $state, $ref, $reason, ObjectManager $manager) {
        $now = new \DateTime();
        $holiday = new Holiday($employee);
        $holiday->setState($state);
        $holiday->setEndedAt($now->add(new \DateInterval('P1M')));;
        $holiday->setStartedAt($now->sub(new \DateInterval('P5D')));
        $holiday->setReason($reason);
        if($state == Holiday::STATE_VALIDATED) $holiday->setValidatedAt($now);
        $manager->persist($holiday);
        $manager->flush();
        $this->addReference($ref, $holiday);

    }

    public function getOrder()
    {
        return 11;
    }
}
