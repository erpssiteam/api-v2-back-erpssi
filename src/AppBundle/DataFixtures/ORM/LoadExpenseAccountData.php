<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\ExpenseAccount;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadExpenseAccountData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    public function load(ObjectManager $manager)
    {

        $ea = new ExpenseAccount($this->getReference('employee'), 'Voyage', 679.90, 20.0);
        $ea->setDescription('Frais d\'avion depuis Dubai à Paris');
        $ea->setImage('image_expense_account.jpg');
        $ea->setState(2);
        $ea->setDateExpenseAccount(new \DateTime('2017-01-01'));
        $ea->setValidatedAt(new \DateTime());
        $manager->persist($ea);
        $manager->flush();

        $ea2 = new ExpenseAccount($this->getReference('employee2'), 'Restaurant', 97.45, 20.0);
        $ea2->setDescription('Restaurant avec client Georges V.');
        $ea2->setImage('image_expense_account.jpg');
        $ea2->setState(3);
        $ea2->setDateExpenseAccount(new \DateTime('2017-03-12'));
        $manager->persist($ea2);
        $manager->flush();

        $ea3 = new ExpenseAccount($this->getReference('employee3'), 'Train', 60, 20.0);
        $ea3->setDescription('Frais de Lyon à Paris');
        $ea3->setImage('image_expense_account.jpg');
        $ea3->setState(2);
        $ea3->setDateExpenseAccount(new \DateTime('2017-03-23'));
        $ea3->setValidatedAt(new \DateTime());
        $manager->persist($ea3);
        $manager->flush();

        $this->setImageForIdentities([
            $ea,
            $ea2,
//            $ea3
        ]);
    }


    private function setImageForIdentities($eas) {
        foreach($eas as $ea) {
            $this->setImageForExpenseAccount($ea);
        }
    }
    private function setImageForExpenseAccount(ExpenseAccount $ea) {
        $image = $this->getImageForExpenseAccount($ea);
        if($image) {
            $this->writeImage($image, $ea);
        }
    }
    private function getImageForExpenseAccount(ExpenseAccount $ea) {
        $path = getcwd()."/src/AppBundle/DataFixtures/Resources/";
        $filename = 'test.jpg';
        $imageFound = file_exists($path.$filename);
        if ($imageFound && $filename != null){
            return $path.$filename;
        }
        return null;
    }
    private function writeImage($image, ExpenseAccount $ea) {
        $path = $this->getPathForExpenseAccount($ea);
        if (!is_dir($path)){
            if (!mkdir($path, 0777, true)) {
                die('Echec lors de la création des répertoires...');
            }else{
                if(file_exists($path.'/'.$ea->getImage()))
                    unlink($path.'/'.$ea->getImage());
                file_put_contents($path."/".$ea->getImage(), file_get_contents($image));
            }
        }
    }
    private function getPathForExpenseAccount(ExpenseAccount $ea) {
        $path = getcwd().'/web/uploads/expense_accounts';
        return $path;
    }


    public function setContainer(ContainerInterface $container = null)
    {
        // TODO: Implement setContainer() method.
    }

    public function getOrder()
    {
        return 18;
    }
}
