<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Apply;
use AppBundle\Entity\Cv;
use AppBundle\Entity\Offer;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadOfferData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    public function load(ObjectManager $manager)
    {
        $date = new \DateTime();

        $offer1 = new Offer($this->getReference("jobProfile1"));
        $offer1->setValid(true);
        $offer1->setStartAt($date->sub(new \DateInterval('P5D')));
        $offer1->setEndAt($date->add(new \DateInterval('P8D')));
        $offer1->setReference("AZERT456");
        $offer1->setName("Recherche Développeur web alternance H/F");
        $offer1->setConsulted();
        $manager->persist($offer1);
        $manager->flush();
        $this->addReference("offer1", $offer1);


        $offer2 = new Offer($this->getReference("jobProfile2"));
        $offer2->setValid(true);
        $offer2->setStartAt($date->sub(new \DateInterval('P2D')));
        $offer2->setEndAt($date->add(new \DateInterval('P6D')));
        $offer2->setReference("ARTI206");
        $offer2->setName("Recherche Jardinier H/F");
        $offer2->setConsulted();
        $manager->persist($offer2);
        $manager->flush();
        $this->addReference("offer2", $offer2);

    }


    public function setContainer(ContainerInterface $container = null)
    {
        // TODO: Implement setContainer() method.
    }

    public function getOrder()
    {
        return 13;
    }
}
