<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Cra;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadCraData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    public function load(ObjectManager $manager)
    {
        $cra = $this->createCra($this->getReference('employee3'), $this->getReference('customer'), "Mission 2", "Entretiens des serveurs de la SI de l'entreprise Total", 2, 200, Cra::CRA_TYPE_JOB_FREELANCE, 'cra1');
        $cra1 = $this->createCra($this->getReference('employee1'), $this->getReference('customer1'), "Mission 5", "Réparation robinet pour la mairie du 8e", 1, 60, Cra::CRA_TYPE_JOB_INTERIM, 'cra2');
        $cra2 = $this->createCra($this->getReference('employee1'), $this->getReference('customer1'), "Mission 15", "Mise en place d'une clim pour l'entreprise Auchan", 05, 2, Cra::CRA_TYPE_JOB_FREELANCE, 'cra3');
        $cra3 = $this->createCra($this->getReference('employee2'), $this->getReference('customer1'), "Mission 16", "Nettoyage des ventilateurs pour l'entreprise Auchan", 05, 2, Cra::CRA_TYPE_JOB_FREELANCE, 'cra4');

        $manager->persist($cra);
        $manager->persist($cra1);
        $manager->persist($cra2);
        $manager->persist($cra3);
        $manager->flush();

    }


    public function setContainer(ContainerInterface $container = null)
    {
        // TODO: Implement setContainer() method.
    }

    private function createCra($employee, $customer, $title, $description, $timeJourney, $distance, $typeJob, $ref) {
        $cra = new Cra($employee, $customer, $title);
        $cra->setStartedAt(new \DateTime());
        $cra->setEndedAt(new \DateTime());
        $cra->setDescription($description);
        $cra->setTimeJourney($timeJourney);
        $cra->setDistance($distance);
        $cra->setTypeJob($typeJob);
        $this->addReference($ref, $cra);

        return $cra;

    }

    public function getOrder()
    {
        return 14;
    }
}
