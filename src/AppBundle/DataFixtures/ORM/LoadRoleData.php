<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Apply;
use AppBundle\Entity\Cra;
use AppBundle\Entity\Cv;
use AppBundle\Entity\JobProfile;
use AppBundle\Entity\Role;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadRoleData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    public function load(ObjectManager $manager)
    {

        $roleAdmin = new Role();
        $roleAdmin->setLabel("ROLE_ADMIN");
        $roleAdmin->setCreatedAt(new \DateTime());
        $roleAdmin->setModifiedAt(new \DateTime());
        $manager->persist($roleAdmin);
        $manager->flush();
        $this->setReference('roleAdmin', $roleAdmin);

        $roleSupervisor = new Role();
        $roleSupervisor->setLabel("ROLE_NEWS");
        $roleSupervisor->setCreatedAt(new \DateTime());
        $roleSupervisor->setModifiedAt(new \DateTime());
        $manager->persist($roleSupervisor);
        $manager->flush();
        $this->setReference('roleNews', $roleSupervisor);

        $roleManager = new Role();
        $roleManager->setLabel("ROLE_MANAGER");
        $roleManager->setCreatedAt(new \DateTime());
        $roleManager->setModifiedAt(new \DateTime());
        $manager->persist($roleManager);
        $manager->flush();
        $this->setReference('roleManager', $roleManager);

        $roleRh = new Role();
        $roleRh->setLabel("ROLE_RH");
        $roleRh->setCreatedAt(new \DateTime());
        $roleRh->setModifiedAt(new \DateTime());
        $manager->persist($roleRh);
        $manager->flush();
        $this->setReference('roleRh', $roleRh);

        $roleCollaborator = new Role();
        $roleCollaborator->setLabel("ROLE_COLLABORATOR");
        $roleCollaborator->setCreatedAt(new \DateTime());
        $roleCollaborator->setModifiedAt(new \DateTime());
        $manager->persist($roleCollaborator);
        $manager->flush();
        $this->setReference('roleCollaborator', $roleCollaborator);

        $roleUser = new Role();
        $roleUser->setLabel("ROLE_USER");
        $roleUser->setCreatedAt(new \DateTime());
        $roleUser->setModifiedAt(new \DateTime());
        $manager->persist($roleUser);
        $manager->flush();
        $this->setReference('roleUser', $roleUser);

    }


    public function setContainer(ContainerInterface $container = null)
    {
        // TODO: Implement setContainer() method.
    }

    public function getOrder()
    {
        return 1;
    }
}
