<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Skill;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadSkillData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    public function load(ObjectManager $manager)
    {

        $php = new Skill("php");
        $php->setDescription("PHP est un langage très puissant.");
        $php->setCreatedAt(new \DateTime());
        $php->setModifiedAt(new \DateTime());
        $manager->persist($php);
        $manager->flush();
        $this->setReference('php', $php);

        $javascript = new Skill("javascript");
        $javascript->setDescription("javascript est un langage très puissant.");
        $javascript->setCreatedAt(new \DateTime());
        $javascript->setModifiedAt(new \DateTime());
        $manager->persist($javascript);
        $manager->flush();
        $this->setReference('javascript', $javascript);

        $excel = new Skill("excel");
        $excel->setDescription("excel est un outil très puissant.");
        $excel->setCreatedAt(new \DateTime());
        $excel->setModifiedAt(new \DateTime());
        $manager->persist($excel);
        $manager->flush();
        $this->setReference('excel', $excel);

        $word = new Skill("word");
        $word->setDescription("word est un outil très puissant.");
        $word->setCreatedAt(new \DateTime());
        $word->setModifiedAt(new \DateTime());
        $manager->persist($word);
        $manager->flush();
        $this->setReference('word', $word);

        $seo = new Skill("seo");
        $seo->setDescription("seo est un outil très puissant.");
        $seo->setCreatedAt(new \DateTime());
        $seo->setModifiedAt(new \DateTime());
        $manager->persist($seo);
        $manager->flush();
        $this->setReference('seo', $seo);

        $ga = new Skill("google analytics");
        $ga->setDescription("google analytics est un outil très puissant.");
        $ga->setCreatedAt(new \DateTime());
        $ga->setModifiedAt(new \DateTime());
        $manager->persist($ga);
        $manager->flush();
        $this->setReference('ga', $ga);

    }


    public function setContainer(ContainerInterface $container = null)
    {
        // TODO: Implement setContainer() method.
    }

    public function getOrder()
    {
        return 16;
    }
}
