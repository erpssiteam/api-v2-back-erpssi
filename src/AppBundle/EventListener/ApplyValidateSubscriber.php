<?php

namespace AppBundle\EventListener;


use AppBundle\Entity\Apply;
use AppBundle\Repository\ApplyRepository;
use AppBundle\Repository\EmployeeRepository;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

class ApplyValidateSubscriber implements EventSubscriber
{
    /**
     * @var UserPasswordEncoder
     */
    private $encoder;

    /**
     * ApplyValidateListener constructor.
     * @param UserPasswordEncoder $encoder
     */
    public function __construct(UserPasswordEncoder $encoder)
    {
        $this->encoder = $encoder;
    }

    public function getSubscribedEvents()
    {
        return [
            'postUpdate'
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     * @internal param LifecycleEventArgs $event
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $apply = $args->getObject();
        $em = $args->getObjectManager();
        if ($apply instanceof Apply) {
            if ($apply->getState() === Apply::APPLY_VALIDATED_STATE && !$this->isEmployeeAlreadyExist($apply->getEmail(), $em->getRepository('AppBundle:Employee'))) {
                $user = $em->getRepository("AppBundle:User")->createUserFromApply($apply, $this->encoder);
                $em->getRepository("AppBundle:Employee")->createEmployeeFromApply($apply, $user);
            }

        }
    }

    /**
     * @param $email
     * @param EmployeeRepository $employeeRepository
     * @return bool
     */
    private function isEmployeeAlreadyExist($email, EmployeeRepository $employeeRepository) {
       $employee = $employeeRepository->findBy(['email' => $email]);
        if($employee) return true;
        return false;
    }
}
