<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Employee;
use AppBundle\Normalizer\EmployeeNormalizer;
use AppBundle\Repository\EmployeeRepository;
use AppBundle\Repository\UserRepository;
use Doctrine\Common\EventSubscriber;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Serializer;

class JWTResponseListener implements EventSubscriber
{
    /**
     * @var EmployeeRepository
     */
    private $employeeRepository;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * JWTResponseListener constructor.
     * @param EmployeeRepository $employeeRepository
     * @param EmployeeNormalizer $employeeNormalizer
     */
    public function __construct(EmployeeRepository $employeeRepository, Serializer $serializer)
    {
        $this->employeeRepository = $employeeRepository;
        $this->serializer = $serializer;
    }

    public function getSubscribedEvents()
    {
        // TODO: Implement getSubscribedEvents() method.
    }

    /**
     * @param AuthenticationSuccessEvent $event
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $data = $event->getData();
        $user = $event->getUser();
        /** @var Employee $employee */
        $employee = $this->employeeRepository->findOneBy(['user' => $user]);

        if (!$user instanceof UserInterface) {
            return;
        }

        $data['data'] = $this->serializer->normalize($employee);

        $event->setData($data);
    }
}
