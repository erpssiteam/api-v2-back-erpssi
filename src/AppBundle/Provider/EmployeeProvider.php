<?php
/**
 * Created by PhpStorm.
 * User: rislou
 * Date: 11/06/17
 * Time: 20:08
 */

namespace AppBundle\Provider;


use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use AppBundle\Repository\EmployeeRepository;

class EmployeeProvider
{

    /**
     * @param User $user
     * @param EmployeeRepository $employeeRepository
     * @return array|null
     */
    public function getAllDecision(User $user, EmployeeRepository $employeeRepository)
    {
        return $this->voteFactory($user->getRoles()[0], $user, $employeeRepository);
    }

    private function voteFactory($role, User $user, EmployeeRepository $employeeRepository) {
        switch ($role) {
            case Role::ROLE_MANAGER:
                return $this->getAllEmployeeManaged($user, $employeeRepository);
            case Role::ROLE_ADMIN:
                return $this->getAllEmployee($employeeRepository);
            case Role::ROLE_RH:
                return $this->getAllEmployee($employeeRepository);
        }
        return null;
    }

    /**
     * @param EmployeeRepository $employeeRepository
     * @return array
     */
    private function getAllEmployee(EmployeeRepository $employeeRepository) {
        return $employeeRepository->findBy(['deprecated' => 0]);
    }

    /**
     * @param User $user
     * @param EmployeeRepository $employeeRepository
     * @return array|null
     */
    private function getAllEmployeeManaged(User $user, EmployeeRepository $employeeRepository) {
        $manager = $employeeRepository->findOneBy(['user' => $user]);
        if($manager) {
            return $employeeRepository->findBy(['managedBy' => $manager]);
        }
        return null;
    }
}