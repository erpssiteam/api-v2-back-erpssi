<?php
/**
 * Created by PhpStorm.
 * User: rislou
 * Date: 11/06/17
 * Time: 20:08
 */

namespace AppBundle\Provider;


use AppBundle\Entity\Holiday;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use AppBundle\Repository\EmployeeRepository;
use Doctrine\ORM\ORMException;

class EntityProvider
{

    /**
     * @var EmployeeRepository
     */
    private $employeeRepository;

    /**
     * EntityProvider constructor.
     * @param EmployeeRepository $employeeRepository
     */
    public function __construct(EmployeeRepository $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }


    /**
     * @param User $user
     * @param $repository
     * @param $options
     * @return array|null
     */
    public function getAllDecision(User $user, $repository, $options = [])
    {
        return $this->voteFactory($user->getRoles()[0], $user, $repository, $options);
    }

    private function voteFactory($role, User $user, $repository, $options) {
        switch ($role) {
            case Role::ROLE_MANAGER:
                return $this->getAllManaged($user, $repository, $options);
            case Role::ROLE_ADMIN:
                return $this->getAll($repository, $options);
            case Role::ROLE_RH:
                return $this->getAll($repository, $options);
            case Role::ROLE_COLLABORATOR:
                return $this->getAllBy($user, $repository, $options);
        }
        return null;
    }

    /**
     * @param EmployeeRepository $repository
     * @param $options
     * @return array
     */
    private function getAll($repository, $options = null) {
        if($options != null) {
            return $repository->findBy($options);
        }

        return $repository->findAll();
    }

    /**
     * @param User $user
     * @param EmployeeRepository $repository
     * @return array
     */
    private function getAllBy(User $user, $repository, $options) {
        $employee = $this->employeeRepository->findOneBy(['user' => $user]);
        return $repository->findBy(array_merge(['employee' => $employee], $options));
    }

    /**
     * @param User $user
     * @param EmployeeRepository $repository
     * @param $options
     * @return array|null
     */
    private function getAllManaged(User $user, $repository, $options) {
        $manager = $this->employeeRepository->findOneBy(['user' => $user]);
        if($manager) {
            try {
                return $repository->findBy(array_merge(['managedBy' => $manager], $options));
            } catch (ORMException $e) {
                $allObject = $repository->findAll();
                $objArray = [];
                foreach ($allObject as $obj) {
                    if($obj->getEmployee()->getManagedBy() != null && $obj->getEmployee()->getManagedBy()->getId() === $manager->getId()) {
                        $objArray [] = $obj;
                    }
                }
                return $objArray;
            }
        }
        return null;
    }
}