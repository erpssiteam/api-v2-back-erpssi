<?php
/**
 * Created by PhpStorm.
 * User: rislou
 * Date: 22/03/16
 * Time: 20:09
 */

namespace AppBundle\Repository;

use AppBundle\Entity\EmployeeSkill;
use Doctrine\ORM\EntityRepository;

class EmployeeSkillRepository extends EntityRepository
{

    /**
     * @param $employeeSkill
     * @param $flush
     */
    public function persist($employeeSkill, $flush)
    {
        $this->getEntityManager()->persist($employeeSkill);
        if($flush) $this->flush();
        return $employeeSkill;
    }

    /**
     * @param EmployeeSkill|null $employeeSkill
     * @return EmployeeSkill|null
     */
    public function flush(EmployeeSkill $employeeSkill = null)
    {
        $this->getEntityManager()->flush($employeeSkill);
        return $employeeSkill;
    }

    /**
     * @param EmployeeSkill $employeeSkill
     */
    public function delete(EmployeeSkill $employeeSkill) {
        $this->_em->remove($employeeSkill);
        $this->flush();
    }

}
