<?php
/**
 * Created by PhpStorm.
 * User: whiskey
 * Date: 24/03/17
 * Time: 14:37
 */

namespace AppBundle\Repository;


use AppBundle\Entity\News;
use Doctrine\ORM\EntityRepository;

class NewsRepository extends EntityRepository
{
    /**
     * @param $news
     * @param $flush
     */
    public function persist($news, $flush)
    {
        $this->getEntityManager()->persist($news);
        if($flush) $this->flush();
        return $news;
    }

    /**
     * @param News|null $news
     * @return News|null
     */
    public function flush(News $news = null)
    {
        $this->getEntityManager()->flush($news);
        return $news;
    }

    /**
     * @param News $news
     */
    public function delete(News $news)
    {
        $this->getEntityManager()->remove($news);
        $this->flush();
    }

    /**
     * @param News $news
     */
    public function publish(News $news)
    {
        $news->setPublished(NEWS::NEWS_PUBLISHED_STATE);
        $this->flush();
    }

    /**
     * @param News $news
     */
    public function unpublish(News $news)
    {
        $news->setPublished(NEWS::NEWS_UNPUBLISHED_STATE);
        $this->flush();
    }
}