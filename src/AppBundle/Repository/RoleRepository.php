<?php
/**
 * Created by PhpStorm.
 * User: rislou
 * Date: 22/03/16
 * Time: 20:09
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Role;
use Doctrine\ORM\EntityRepository;

class RoleRepository extends EntityRepository
{
    /**
     * @param Role $role
     */
    public function delete(Role $role) {
        $this->_em->remove($role);
    }
}
