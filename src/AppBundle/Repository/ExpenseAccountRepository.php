<?php
/**
 * Created by PhpStorm.
 * User: rislou
 * Date: 22/03/16
 * Time: 20:09
 */

namespace AppBundle\Repository;

use AppBundle\Entity\ExpenseAccount;
use Doctrine\ORM\EntityRepository;

class ExpenseAccountRepository extends EntityRepository
{

    /**
     * @param $expenseAccount
     * @param $flush
     */
    public function persist($expenseAccount, $flush)
    {
        $this->getEntityManager()->persist($expenseAccount);
        if($flush) $this->flush();
        return $expenseAccount;
    }

    /**
     * @param ExpenseAccount|null $expenseAccount
     * @return ExpenseAccount|null
     */
    public function flush(ExpenseAccount $expenseAccount = null)
    {
        $this->getEntityManager()->flush($expenseAccount);
        return $expenseAccount;
    }

    /**
     * @param ExpenseAccount $expenseAccount
     */
    public function delete(ExpenseAccount $expenseAccount) {
        $this->getEntityManager()->remove($expenseAccount);
        $this->flush();
    }

    /**
     * @param ExpenseAccount $expenseAccount
     */
    public function validate(ExpenseAccount $expenseAccount)
    {
        $expenseAccount->setState(ExpenseAccount::EXPENSE_ACCOUNT_VALIDATED_STATE);
        $expenseAccount->setValidatedAt(new \DateTime());
        $this->flush();
    }

    /**
     * @param ExpenseAccount $expenseAccount
     */
    public function denied(ExpenseAccount $expenseAccount)
    {
        $expenseAccount->setState(ExpenseAccount::EXPENSE_ACCOUNT_DENIED_STATE);
        $this->flush();
    }

    /**
     * @param ExpenseAccount $expenseAccount
     * @param $imageFileName
     * @return ExpenseAccount
     */
    public function updateImage(ExpenseAccount $expenseAccount, $imageFileName)
    {
        $expenseAccount->setImage($imageFileName);
        $this->flush($expenseAccount);
        return $expenseAccount;
    }

}
