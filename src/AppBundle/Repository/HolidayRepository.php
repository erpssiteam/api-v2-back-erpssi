<?php
/**
 * Created by PhpStorm.
 * User: rislou
 * Date: 22/03/16
 * Time: 20:09
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Holiday;
use Doctrine\ORM\EntityRepository;

class HolidayRepository extends EntityRepository
{

    /**
     * @param $holidayEntity
     * @param $flush
     * @return Holiday
     */
    public function persist(Holiday $holidayEntity, $flush)
    {
        $this->getEntityManager()->persist($holidayEntity);
        if($flush) $this->flush();
        return $holidayEntity;
    }

    public function flush(Holiday $holiday = null) {
        $this->getEntityManager()->flush();
        return $holiday;
    }

    /**
     * @param Holiday $holiday
     */
    public function delete(Holiday $holiday)
    {
        $this->getEntityManager()->remove($holiday);
        $this->flush();
    }

    /**
     * @param Holiday $holiday
     */
    public function validate(Holiday $holiday)
    {
        $holiday->setState(Holiday::STATE_VALIDATED);
        $holiday->setValidatedAt(new \DateTime());
        $this->flush();
    }

    /**
     * @param Holiday $holiday
     */
    public function denied(Holiday $holiday)
    {
        $holiday->setState(Holiday::STATE_DENIED);
        $this->flush();
    }
}