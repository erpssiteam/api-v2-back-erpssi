<?php
/**
 * Created by PhpStorm.
 * User: rislou
 * Date: 22/03/16
 * Time: 20:09
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Cra;
use AppBundle\Entity\Employee;
use Doctrine\ORM\EntityRepository;

class CraRepository extends EntityRepository
{

    /**
     * @param Cra $craEntity
     * @param $flush
     * @return Cra
     */
    public function persist(Cra $craEntity, $flush)
    {
        $this->getEntityManager()->persist($craEntity);
        if ($flush) $this->flush();
        return $craEntity;
    }

    /**
     * @param Cra $cra
     */
    public function delete(Cra $cra)
    {
        $this->getEntityManager()->remove($cra);
        $this->flush();
    }

    /**
     * @param Cra $cra
     * @return Cra
     */
    public function flush(Cra $cra = null)
    {
        $this->getEntityManager()->flush($cra);
        return $cra;
    }

    /**
     * @param Cra $cra
     */
    public function validate(Cra $cra)
    {
        $cra->setState(Cra::STATE_VALIDATED);
        $cra->setValidatedAt(new \DateTime());
        $this->flush();
    }

    /**
     * @param Cra $cra
     */
    public function denied(Cra $cra)
    {
        $cra->setState(Cra::STATE_DENIED);
        $this->flush();
    }

}


