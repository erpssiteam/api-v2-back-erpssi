<?php
/**
 * Created by PhpStorm.
 * User: rislou
 * Date: 22/03/16
 * Time: 20:09
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Apply;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Tests\Encoder\PasswordEncoder;

class UserRepository extends EntityRepository
{

    const ROLE_ADMIN = 1;
    const ROLE_SUPERVISOR = 2;
    const ROLE_MANAGER = 3;
    const ROLE_RH = 4;
    const ROLE_COLLABORATOR = 5;
    const ROLE_REDACTOR = 6;

    public $rolesMap = [
        self::ROLE_ADMIN => ['ROLE_ADMIN', 'ROLE_SUPERVISOR', 'ROLE_RH', 'ROLE_MANAGER', 'ROLE_COLLABORATOR', 'ROLE_REDACTOR'],
        self::ROLE_SUPERVISOR => ['ROLE_SUPERVISOR', 'ROLE_MANAGER', 'ROLE_RH', 'ROLE_COLLABORATOR', 'ROLE_REDACTOR'],
        self::ROLE_MANAGER => ['ROLE_MANAGER', 'ROLE_COLLABORATOR'],
        self::ROLE_RH => ['ROLE_RH', 'ROLE_COLLABORATOR'],
        self::ROLE_COLLABORATOR => ['ROLE_COLLABORATOR'],
        self::ROLE_REDACTOR => ['ROLE_REDACTOR']
    ];

    /**
     * @return array
     */
    public function findAll() {
        return $this->createQueryBuilder('user')
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * @param User $user
     * @param $data
     * @return User
     */
    public function updateInformation(User $user, $data) {

        $this->_em->flush($this->setAllUserInformation($user, $data));
        return $user;

    }

    /**
     * @param User $user
     * @param $data
     * @return User
     */
    public function create(User $user, $data) {

        $this->setAllUserInformation($user, $data);
        $this->_em->flush();

        return $user;

    }

    /**
     * @param $user
     * @param $flush
     */
    public function persist($user, $flush)
    {
        $this->getEntityManager()->persist($user);
        if($flush) $this->flush();
        return $user;
    }

    /**
     * @param User|null $employee
     */
    public function flush(User $user = null)
    {
        $this->getEntityManager()->flush($user);
    }

    /**
     * @param User $user
     */
    public function delete(User $user) {
        $this->_em->remove($user);
    }

    /**
     * @param User $user
     * @param $data
     * @return User
     */
    private function setAllUserInformation(User $user, $data) {

        if (isset($data['username'])) $user->setUsername($data['username']);
        if (isset($data['password'])) $user->setLastname($data['password']);
        if (isset($data['role'])) $user->setRoles($this->rolesMap[$data['role']]);

        return $user;

    }

    /**
     * @param Apply $apply
     * @param UserPasswordEncoder $encoder
     * @return User
     * @internal param $password
     */
    public function createUserFromApply(Apply $apply, UserPasswordEncoder $encoder) {
        $roleCollaborator = $this->getRoleRepository()->findOneBy(['label' => 'ROLE_COLLABORATOR']);
        $roleUser = $this->getRoleRepository()->findOneBy(['label' => 'ROLE_USER']);
        /** @var Role $roleCollaborator */
        $user = new User($apply->getFirstname()."_" .$apply->getLastname(), "fakePassword", $roleCollaborator);
        $user->setPassword($encoder->encodePassword($user, "erpssi".$apply->getId()));
        /** @var Role $roleUser */
        $user->addRole($roleUser);
        $this->persist($user, true);
        return $user;
    }

    /**
     * @return RoleRepository|EntityRepository
     */
    private function getRoleRepository() {
        return $this->getEntityManager()->getRepository("AppBundle:Role");
    }

}
