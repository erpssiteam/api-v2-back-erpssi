<?php
/**
 * Created by PhpStorm.
 * User: rislou
 * Date: 22/03/16
 * Time: 20:09
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Skill;
use Doctrine\ORM\EntityRepository;

class SkillRepository extends EntityRepository
{

    /**
     * @param $skill
     * @param $flush
     */
    public function persist($skill, $flush)
    {
        $this->getEntityManager()->persist($skill);
        if($flush) $this->flush();
        return $skill;
    }

    /**
     * @param Skill|null $skill
     * @return Skill|null
     */
    public function flush(Skill $skill = null)
    {
        $this->getEntityManager()->flush($skill);
        return $skill;
    }

    /**
     * @param Skill $skill
     */
    public function delete(Skill $skill) {

        $skill->setDepreciated(TRUE);
        $this->getEntityManager()->flush($skill);

    }
}
