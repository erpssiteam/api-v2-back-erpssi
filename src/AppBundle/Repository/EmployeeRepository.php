<?php
/**
 * Created by PhpStorm.
 * User: rislou
 * Date: 22/03/16
 * Time: 20:09
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Apply;
use AppBundle\Entity\Employee;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class EmployeeRepository extends EntityRepository
{
    /**
     * @return UserRepository|EntityRepository
     */
    private function getUserRepository() {
        return $this->getEntityManager()->getRepository('AppBundle:User');
    }

    /**
     * @param $employee
     * @param $flush
     */
    public function persist($employee, $flush)
    {
        $this->getEntityManager()->persist($employee);
        if($flush) $this->flush();
        return $employee;
    }

    /**
     * @param Employee|null $employee
     */
    public function flush(Employee $employee = null)
    {
        $this->getEntityManager()->flush($employee);
        return $employee;
    }

    /**
     * @param Employee $employee
     */
    public function delete(Employee $employee)
    {
        $employee->setDeprecated(1);
        $userRef = $employee->getUser();
        $employee->setUser(null);
        $this->flush();
        $this->getUserRepository()->delete($userRef);
    }

    /**
     * @param Apply $apply
     * @param User $user
     * @return Employee
     */
    public function createEmployeeFromApply(Apply $apply, User $user) {
        $employee = new Employee($apply->getFirstname(), $apply->getLastname(), $apply->getBirthdayDate(), $apply->getEmail(), $apply->getAddress(), $apply->getZipCode(), $apply->getCity(), $user);
        $this->persist($employee, true);
        return $employee;
    }

    /**
     * @param User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findEmployeeFromUser(User $user){
        $qb = $this->createQueryBuilder("e")
            ->select("e")
            ->where("e.user = :idUser")
            ->setParameter("idUser", $user->getId());

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param Employee $employee
     * @return mixed
     */
    public function isEmployeeManaged(Employee $employee) {
        $qb = $this->createQueryBuilder("e")
            ->select("e")
            ->where("e.employee = :employee")
            ->setParameter("employee", $employee);

        return $qb->getQuery()->getOneOrNullResult();
    }

}
