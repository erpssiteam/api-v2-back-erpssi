<?php
/**
 * Created by PhpStorm.
 * User: rislou
 * Date: 22/03/16
 * Time: 20:09
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Apply;
use Doctrine\ORM\EntityRepository;

class ApplyRepository extends EntityRepository
{
    /**
     * @param $applyEntity
     * @param $flush
     * @return Apply
     */
    public function persist(Apply $applyEntity, $flush)
    {
        $this->getEntityManager()->persist($applyEntity);
        if($flush) $this->flush();
        return $applyEntity;
    }

    /**
     * @param Apply $apply
     */
    public function delete(Apply $apply)
    {
        $this->getEntityManager()->remove($apply);
        $this->flush();
    }

    /**
     * @param Apply $apply
     * @return Apply
     */
    public function flush(Apply $apply = null) {
        $this->getEntityManager()->flush($apply);
        return $apply;
    }

    /**
     * @param Apply $apply
     */
    public function validate(Apply $apply)
    {
        $apply->setState(Apply::APPLY_VALIDATED_STATE);
        $this->flush();
    }

    /**
     * @param Apply $apply
     */
    public function denied(Apply $apply){
        $apply->setState(Apply::APPLY_DENIED_STATE);
        $this->flush();
    }

    /**
     * @param Apply $apply
     * @param $fileName
     * @return Apply
     */
    public function updateCv(Apply $apply, $fileName)
    {
        $apply->setCv($fileName);
        return $this->flush($apply);
    }
}