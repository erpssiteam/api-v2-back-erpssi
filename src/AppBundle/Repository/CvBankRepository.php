<?php
/**
 * Created by PhpStorm.
 * User: rislou
 * Date: 22/03/16
 * Time: 20:09
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Cv;
use AppBundle\Entity\User;
use AppBundle\Entity\Apply;


class CvBankRepository extends EntityRepository
{
    /**
     * @param $cv
     * @return array
     */
    public function findArray($cv)
    {
        return $this->createQueryBuilder('cv')
            ->where('cv.id = :cv')
            ->setParameter('cv', $cv)
            ->getQuery()
            ->getArrayResult();
    }




    /**
     * @param User $user
     * @return array()
     */
    public function findByUser(User $user)
    {
        return $this->createQueryBuilder('apply')
            ->leftJoin('apply.id_cv', 'cv')
            ->where('apply.id_user = :user')
            ->setParameter('user',$user)
            ->getQuery()
            ->getArrayResult();
    }
    /**
     * @param Apply $apply
     * @return array()
     */
    public function findByApply(Apply $apply)
    {
        return $this->createQueryBuilder('apply')
            ->leftJoin('apply.id_cv', 'cv')
            ->where('appy.id = :apply')
            ->setParameter('apply',$apply)
            ->getQuery()
            ->getArrayResult();
    }



}
