<?php
/**
 * Created by PhpStorm.
 * User: rislou
 * Date: 22/03/16
 * Time: 20:07
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Customer;
use AppBundle\Entity\Employee;
use AppBundle\Entity\JobProfile;
use Doctrine\ORM\EntityRepository;

class JobProfileRepository extends EntityRepository
{

    /**
     * @return OfferRepository|EntityRepository
     */
    private function getOfferRepository() {
        return $this->getEntityManager()->getRepository('AppBundle:Offer');
    }
    /**
     * @param $jobProfile
     * @param $flush
     */
    public function persist($jobProfile, $flush)
    {
        $this->getEntityManager()->persist($jobProfile);
        if($flush) $this->flush();
        return $jobProfile;
    }

    /**
     * @param JobProfile|null $jobProfile
     * @return JobProfile|null
     */
    public function flush(JobProfile $jobProfile = null)
    {
        $this->getEntityManager()->flush($jobProfile);
        return $jobProfile;
    }

    /**
     * @param JobProfile $jobProfile
     */
    public function delete(JobProfile $jobProfile)
    {
        $this->getOfferRepository()->synchronize($jobProfile);
        $this->getEntityManager()->remove($jobProfile);
        $this->flush();
    }

    /**
     * @param Employee $employee
     * @return array
     */
    public function findCustomersByJobProfile(Employee $employee)
    {
        // TODO check if this method is ok
        // should return an array of customer for the employee given
        $qb = $this->createQueryBuilder("jb")
            ->where("jb.employee = :idEmployee")
            ->setParameter("idEmployee", $employee->getId());
        return $qb->getQuery()->getResult();
    }
}