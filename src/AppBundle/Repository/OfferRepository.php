<?php
/**
 * Created by PhpStorm.
 * User: rislou
 * Date: 22/03/16
 * Time: 20:09
 */

namespace AppBundle\Repository;

use AppBundle\Entity\JobProfile;
use AppBundle\Entity\Offer;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityRepository;

class OfferRepository extends EntityRepository
{
    
    /**
     * @param $offer
     * @param $flush
     */
    public function persist($offer, $flush)
    {
        $this->getEntityManager()->persist($offer);
        if($flush) $this->flush();
        return $offer;
    }

    /**
     * @param Offer|null $offer
     * @return Offer|null
     */
    public function flush(Offer $offer = null)
    {
        $this->getEntityManager()->flush($offer);
        return $offer;
    }

    /**
     * @param Offer $offer
     */
    public function delete(Offer $offer)
    {
        $this->getEntityManager()->remove($offer);
        $this->flush();
    }

    /**
     * @param JobProfile $jobProfile
     */
    public function synchronize(JobProfile $jobProfile){
        $qb = $this->createQueryBuilder('o');
        $qb->where('o.jobProfile = :jobProfile');
        $qb->setParameter('jobProfile', $jobProfile);
        $offer = $qb->getQuery()->getOneOrNullResult();

        if($offer) $this->delete($offer);

    }

}