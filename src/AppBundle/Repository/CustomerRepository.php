<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Customer;
use Doctrine\ORM\EntityRepository;

class CustomerRepository extends EntityRepository
{

    /**
     * @param $customer
     * @param $flush
     */
    public function persist($customer, $flush)
    {
        $this->getEntityManager()->persist($customer);
        if($flush) $this->flush();
        return $customer;
    }

    /**
     * @param customer|null $customer
     */
    public function flush(Customer $customer = null)
    {
        $this->getEntityManager()->flush($customer);
        return $customer;
    }

    /**
     * @param customer $customer
     */
    public function delete(Customer $customer)
    {
        $this->getEntityManager()->remove($customer);
        $this->flush();
    }
}
