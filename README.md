INTRANET
========================

## Prod
- Adresse de la prod : http://erpssi.sandbox.skilvioo.com (il n'y a pas de page par défaut, donc vous aurez une 404)
- Adresse pour le login : http://erpssi.sandbox.skilvioo.com/api/login_check

- login : user
- password : erpssi


##JWT
### JWT Lexik
#### How to init JWT on localhost or on server ?
In order to use JWT Authentication on your env, you need to do those commands : 
 ```
 mkdir -p var/jwt 
 ```
 ``` 
 openssl genrsa -out var/jwt/private.pem -aes256 4096
  ```
 ```
  openssl rsa -pubout -in var/jwt/private.pem -out var/jwt/public.pem
   ```
 ```
  composer update
 ```

Don't forget to load your fixture
 ```
  php app/console doctrine:fixtures:load
```

The user1 (password: erpssi) is working. 

#### How to login ?
With your postman or your front api :
Request /api/login_check with thoses parameters : _username and _password

#### How to reach a route ?
With your postman or your front api you need to give inside the header the token :
Autorization                    Bearer token_string

Caution, do not forget the space between Bearer and your token_string
Why Bearer for token prefix ? Because Bearer is a good JWT practice ;)

### JWT Refresh

 ```
  composer update
 ```
 
 ### JWT Word 
 @Security from Symfony does work with JWT, feel free to use it !
 @Voter from Symfony does work with JWT, feel sooo free to use it !
    
##Use docker
Install it, docker.com and search for docker-compose 

Once both are installed. just type that command in the project:


```
docker-compose up -d --build
```

To connect to the server, use this command:

``` 
docker exec -it intranet_apache_1 bash
```
 
if you're using debian, you should launch docker with sudo. Therefor, to avoid permissions problems, you should connect and execute composer install via this command:

``` 
sudo docker exec -u 1000 -it intranet_apache_1 bash
```

You can always redo your docker container if you break something.

fichier parameters.yml

```
parameters:
    database_host: db
    database_port: 3306
    database_name: intranet
    database_user: intranet
    database_password: intranet
    mailer_transport: smtp
    mailer_host: 127.0.0.1
    mailer_user: null
    mailer_password: null
    secret: ThisTokenIsNotSoSecretChangeIt
```

Enjoy !

## Prerequisites

Last version of:
  * Git
  * Composer

  Just run this line to get the project
    
```   
git clone http://78.252.100.60:3280/ERPSSI/intranet.git
```


## Installing

Initilisation of the project
```
composer install
```

Modify the database settings in `/app/config/parameters.yml`

Then, run those two commands to set up your database with data
```
  php app/console doctrine:schema:update --force
  php app/console doctrine:fixtures:load
```