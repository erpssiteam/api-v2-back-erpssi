#!/usr/bin/env bash
cd /var/www/erpssi/
setfacl -R -m u:www-data:rwX -m u:`whoami`:rwX app/cache app/logs
setfacl -dR -m u:www-data:rwx -m u:`whoami`:rwx app/cache app/logs